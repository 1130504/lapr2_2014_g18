/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.model.Organizador;
import eventoscientificos.model.Evento;
import eventoscientificos.model.RegistoOrganizadores;
import eventoscientificos.model.Utilizador;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.RegistoEventos;
import eventoscientificos.model.Topico;
import eventoscientificos.model.ListaTopicos;
import eventoscientificos.controller.CriarCPController;
import eventoscientificos.eventostate.EventoTopicosCriadosState;
import eventoscientificos.model.Artigo;
import eventoscientificos.model.CP;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alexandre
 */
public class CriarCPControllerTest {
    
    public CriarCPControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getEventosOrganizador method, of class CriarCPController.
     */
    @Test
    public void testGetEventosOrganizador() {
        System.out.println("getEventosOrganizador");
        Utilizador u = new Utilizador("user2", "12345", "utilizador 2", "user2@xxx.pt");
        Evento e = new Evento("ESOFT Workshop", "Workshop sobre Engenharia de Software");
        
        Empresa ep = new Empresa();
        RegistoEventos registoEventos = new RegistoEventos();
       
        
        CriarCPController instance = new CriarCPController(ep);
        instance.selectEvento(e);
        
       
        Organizador org = new Organizador("user2", u);
        List<Organizador> listOrg = new ArrayList<Organizador>();
        listOrg.add(org);
        RegistoOrganizadores listaOrganizador = new RegistoOrganizadores();
        listaOrganizador.setM_listaOrganizadores(listOrg);
       
        e.setM_listaOrganizadores(listaOrganizador);
       
       
        List<Evento> expResult = registoEventos.getEventosOrganizador("user2", ep);
        
        List<Evento> result = instance.getEventosOrganizador("user2");
        
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getTopicosEvento method, of class CriarCPController.
     */
    @Test
    public void testGetTopicosEvento() {
        System.out.println("getTopicosEvento");
        Evento e = new Evento("ESOFT Workshop", "Workshop sobre Engenharia de Software");
        Empresa ep = new Empresa();
        CriarCPController instance = new CriarCPController(ep);
        Topico toipco = new Topico("Descricao", "ACM");
        e.addTopico(toipco);
        ListaTopicos expResult = e.getTopicos();
        instance.selectEvento(e);
        ListaTopicos result = instance.getTopicosEvento();
        assertEquals(expResult, result);
        
    }



    /**
     * Test of addMembroCP method, of class CriarCPController.
     */
    @Test
    public void testAddMembroCP() {
        System.out.println("addMembroCP");
        Utilizador u = new Utilizador("user2", "12345", "Utilizador 2", "user2@xxx.pt");
        Revisor rev = new  Revisor(u);
        
        Evento e = new Evento("ESOFT Workshop", "Workshop sobre Engenharia de Software");
        Empresa ep = new Empresa();
        
        CriarCPController instance = new CriarCPController(ep);
        instance.selectEvento(e);
        instance.registaMembroCP(rev);
        
        Topico t = new Topico("string", "string");
         
        ListaTopicos lt = new ListaTopicos();
        
        lt.getTopicos().add(t);
        rev.setListaTopicos(lt);
        
        
        
        String expResult =rev.toString();
        String result= u.toString() + ": "+ t.toString();
                
        assertEquals(expResult, result);
    }

    /**
     * Test of registaMembroCP method, of class CriarCPController.
     */
    @Test
    public void testRegistaMembroCP() {
        System.out.println("registaMembroCP");
        Utilizador u = new Utilizador("user2", "12345", "Utilizador 2", "user2@xxx.pt");
        Revisor rev = new  Revisor(u);
        Evento e = new Evento("ESOFT Workshop", "Workshop sobre Engenharia de Software");
        Empresa ep = new Empresa();
        
        
        CriarCPController instance = new CriarCPController(ep);
        instance.selectEvento(e);
        instance.registaMembroCP(rev);
        boolean expResult = true;
        boolean result = instance.registaMembroCP(rev);
        assertEquals(expResult, result);
       
    }
    
    /**
     * Test of alterarEstado method, of class CriarCPController.
     */
    @Test
    public void testalterarEstado() {
        System.out.println("Teste método alterarEstado da classe CriarCPController");

        Utilizador u1 = new Utilizador("nome", "nome", "nome", "nome");
        Utilizador u2 = new Utilizador("nome2", "nome2", "nome2", "nome2");
        //Revisor tem uma lista de tópicos.
        Revisor r1 = new Revisor(u1);
        Revisor r2 = new Revisor(u2);
        Topico t1 = new Topico("omg", "omg");
        r1.getM_listaTopicos().add(t1);
        r2.getM_listaTopicos().add(t1);
        //Tópico adicionado para cada Revisor.

        /**
         * Passa de EventoTopicosCriados para EventoCPDefinidaState com o sucesso do UseCase (Criar
         * Comissão de Programa).
         */
        Empresa a = new Empresa();
        CriarCPController controller = new CriarCPController(a);
        Evento e = new Evento("ESOFT Workshop", "Workshop sobre Engenharia de Software");
        CP cp = new CP();
        cp.registaMembroCP(r1);
        cp.registaMembroCP(r2);
        /**
         * CP tem dois Revisores, Evento tem CP.
         */


        e.setEstado(new EventoTopicosCriadosState(e));
        controller.selectEvento(e); //Cria uma CP nova.
        //setCP após o método selectEvento da classe controller.
        e.setCP(cp);
        boolean expResult = true;
        /**
         * Último estado: EventoTopicosCriadosState.
         */

        boolean result = controller.alterarEstado();

        assertEquals(expResult, result);

    }
    
}
