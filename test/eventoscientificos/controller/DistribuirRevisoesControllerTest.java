/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.eventostate.EventoCPDefininaState;
import eventoscientificos.eventostate.EventoCriadoState;
import eventoscientificos.model.Distribuicao;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.RegistoOrganizadores;
import eventoscientificos.model.Organizador;
import eventoscientificos.model.ProcessoDistribuicao;
import eventoscientificos.model.RegistoEventos;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Utilizador;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class DistribuirRevisoesControllerTest {
    
    public DistribuirRevisoesControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getEventosOrganizador method, of class DistribuirRevisoesController.
     */
    @Test
    public void testGetEventosOrganizador() {
        System.out.println("getEventosOrganizador");
        
        RegistoEventos es = new RegistoEventos();
        Utilizador u = new Utilizador("user1", "teste", "user1", "user1@xxx.pt");
        String strId = "user1";
        Organizador o = new Organizador("user1", u);
        RegistoOrganizadores lo = new RegistoOrganizadores();
        lo.addOrganizador(o);
        Evento e = new Evento();
        e.setM_listaOrganizadores(lo);
        
        DistribuirRevisoesController instance = null;
        //List<Evento> expResult = es.getEventosOrganizador(strId);;
        List<Evento> result = instance.getEventosOrganizador(strId);
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListaRevisores method, of class DistribuirRevisoesController.
     */
    @Test
    public void testGetListaRevisores() {
        System.out.println("getListaRevisores");
        DistribuirRevisoesController instance = null;
        ArrayList<Revisor> expResult = null;
        ArrayList<Revisor> result = instance.getListaRevisores();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDistribuicao method, of class DistribuirRevisoesController.
     */
    @Test
    public void testSetDistribuicao() {
        System.out.println("setDistribuicao");
        Distribuicao d = null;
        DistribuirRevisoesController instance = null;
        instance.setDistribuicao(d);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDistribuicoes method, of class DistribuirRevisoesController.
     */
    @Test
    public void testGetDistribuicoes() {
        System.out.println("getDistribuicoes");
        DistribuirRevisoesController instance = null;
        List<Distribuicao> expResult = null;
        List<Distribuicao> result = instance.getDistribuicoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of mostraDistribuicao method, of class DistribuirRevisoesController.
     */
    @Test
    public void testMostraDistribuicao() {
        System.out.println("mostraDistribuicao");
        Distribuicao d = null;
        DistribuirRevisoesController instance = null;
        String expResult = "";
        String result = instance.mostraDistribuicao(d);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of novaDistribuicao method, of class DistribuirRevisoesController.
     */
    @Test
    public void testNovaDistribuicao() {
        System.out.println("novaDistribuicao");
        DistribuirRevisoesController instance = null;
        Distribuicao expResult = new Distribuicao();
        Distribuicao result = instance.novaDistribuicao();
        assertEquals(expResult, result);
    }
    
     /**
     * Test of alterarEstado method, of class DistribuirRevisoesController.
     */
    @Test
    public void testalterarEstado() {
        System.out.println("Teste ao método alterarEstado da classe DistribuirRevisoesController");
        Empresa a =new Empresa();
        DistribuirRevisoesController controller = new DistribuirRevisoesController(a);
      
        Evento e = new Evento("ESOFT Workshop", "Workshop sobre Engenharia de Software");
        ArrayList <Distribuicao> lr = new ArrayList<>();
        Distribuicao d = new Distribuicao();
        lr.add(d);
        ProcessoDistribuicao pd  = new ProcessoDistribuicao(lr);
        
        e.setM_processoDistribuicao(pd);
        
        
        controller.setEvento(e);
        e.setEstado(new EventoCPDefininaState(e));
        

        boolean expResult = true;
        /**
         * Último estado: EventoCPDefinidaState.
         */

        boolean result = controller.alterarEstado();

        assertEquals(expResult, result);

    }
    
}
