/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Autor;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import eventoscientificos.controller.*;

/**
 *
 * @author Fábio Ferreira <1131242@isep.ipp.pt>
 */
public class SubmeterArtigoControllerTest {
    
    public SubmeterArtigoControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of verificaMailAutor method, of class SubmeterArtigoController.
     */
    @Test
    public void testVerificaMailAutor() {
        System.out.println("verificaMailAutor");
        String mail = "1131242@isep.ipp.pt";
        
        Autor a = new Autor();
        a.setAfiliacao("XXX");
        a.setEMail(mail);
        a.setNome("VVV");
        List<Autor> la = new ArrayList<Autor>();
        la.add(a);
        Artigo a1 = new Artigo();
        a1.setM_listaAutores(la);
        
        Empresa e = new Empresa();
        SubmeterArtigoController instance = new SubmeterArtigoController(e);
        instance.setM_artigo(a1);
        
        boolean expResult = false;
        boolean result = instance.verificaMailAutor(mail);
        assertEquals(expResult, result);
        
    }
    
}
