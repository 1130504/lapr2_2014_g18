///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package eventoscientificos.controller;
//
//import eventoscientificos.controller.AceitarRejeitarArtigoController;
//import eventoscientificos.model.Empresa;
//import eventoscientificos.model.Evento;
//import eventoscientificos.model.Utilizador;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//
///**
// *
// * @author Daniela Maia <1130588@isep.ipp.pt>
// */
//public class AceitarRejeitarArtigoControllerTest {
//    
//    public AceitarRejeitarArtigoControllerTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
//
//    /**
//     * Test of validaLogin method, of class AceitarRejeitarArtigoController.
//     */
//    @Test
//  public void testValidaLogin() {
//        System.out.println("validaLogin");
//        String organizador = "org1";
//        Utilizador u = new Utilizador("org1", "123", "Org 1", "org1@email");
//        Evento e1 = new Evento();
//        Empresa e = new Empresa();
//        e1.getM_listaOrganizadores().addOrganizador(u.getUsername(), u);
//        AceitarRejeitarArtigoController instance = new AceitarRejeitarArtigoController(e);
//        instance.setM_evento(e1,0);
//        boolean expResult = true;
//        boolean result = instance.validaLogin(organizador);
//        assertEquals(expResult, result);
//
//    }
//
//    /**
//     * Test of aceitaRejeitaController method, of class
//     * AceitarRejeitarArtigoController.
//     */
//    @Test
//    public void testAceitaRejeitaController() {
//        System.out.println("aceitaRejeitaController");
//        String resposta = "a";
//        Empresa e = new Empresa();
//        AceitarRejeitarArtigoController instance = new AceitarRejeitarArtigoController(e);
//        boolean expResult = true;
//        boolean result = instance.aceitaRejeitaController(resposta, 0, 0);
//        assertEquals(expResult, result);
//
//    }
//    
//}
