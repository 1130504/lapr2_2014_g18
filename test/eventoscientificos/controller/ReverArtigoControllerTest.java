/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.eventostate.EventoCriadoState;
import eventoscientificos.eventostate.EventoDistribuidoState;
import eventoscientificos.model.*;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Fábio Ferreira <1131242@isep.ipp.pt>
 */
public class ReverArtigoControllerTest {
    
    public ReverArtigoControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of validaLogin method, of class ReverArtigoController.
     */
    @Test
    public void testValidaLogin() {
        /*System.out.println("validaLogin");
        String revisor = "user0";      
        Empresa e = new Empresa();
        Evento e1 = e.getRegistaEvento().getListaEventos().get(0);
        ReverArtigoController instance = new ReverArtigoController(e);
        instance.setM_evento(e1);
        boolean expResult = true;
        boolean result = instance.validaLogin(revisor);
        assertEquals(expResult, result);*/
    }

    /**
     * Test of addRevisao method, of class ReverArtigoController.
     */
    @Test
    public void testAddRevisao() {
        /*System.out.println("addRevisao");
        Empresa e = new Empresa();
        
        Artigo a = new Artigo();
        a.setTitulo("aa");
        a.setResumo("aaa");
        
        Utilizador u = new Utilizador("user88", "1234", "ola", "ola@xxx.pt");
        Revisor r = new Revisor(u);
        
        Revisao revisao = new Revisao(a, r);
        List<Revisao> esperado = new ArrayList<>();
        esperado.add(revisao);
        Evento ev = new Evento ( "titulo", " ola");
        ReverArtigoController instance = new ReverArtigoController(e);
        instance.setM_evento(ev);
        instance.setRevisao(revisao);
        instance.addRevisao();
        
        
        List<Revisao> resultado = ev.getM_listaRevisoes();
        assertEquals(esperado, resultado);*/
    }
    
    /**
     * Test of alterarEstado method, of class CriarEventoCientificoController.
     */
    @Test
    public void testalterarEstado() {
        System.out.println("Teste ao método alterarEstado da classe ReverArtigoController");
        Empresa a =new Empresa();
        ReverArtigoController controller = new ReverArtigoController(a);
        Utilizador u = new Utilizador("nome", "nome", "nome", "nome");
        Revisor r = new Revisor(u);
        Revisao r1 = new Revisao(1, 1, 1, 1, "Aceite", r);
        Evento e = new Evento("ESOFT Workshop", "Workshop sobre Engenharia de Software");
        /**
         * Adiciona Revisão à lista Revisão.
         */
        List<Revisao> lr = new ArrayList<>();
        lr.add(r1);
        e.setM_listaRevisoes(lr);
        /**
         * Evento tem Revisão na Lista Revisões.
         */
        controller.selectEvento(e);
        e.setEstado(new EventoDistribuidoState(e));
        

        boolean expResult = true;
        /**
         * Último estado: EventoCriadoState.
         */

        boolean result = controller.alterarEstado();

        assertEquals(expResult, result);

    }
    
}
