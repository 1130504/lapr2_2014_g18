/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.model.*;
import eventoscientificos.controller.CriarEventoCientificoController;
import eventoscientificos.eventostate.EventoCriadoState;
import eventoscientificos.eventostate.EventoTopicosCriadosState;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */
public class CriarEventoCientificoControllerTest {

    //lleeeeeeeeeeeell
    public CriarEventoCientificoControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of novoEvento method, of class CriarEventoCientificoController.
     */
    @Test
    public void testNovoEvento() {
        System.out.println("novoEvento");
        Evento e = new Evento("Evento1", "Evento2");
        Evento e1 = new Evento("Evento2", "Evento2");
        RegistoEventos r = new RegistoEventos();

        List<Evento> expResult = new ArrayList<Evento>();
        expResult.add(e);
        expResult.add(e1);
        expResult.add(r.novoEvento());

        RegistoEventos i = new RegistoEventos();
        i.setListaEventos(expResult);
        assertEquals(expResult, i.getListaEventos());
    }

    
    /**
     * Test of alterarEstado method, of class CriarEventoCientificoController.
     */
    @Test
    public void testalterarEstado() {
        System.out.println("Teste ao método alterarEstado da classe CriarEventoCientificoController");
        Empresa a =new Empresa();
        CriarEventoCientificoController controller = new CriarEventoCientificoController(a);
      
        Evento e = new Evento("ESOFT Workshop", "Workshop sobre Engenharia de Software");
        controller.selectEvento(e);
        e.setEstado(new EventoCriadoState(e));
        

        boolean expResult = true;
        /**
         * Último estado: EventoCriadoState.
         */

        boolean result = controller.alterarEstado();

        assertEquals(expResult, result);

    }}
