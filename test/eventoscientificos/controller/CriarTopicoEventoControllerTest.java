/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.eventostate.EventoCriadoState;
import eventoscientificos.eventostate.EventoValRegDefState;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Topico;
import eventoscientificos.model.Utilizador;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */
public class CriarTopicoEventoControllerTest {
    
    public CriarTopicoEventoControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getEventosOrganizador method, of class CriarTopicoEventoController.
     */
    @Test
    public void testGetEventosOrganizador() {
//        System.out.println("getEventosOrganizador");
//        String strId = "";
//        CriarTopicoEventoController instance = null;
//        List<Evento> expResult = null;
//        List<Evento> result = instance.getEventosOrganizador(strId);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of setEvento method, of class CriarTopicoEventoController.
     */
    @Test
   public void testSetEvento() {
//        System.out.println("setEvento");
//        Evento e = null;
//        CriarTopicoEventoController instance = null;
//        instance.setEvento(e);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of addTopico method, of class CriarTopicoEventoController.
     */
    @Test
    public void testAddTopico() {
//        System.out.println("addTopico");
//        String strCodigo = "";
//        String strDescricao = "";
//        CriarTopicoEventoController instance = null;
//        Topico expResult = null;
//        Topico result = instance.addTopico(strCodigo, strDescricao);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of registaTopico method, of class CriarTopicoEventoController.
     */
    @Test
    public void testRegistaTopico() {
//        System.out.println("registaTopico");
//        Topico t = null;
//        CriarTopicoEventoController instance = null;
//        boolean expResult = false;
//        boolean result = instance.registaTopico(t);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of alterarEstado method, of class CriarTopicoEventoController.
     */
    @Test
    public void testAlterarEstado() {
        System.out.println("Teste ao método alterarEstado da classe CriarTopicosEventoController");
        Empresa a =new Empresa();
        CriarTopicoEventoController controller = new CriarTopicoEventoController(a);
        
        Topico t1 = new Topico("topico", "topico");
        Topico t2 = new Topico("topico", "topico");
       
        Evento e = new Evento("ESOFT Workshop", "Workshop sobre Engenharia de Software");
        controller.setEvento(e);
        e.addTopico(t1);
        e.addTopico(t2);
        e.setEstado(new EventoValRegDefState(e));
        

        boolean expResult = true;
        /**
         * Último estado: EventoValRegDefState.
         */

        boolean result = controller.alterarEstado();

        assertEquals(expResult, result);
    
}}
