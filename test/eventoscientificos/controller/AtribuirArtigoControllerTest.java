///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package eventoscientificos.controller;
//
//import eventoscientificos.model.CP;
//import eventoscientificos.model.Empresa;
//import eventoscientificos.model.Evento;
//import eventoscientificos.model.Revisor;
//import eventoscientificos.model.Utilizador;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import static org.junit.Assert.*;
//
///**
// *
// * @author Daniela Maia <1130588@isep.ipp.pt>
// */
//public class AtribuirArtigoControllerTest {
//    
//    public AtribuirArtigoControllerTest() {
//    }
//    
//    @BeforeClass
//    public static void setUpClass() {
//    }
//    
//    @AfterClass
//    public static void tearDownClass() {
//    }
//    
//    @Before
//    public void setUp() {
//    }
//    
//    @After
//    public void tearDown() {
//    }
//
//    
//    /**
//     * Test of validaLogin method, of class AtribuirArtigoController.
//     */
//    @Test
//    public void testValidaLogin() {
//        System.out.println("validaLogin");
//        String organizador = "org1";
//        Utilizador u = new Utilizador("org1", "123", "Org 1", "org1@email");
//        Evento e1 = new Evento();
//        Empresa e = new Empresa();
//        e1.getM_listaOrganizadores().addOrganizador(u.getUsername(), u);
//        AceitarRejeitarArtigoController instance = new AceitarRejeitarArtigoController(e);
//        instance.setM_evento(e1, 0);
//        boolean expResult = true;
//        boolean result = instance.validaLogin(organizador);
//        assertEquals(expResult, result);
//    }
//
//   
//    /**
//     * Test of validaRevisor method, of class AtribuirArtigoController.
//     */
//    @Test
//    public void testValidaRevisor() {
//        System.out.println("validaRevisor");
//        int indiceRevisor = 0;
//        Empresa e = new Empresa();
//        
//        Evento e1 = new Evento("evento", "descricao");
//        e.getRegistaEvento().addEvento(e1);
//        CP cp = new CP(); 
//        Utilizador u = new Utilizador("rev1", "123", "Rev 1", "rev1@email");
//        cp.addMembroCP("rev1", u);
//        AtribuirArtigoController instance = new AtribuirArtigoController(e);
//        instance.setM_evento(0,e1);
//        instance.setM_empresa(e);
//        instance.setCp(cp,0);
//        e1.setCP(cp);        
//        Revisor r = new Revisor(u);
//        e1.getCP().getM_listaRevisores().add(indiceRevisor, r);
//        instance.setM_evento(0,e1);
//        boolean expResult = true;
//        boolean result = instance.validaRevisor(indiceRevisor,0);
//        assertEquals(expResult, result);
//
//    }
//
// /**
//     * Test of validaArtigo method, of class AtribuirArtigoController.
//     */
//    @Test
//    public void testValidaArtigo() {
//        System.out.println("validaArtigo");
//        int indiceArtigo = 0;
//        Empresa e = new Empresa();
//        Evento e1 = new Evento("evento", "descriçao");
//        AtribuirArtigoController instance = new AtribuirArtigoController(e);
//        instance.setM_evento(0,e1);
//        instance.setM_empresa(e);
//        boolean expResult = true;
//        boolean result = instance.validaArtigo(indiceArtigo,0);
//        assertEquals(expResult, result);
//    }
//    
//}
