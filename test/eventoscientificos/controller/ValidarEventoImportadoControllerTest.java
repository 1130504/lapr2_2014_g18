/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.eventostate.*;
import eventoscientificos.model.*;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */
public class ValidarEventoImportadoControllerTest {
    
    public ValidarEventoImportadoControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of EventosPorValidar method, of class ValidarEventoImportadoController.
     */
    @Test
    public void testEventosPorValidar() {
        
    }

    /**
     * Test of registoEventosTemp method, of class ValidarEventoImportadoController.
     */
    @Test
    public void testRegistoEventosTemp() {
        Evento e1 = new Evento("Evento1", "Evento1");
        Evento e2 = new Evento("Evento2", "Evento2");
        Evento e3 = new Evento("Evento3", "Evento3");
        Empresa a = new Empresa();
        e1.setEstado(new EventoCriadoCSVState(e1));
        e2.setEstado(new EventoCriadoCSVState(e2));
        e3.setEstado(new EventoCPDefininaState (e3));
        a.getRegistaEvento().addEvento(e1);
        a.getRegistaEvento().addEvento(e2);
        a.getRegistaEvento().addEvento(e3);
        /**
         * Empresa tem Eventos.
         */
        ValidarEventoImportadoController controller = new ValidarEventoImportadoController(a);
        controller.registoEventosTemp();
        List <Evento> le1 = controller.getListaTemp();
        Evento teste1  = le1.get(0);
        //RegistoEventos1 é o resultado da Lista Temporário de Eventos da Classe Controller.
        RegistoEventos re1= new RegistoEventos();
        re1.setListaEventos(le1);
        
        RegistoEventos re = new RegistoEventos();
        //RegistoEventos tem Evento1 e Evento2 (ResultadoEsperado).
        re.addEvento(e1);
        re.addEvento(e2);
        assertEquals(e1.getEstado(), teste1.getEstado());
        //Eventos passados com Sucesso.
       
                
    }

    /**
     * Test of RegistarCompleto method, of class ValidarEventoImportadoController.
     */
    @Test
    public void testRegistarCompleto() {
        Evento e1 = new Evento("Evento1", "Evento1");
        Evento e2 = new Evento("Evento2", "Evento2");
        Evento e3 = new Evento("Evento3", "Evento3");
        Empresa a = new Empresa();
        e1.setEstado(new EventoCriadoCSVState(e1));
        e2.setEstado(new EventoCriadoCSVState(e2));
        e3.setEstado(new EventoCPDefininaState (e3));
        /**
         * Empresa tem Eventos.
         */
        a.getRegistaEvento().addEvento(e1);
        a.getRegistaEvento().addEvento(e2);
        a.getRegistaEvento().addEvento(e3);
        
        ValidarEventoImportadoController controller = new ValidarEventoImportadoController(a);
        controller.registoEventosTemp();
        List <Evento> le1 = controller.getListaTemp();
        //le1 tem evento e1, evento e2;
              
        controller.RegistarCompleto();
        //Evento e1 que deverá estar no Estado EventoLidoCSVState.
        Evento teste1 = a.getRegistaEvento().getListaEventos().get(1);
        System.out.println(teste1.getEstado());
        
        Evento teste2 = new Evento();
        teste2.setEstado(new EventoLidoCSVState(teste1));

      assertEquals (teste1.getEstado(),teste2.getEstado());
    }
    
}
