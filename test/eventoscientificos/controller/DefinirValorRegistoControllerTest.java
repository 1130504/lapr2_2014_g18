/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.eventostate.EventoCriadoState;
import eventoscientificos.eventostate.EventoRegistadoState;
import eventoscientificos.model.*;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class DefinirValorRegistoControllerTest {

    public DefinirValorRegistoControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of iniciarDefinirValorRegisto method, of class
     * DefinirValorRegistoController.
     */
    @Test
    public void testIniciarDefinirValorRegisto() {
        /*System.out.println("iniciarDefinirValorRegisto");
        Empresa m_empresa = new Empresa();

        DefinirValorRegistoController instance = new DefinirValorRegistoController(m_empresa);
        Evento e1 = new Evento("ESOFT Workshop", "Workshop sobre Engenharia de Software");
        List<Evento> expResult = m_empresa.getRegistaEvento().getEventosOrganizador("user1", m_empresa);
        List<Evento> result = instance.iniciarDefinirValorRegisto("user1");

        assertEquals(expResult, result);*/
    }

    /**
     * Test of setValores method, of class DefinirValorRegistoController.
     */
    @Test
    public void testSetValores() {
       /* System.out.println("setValores");
        Empresa m_empresa = new Empresa();
        DefinirValorRegistoController instance = new DefinirValorRegistoController(m_empresa);
      
        double valorShort = 2.0;
        double valorFull = 3.0;
        double valorPoster = 4.0;
        
        instance.setEvento(result);
        
        instance.setValores(valorShort, valorFull, valorPoster);
       
        instance.setValores(valorShort, valorFull, valorPoster);
        
        
        
        
        System.out.println(expResult.getValorFull());
        System.out.println(expResult.getValorPoster());
        System.out.println(expResult.getValorShort());
        assertEquals(expResult, result);*/
    }
    
    
     /**
     * Test of alterarEstado method, of class DefinirValorRegistoController.
     */
    @Test
    public void testalterarEstado() {
        System.out.println("Teste ao método alterarEstado da classe DefinirValorRegistoController");
        Empresa a =new Empresa();
        DefinirValorRegistoController controller = new DefinirValorRegistoController(a);
        
        Evento e = new Evento("ESOFT Workshop", "Workshop sobre Engenharia de Software");
        e.setValorFull(1);
        e.setValorPoster(1);
        e.setValorShort(1);
        controller.setEvento(e);
        
        e.setEstado(new EventoRegistadoState(e));
        

        boolean expResult = true;
        /**
         * Último estado: EventoRegistadoState.
         */

        boolean result = controller.alterarEstado();

        assertEquals(expResult, result);

    }

}
