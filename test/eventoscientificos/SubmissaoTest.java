/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import eventoscientificos.model.Submissao;
import eventoscientificos.model.Artigo;
import eventoscientificos.model.Autor;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class SubmissaoTest {

    public SubmissaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getArtigo method, of class Submissao.
     */
    @Test
    public void testGetArtigo() {
        System.out.println("getArtigo");
        Submissao instance = new Submissao();
        Artigo expResult = null;
        Artigo result = instance.getArtigo();
        assertEquals(expResult, result);

    }

    /**
     * Test of setArtigo method, of class Submissao.
     */
    @Test
    public void testSetArtigo() {
        System.out.println("setArtigo");
        Artigo artigo = null;
        Submissao instance = new Submissao();
        instance.setArtigo(artigo);
    }

    /**
     * Test of valida method, of class Submissao.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Submissao instance = new Submissao();
        instance.setArtigo(null);
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of novoArtigo method, of class Submissao.
     */
    @Test
    public void testNovoArtigo() {
        System.out.println("novoArtigo");
        Submissao instance = new Submissao();
        Artigo expResult = null;
        Artigo result = instance.novoArtigo();
        assertEquals(result, result);
    }

    /**
     * Test of getInfo method, of class Submissao.
     */
    @Test
    public void testGetInfo() {
        System.out.println("getInfo");
        Artigo a = new Artigo();
        Autor b = new Autor();
        b.setEMail("email@test");
        a.setAutorCorrespondente(b);
        a.setTitulo("teste");

        Submissao instance = new Submissao(a);
        String expResult = "Submissão:\n" + a.getInfo();
        String result = instance.getInfo();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Submissao.
     */
    @Test
    public void testToString() {
        System.out.println("toString");

        Artigo a = new Artigo();
        Autor b = new Autor();
        b.setEMail("email@test");
        a.setAutorCorrespondente(b);
        a.setTitulo("teste");

        Submissao instance = new Submissao(a);
        String expResult = "Submissão:\n" + a.getInfo();
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Submissao.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");

        Artigo a = new Artigo();
        Autor autorA = new Autor();
        autorA.setEMail("email@test");
        a.setAutorCorrespondente(autorA);
        a.setTitulo("teste");

        Object s = new Submissao(a);

        Artigo b = new Artigo();
        Autor autorB = new Autor();
        autorB.setEMail("email@test");
        b.setAutorCorrespondente(autorB);
        b.setTitulo("teste");

        Submissao instance = new Submissao(b);
        boolean expResult = true;
        boolean result = instance.equals(s);
        assertEquals(expResult, result);
    }
       /**
     * Test of equals method, of class Submissao.
     */
    @Test
    public void testEqualsNot() {
        System.out.println("equals not");

        Artigo a = new Artigo();
        Autor autorA = new Autor();
        autorA.setEMail("emailDiferente@test");
        a.setAutorCorrespondente(autorA);
        a.setTitulo("teste");

        Object s = new Submissao(a);

        Artigo b = new Artigo();
        Autor autorB = new Autor();
        autorB.setEMail("email@test");
        b.setAutorCorrespondente(autorB);
        b.setTitulo("teste");

        Submissao instance = new Submissao(b);
        boolean expResult = false;
        boolean result = instance.equals(s);
        assertEquals(expResult, result);
    }


}
