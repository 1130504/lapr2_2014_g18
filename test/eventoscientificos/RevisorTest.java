/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import eventoscientificos.model.Utilizador;
import eventoscientificos.model.Revisor;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alexandre
 */
public class RevisorTest {
    
    public RevisorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    

    /**
     * Test of valida method, of class Revisor.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Utilizador u = new Utilizador("user2", "12345", "utilizador 2", "user2@xxx.pt");
        Revisor instance = new Revisor(u);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
       
    }

    
   

    /**
     * Test of toString method, of class Revisor.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        
        String str = "Utilizador:\n";
        str += "\tNome: " + "Utilizador 2" + "\n";
        str += "\tUsername: " + "user2" + "\n";
        str += "\tPassword: " + "12345" + "\n";
        str += "\tEmail: " + "user2@xxx.pt" + "\n" + ": ";
        Utilizador u = new Utilizador("user2", "12345", "Utilizador 2", "user2@xxx.pt");
        Revisor instance = new Revisor(u);
        String expResult = str;
        String result = instance.toString();
        assertEquals(expResult, result);
        
    }
    
}
