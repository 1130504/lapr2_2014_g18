/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import eventoscientificos.model.Evento;
import eventoscientificos.model.RegistoEventos;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */
public class RegistoEventosTest {

    public RegistoEventosTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    
      /**
        * Test of testEquals method, if class CriarEventoCientificoController.
        */
    @Test
    public void testEquals(){
        System.out.println("Equals");
      
        Evento e = new Evento("Evento1", "Evento2");
        Evento e1 = new Evento("Evento2", "Evento2");
        RegistoEventos r = new RegistoEventos();
        RegistoEventos r1 = new RegistoEventos();
      
        List<Evento> expResult = new ArrayList<Evento>();
        expResult.add(e);
        expResult.add(e1);
        r.setListaEventos(expResult);
        r1.setListaEventos(expResult);
        boolean bool = r.equals(r1);
        boolean a = true;
        
        assertEquals(bool, a);
    }
    /**
     * Test of novoEvento method, of class RegistoEventos.
     * 
     */
    @Test
    public void testNovoEvento() {
        System.out.println("novoEvento");
        Evento e = new Evento("Evento1", "Evento2");
        Evento e1 = new Evento("Evento2", "Evento2");
        RegistoEventos r =  new RegistoEventos();
        
        
        List<Evento> expResult = new ArrayList<Evento>();
        expResult.add(e);
        expResult.add(e1);
        expResult.add(r.novoEvento());
        
        
        RegistoEventos i = new RegistoEventos();
        i.setListaEventos(expResult);
        assertEquals(expResult, i.getListaEventos());
    }

    /**
     * Test of validaEvento method, of class RegistoEventos.
     */
    @Test
    public void testValidaEvento() {
        System.out.println("validaEvento");
        Evento e = new Evento("Evento1", "Evento2");
        Evento e1 = new Evento("Evento2", "Evento2");
        
        List<Evento> expResult = new ArrayList<Evento>();
        expResult.add(e);
        expResult.add(e1);
        
        RegistoEventos i = new RegistoEventos();
        i.setListaEventos(expResult);
        
        if (i.validaEvento(e)) {
           assertEquals(expResult, i.getListaEventos());  
        }else{
            System.out.println("Error");
                }
       
    }

    /**
     * Test of registaEvento method, of class RegistoEventos.
     */
    @Test
    public void testRegistaEvento() {
        System.out.println("registaEvento");
        Evento e = new Evento("Evento1", "Evento2");
        Evento e1 = new Evento("Evento2", "Evento2");
        
        List<Evento> expResult = new ArrayList<Evento>();
        expResult.add(e);
        expResult.add(e1);
        
        
        RegistoEventos i = new RegistoEventos();
        i.setListaEventos(expResult);
        assertEquals(expResult, i.getListaEventos());
       
    }

    /**
     * Test of getListaEventos method, of class RegistoEventos.
     */
    @Test
    public void testGetListaEventos() {
        System.out.println("getListaEventos");
        Evento e = new Evento("Evento1", "Evento2");
        Evento e1 = new Evento("Evento2", "Evento2");
        
        List<Evento> expResult = new ArrayList<Evento>();
        expResult.add(e);
        expResult.add(e1);
        
        
        RegistoEventos i = new RegistoEventos();
        i.setListaEventos(expResult);
        assertEquals(expResult, i.getListaEventos());
        
    }

    /**
     * Test of setListaEventos method, of class RegistoEventos.
     */
    @Test
    public void testSetListaEventos() {
        System.out.println("setListaEventos");
        Evento e = new Evento("Evento1", "Evento2");
        Evento e1 = new Evento("Evento2", "Evento2");

        List<Evento> expResult = new ArrayList<Evento>();
        expResult.add(e);
        expResult.add(e1);

        RegistoEventos i = new RegistoEventos();
        i.setListaEventos(expResult);

        assertEquals(expResult, i.getListaEventos());
    }

}
