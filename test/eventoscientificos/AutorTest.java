/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Utilizador;
import eventoscientificos.model.Autor;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author amartins
 */
public class AutorTest {
    
    public AutorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   

    /**
     * Test of valida method, of class Autor.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Autor instance = new Autor();
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);

    }

    /**
     * Test of podeSerCorrespondente method, of class Autor.
     */
    @Test
    public void testPodeSerCorrespondente() {
        System.out.println("podeSerCorrespondente");
        Autor instance = new Autor();
        boolean expResult = false;
        boolean result = instance.podeSerCorrespondente();
        assertEquals(expResult, result);
  
    }

    /**
     * Test of toString method, of class Autor.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Autor instance = new Autor();
        instance.setAfiliacao("ISEP");
        instance.setEMail("email");
        instance.setNome("nome");
        instance.setUtilizador(new Utilizador("1", "2", "3", "4"));
        
        String expResult = "nome - ISEP - email" ;
        String result = instance.toString();
        assertEquals(expResult, result);
   
    }

    /**
     * Test of equals method, of class Autor.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        Autor instance = new Autor();
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);

    }
    
}
