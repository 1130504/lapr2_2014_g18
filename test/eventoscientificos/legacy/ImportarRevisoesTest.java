/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.legacy;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Revisao;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Utilizador;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alexandre
 */
public class ImportarRevisoesTest {

    public ImportarRevisoesTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of leFicheiroCsv method, of class ImportarRevisoes.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testLeFicheiroCsv() throws Exception {
        System.out.println("leFicheiroCsv");
        String filename = "EventHist_CDIO_v3_Reviews";
        Empresa ep = null;
        int[] vec = {0, 0, 0, 0};
        
        Utilizador util = new Utilizador("", "", "", "wolverine@marvel.com");

        Revisor revisor = new Revisor(util);
        Revisao rev = new Revisao();
        rev.setM_revisor(revisor);
        rev.setDados(vec, "");
        rev.setTexto("texto");
     
        
        Map<String, Revisao> expResult = new HashMap<>();
        Map<String, Revisao> result = new HashMap<>();

        String[] ordem = null;
        String[] partes;
        
        expResult.put(revisor.getUtilizador().getEmail(), rev);
        Scanner in;

        String revisor1 = "wolverine@marvel.com";

        in = new Scanner(new File(filename + ".csv"), "ISO-8859-15");
        String linha;
        int nrLinhas = 0;

        boolean primeiraLinha = true;
        while (in.hasNextLine()) {
            linha = in.nextLine();
            Revisao revisao = new Revisao();
            int[] vec1 = {0, 0, 0, 0};
            
            revisao.setDados(vec1, "");
            revisao.setTexto("texto");
            nrLinhas++;
            if (primeiraLinha) {
                ordem = linha.split(";");
                primeiraLinha = false;
            } else {

                
                partes = linha.split(";");
                for (int i = 0; i < partes.length; i++) {
                    switch (ordem[1]) {

                        case "Reviewer":
                           if(revisor1 == partes[i]){
                               break;
                           }
                            

                    }

                }

            }
            result.put(revisor1, revisao);
        }
        
        assertEquals(expResult.values().toString(), result.values().toString());
    }
}
