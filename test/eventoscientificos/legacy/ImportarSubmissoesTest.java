/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.legacy;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Autor;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Submissao;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alexandre
 */
public class ImportarSubmissoesTest {

    public ImportarSubmissoesTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of lerFicheiroCsv method, of class ImportarSubmissoes.
     */
    @Test
    public void testLerFicheiroCsv() throws Exception {
        System.out.println("lerFicheiroCsv");
        String filename = "EventHist_CDIO_v3_Papers";
        ImportarSubmissoes instance = new ImportarSubmissoes();
        Map<String, Submissao> expResult = new HashMap<>();
        Map<String, Submissao> result = new HashMap<>();
        String[] ordem = null;
        String[] partes;
        Map<String, Submissao> map = new HashMap<>();
        Scanner in;
        int nrLinhas = 0;
        in = new Scanner(new File(filename + ".csv"), "ISO-8859-15");
        String linha;
        boolean primeiraLinha = true;
        List<Autor> lista0 = new ArrayList<>();
        
        Artigo a = new Artigo("Dummy M049", "", "");
        a.setM_listaAutores(lista0);
        a.setTipo("");
        Submissao sub = new Submissao(a);
        String eventoId = "";
        expResult.put(eventoId, sub);
        while (in.hasNextLine()) {
            linha = in.nextLine();
            nrLinhas++;
            List<Autor> lista = new ArrayList<>();
            Artigo artigo = new Artigo();
            artigo.setM_listaAutores(lista);
            artigo.setResumo("");
            artigo.setTipo("");
            Submissao sub1 = new Submissao(artigo);
            if (primeiraLinha) {
                ordem = linha.split(";");
                primeiraLinha = false;
            } else {

                partes = linha.split(";");
                for (int i = 0; i < partes.length; i++) {
                    switch (ordem[i]) {
                        case "Title":
                            artigo.setTitulo(partes[i]);
                            break;
                    }
                    
                }
            }result.put(eventoId, sub1);
        }assertEquals(expResult.values().toString(), result.values().toString());
    }
}
