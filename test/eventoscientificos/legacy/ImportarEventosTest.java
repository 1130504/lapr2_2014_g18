/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.legacy;

import eventoscientificos.eventostate.EventoCriadoState;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Organizador;
import eventoscientificos.model.Utilizador;
import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alexandre
 */
public class ImportarEventosTest {

    public ImportarEventosTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of lerFicheiroCsv method, of class ImportarEventos.
     */
    @Test
    public void testLerFicheiroCsv() throws Exception {
        System.out.println("lerFicheiroCsv");
        Date data = new Date();
        Evento e = new Evento("6th International CDIO Conference", "");
        e.setDescricao("");
        e.setLocal("");
        e.setDataLimiteSubmissão(data);
        e.setDataInicio(data);
        e.setDataFim(data);
        String[] ordem = null;
        String[] partes;
        Map<String, Evento> map1 = new HashMap<String, Evento>();
        String eventoId = "";
        map1.put(eventoId, e);
        Scanner in;
        String filename = "EventHist_CDIO_v3_Events";

        Empresa ep = new Empresa();

        Map<String, Evento> map2 = new HashMap<>();

        in = new Scanner(new File(filename + ".csv"), "ISO-8859-15");
        String linha;
        boolean primeiraLinha = true;
        while (in.hasNextLine()) {
            linha = in.nextLine();
            if (primeiraLinha) {
                ordem = linha.split(";");
                primeiraLinha = false;
            } else {

                Evento e1 = new Evento();
                e1.setDescricao("");
                e1.setLocal("");
                e1.setDataLimiteSubmissão(data);
                e1.setDataInicio(data);
                e1.setDataFim(data);

                partes = linha.split(";");
                for (int i = 0; i < partes.length; i++) {
                    switch (ordem[i]) {
                        case "Name":
                            e1.setTitulo(partes[i]);
                            break;

                    }
                    map2.put(eventoId, e1);
                }
            }
        }
        
        assertEquals(map1.values().toString(), map2.values().toString());
    }
}
