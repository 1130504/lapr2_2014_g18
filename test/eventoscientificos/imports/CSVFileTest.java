/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.imports;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */
public class CSVFileTest {
    
    public CSVFileTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calcularDataFim method, of class CSVFile.
     */
    @Test
    public void testCalcularDataFim() {
        System.out.println("calcularDataFim");
        String duracao = "5";
        Date dataInicio = new Date("Friday, June 27, 2014");
        //Instância de CSV tem filepath e data (não é preciso para este teste)
        CSVFile instance = new CSVFile("", null);
        Date expResult = new Date("Tuesday, July 1,2014");
        //Testar com Date DataInicio e com Duracao(dias).
        Date result = instance.calcularDataFim(duracao, dataInicio);
        System.out.println(expResult.toLocaleString());
        System.out.println(result.toLocaleString());
        //assertEquals(expResult.toLocaleString(), result.toLocaleString());
        //
      assertEquals(expResult,result);
        
    }}