/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.imports;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */
public class XMLFileTest {
    
    public XMLFileTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   

    /**
     * Test of lerXMLFile method, of class XMLFile.
     */
    @Test
    public void testLerXMLFile() throws Exception {
       /**
         * Evento Esperado.
         */
        Evento expResult = new Evento("2013 Latin American Regional CDIO Meeting", "Universidad de Chile");
        expResult.setDataFim(new Date("Tuesday, June 24, 2014"));
        expResult.setDataInicio(new Date("Monday, June 23, 2014"));
        expResult.setLocal("Cidade" + ", "+ "Pais");
        System.out.println(expResult.toString());
        /**
         * Comparar Strings.
         */
        Empresa a = new Empresa();
        /**
         * O Ficheiro Teste tem Evento.
         */
        XMLFile instance = new XMLFile("C:/Origin/Testes/EventoXml.xml", a);
        
        /**
         * get primeiro Evento e comparar com a String do Evento expResult.
         */
        List <Evento> le = instance.lerXMLFile();
        Evento result = le.get(0);
        System.out.println("");
        System.out.println(result.toString());
       
        
        assertEquals(expResult.toString(), result.toString());
       
    }
    
}
