/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import eventoscientificos.model.Utilizador;
import eventoscientificos.model.ListaUtilizadores;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Daniela Maia <1130588@isep.ipp.pt>
 */
public class ListaUtilizadoresTest {

    public ListaUtilizadoresTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class ListaUtilizadores.
     */
    @Test
    public void testAdd() {
        int i;
        System.out.println("add");
        Utilizador utilizador = new Utilizador("user1", "12345", "Utilizador 1", "user1@xxx.pt");
        ListaUtilizadores instance = new ListaUtilizadores();
        boolean expResult = true;
        boolean result = instance.add(utilizador);
        assertEquals(expResult, result);

    }

    /**
     * Test of addUtilizador method, of class ListaUtilizadores.
     */
    @Test
    public void testAddUtilizador_4args() {
        System.out.println("addUtilizador");
        String username = "";
        String pwd = "";
        String nome = "";
        String email = "";
        ListaUtilizadores instance = new ListaUtilizadores();
        boolean expResult = true;
        boolean result = instance.addUtilizador(username, pwd, nome, email);
        assertEquals(expResult, result);

    }

    /**
     * Test of addUtilizador method, of class ListaUtilizadores.
     */
    @Test
    public void testAddUtilizador_Utilizador() {
        System.out.println("addUtilizador");
        Utilizador utilizador = new Utilizador("bucky","", "Buck Rogers", "buckr@gmail.com");
        ListaUtilizadores instance = new ListaUtilizadores();
        boolean expResult = true;
        boolean result = instance.addUtilizador("bucky","", "Buck Rogers", "buckr@gmail.com");
        assertEquals(expResult, result);

    }

    /**
     * Test of getM_listaUtilizadores method, of class ListaUtilizadores.
     */
    @Test
    public void testGetM_listaUtilizadores() {
        System.out.println("getM_listaUtilizadores");
        ListaUtilizadores instance = new ListaUtilizadores();

        List<Utilizador> expResult = instance.getM_listaUtilizadores();

        List<Utilizador> result = instance.getM_listaUtilizadores();
        assertEquals(expResult, result);

    }

    /**
     * Test of get method, of class ListaUtilizadores.
     */
    @Test
    public void testGet() {
        System.out.println("get");
        ListaUtilizadores instance = new ListaUtilizadores();
        Utilizador u1 = new Utilizador("u1", "123", "utilizador 1", "u1@gmail.com");
        instance.add(u1);
        int utilizadores =0;      
        
        Utilizador expResult = u1;
        Utilizador result = instance.get(utilizadores);
        assertEquals(expResult, result);
               

    }
    
    /**
     * Test of toString method, of class ListaUtilizadores.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        ListaUtilizadores instance = new ListaUtilizadores();
        Utilizador u = new Utilizador("user1", "12345", "User 1", "user1@isep.ipp.pt");
        instance.add(u);
        String expResult = "1. User 1;\n";
        String result = instance.toString();
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class ListaUtilizadores.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Utilizador u1 = new Utilizador("user1", "12345", "User 1", "user1@isep.ipp.pt");
        Utilizador u2 = new Utilizador("user2", "12345", "User 2", "user2@isep.ipp.pt");
        
        ListaUtilizadores lista1 = new ListaUtilizadores();
        
        lista1.add(u1);
        lista1.add(u2);
        ListaUtilizadores instance = new ListaUtilizadores();
        instance.add(u1);
        instance.add(u2);
        boolean expResult = true;
        boolean result = instance.equals(lista1);
        assertEquals(expResult, result);

    }

    /**
     * Test of novoUtilizador method, of class ListaUtilizadores.
     * Erro ao testar o método
     */
    @Test
    public void testNovoUtilizador() {
        System.out.println("novoUtilizador");
        ListaUtilizadores instance = new ListaUtilizadores();
        Utilizador expResult = new Utilizador("user1", "12345", "User 1", "user1@isep.ipp.pt");
        Utilizador result = instance.novoUtilizador();
        result.setEmail("user1@isep.ipp.pt");
        result.setM_strNome("User 1");
        result.setPassword("12345");
        result.setUsername("user1");
        assertEquals(expResult, result);
    }

    /**
     * Test of registaUtilizador method, of class ListaUtilizadores.
     */
    @Test
    public void testRegistaUtilizador() {
        System.out.println("registaUtilizador");
        Utilizador u = new Utilizador("user1", "12345", "User 1", "user1@isep.ipp.pt");
        ListaUtilizadores instance = new ListaUtilizadores();
        boolean expResult = true;
        boolean result = instance.registaUtilizador(u);
        assertEquals(expResult, result);
     
    }

    /**
     * Test of getUtilizador method, of class ListaUtilizadores.
     */
    @Test
    public void testGetUtilizador() {
        System.out.println("getUtilizador");
        String strId = "";
        ListaUtilizadores instance = new ListaUtilizadores();
        Utilizador expResult = null;
        Utilizador result = instance.getUtilizador(strId);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getUtilizadorEmail method, of class ListaUtilizadores.
     */
    @Test
    public void testGetUtilizadorEmail() {
        System.out.println("getUtilizadorEmail");
        String strEmail = "";
        ListaUtilizadores instance = new ListaUtilizadores();
        Utilizador expResult = null;
        Utilizador result = instance.getUtilizadorEmail(strEmail);
        assertEquals(expResult, result);

    }

    /**
     * Test of addUtilizador method, of class ListaUtilizadores.
     */
    @Test
    public void testAddUtilizador() {
        System.out.println("addUtilizador");
        String username = "user1";
        String pwd = "123";
        String nome = "User 1";
        String email = "user1@isep.ipp.pt";
        ListaUtilizadores instance = new ListaUtilizadores();
        boolean expResult = true;
        boolean result = instance.addUtilizador(username, pwd, nome, email);
        assertEquals(expResult, result);

    }
    
    
}
