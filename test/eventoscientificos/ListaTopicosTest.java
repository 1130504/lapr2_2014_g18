/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import eventoscientificos.model.Topico;
import eventoscientificos.model.ListaTopicos;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alexandre
 */
public class ListaTopicosTest {
    
    public ListaTopicosTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class ListaTopicos.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        Topico t = new Topico();
        ListaTopicos instance = new ListaTopicos();
        boolean expResult = true;
        boolean result = instance.add(t);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of validaTopico method, of class ListaTopicos.
     */
    @Test
    public void testValidaTopico() {
        System.out.println("validaTopico");
        Topico t = new Topico();
        ListaTopicos instance = new ListaTopicos();
        boolean expResult = false;
        boolean result = instance.validaTopico(t);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getTopicos method, of class ListaTopicos.
     */
    @Test
    public void testGetTopicos() {
        System.out.println("getTopicos");
        ListaTopicos instance = new ListaTopicos();
        List<Topico> expResult = new ArrayList<Topico>();
        List<Topico> result = instance.getTopicos();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of addAll method, of class ListaTopicos.
     */
    @Test
    public void testAddAll() {
        System.out.println("addAll");
        List<Topico> listaTopicos = new ArrayList<Topico>();
        Topico t1 = new Topico();
        listaTopicos.add(t1);
        ListaTopicos instance = new ListaTopicos();
        instance.addAll(listaTopicos);
        assertEquals(instance.getTopicos(), listaTopicos);
        
    }

    /**
     * Test of size method, of class ListaTopicos.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        ListaTopicos instance = new ListaTopicos();
        int expResult = 0;
        int result = instance.size();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of get method, of class ListaTopicos.
     */
    @Test
    public void testGet() {
        System.out.println("get");
        int indice = 0;
        ListaTopicos instance = new ListaTopicos();
        Topico t1 = new Topico();
        Topico t2 = new Topico();
        List<Topico> listaTopicos = new ArrayList<Topico>();
        listaTopicos.add(t1);
        listaTopicos.add(t2);
        Topico expResult =listaTopicos.get(indice);
        instance.add(t1);
        instance.add(t2);
        Topico result = instance.get(indice);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of equals method, of class ListaTopicos.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Topico t1 = new Topico("Descrição1", "ACM1");
        Topico t2 = new Topico("Descrição2", "ACM2");
        
        ListaTopicos lista1 = new ListaTopicos();
        
        lista1.add(t1);
        lista1.add(t2);
        
        ListaTopicos instance = new ListaTopicos();
        
        instance.add(t1);
        instance.add(t2);
        
        boolean expResult = false;
        boolean result = instance.equals(lista1.getTopicos());
        assertEquals(expResult, result);

    }
    
}
