/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Fabio
 */
public class EstatisticaTest {
    
    public EstatisticaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calculoDoDesvio method, of class Estatistica.
     */
    @Test
    public void testCalculoDoDesvio() {
        System.out.println("calculoDoDesvio");
        double G = 5.0;
        double Cij = 3.0;
        Empresa e = new Empresa();
        Estatistica instance = new Estatistica(e);
        double expResult = 2.0;
        double result = instance.calculoDoDesvio(G, Cij);
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of mediaDoArtigo method, of class Estatistica.
     */
    @Test
    public void testMediaDoArtigo() {
        System.out.println("mediaDoArtigo");
        Revisao revisao = new Revisao(4, 4, 2, 2, "aceite", null);
        Empresa e = new Empresa();
        Estatistica instance = new Estatistica(e);
        double expResult = 3.0;
        double result = instance.mediaDoArtigo(revisao);
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of mediaDosDesvios method, of class Estatistica.
     */
    @Test
    public void testMediaDosDesvios() {
        System.out.println("mediaDosDesvios");
        double[] desvios = {0.5,0.3,0.7,0.1,0.3};
        Empresa e = new Empresa();
        Estatistica instance = new Estatistica(e);
        double expResult = 0.38;
        double result = instance.mediaDosDesvios(desvios);
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of calculaS method, of class Estatistica.
     */
    @Test
    public void testCalculaS() {
        System.out.println("calculaS");
        double[] desvios = {0.5,0.3,0.7,0.1,0.3};
        Empresa e = new Empresa();
        Estatistica instance = new Estatistica(e);
        double expResult = 0.2945;
        double result = instance.calculaS(desvios);
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of calculaZ method, of class Estatistica.
     */
    @Test
    public void testCalculaZ() {
        System.out.println("calculaZ");
        double mediaDesvios = 2.0;
        double s = 1.4;
        double[] desvios = new double[20];
        Empresa e= new Empresa();
        Estatistica instance = new Estatistica(e);
        double expResult = 3.1943828249997;
        double result = instance.calculaZ(mediaDesvios, s, desvios);
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of aceitaRejeita method, of class Estatistica.
     */
    @Test
    public void testAceitaRejeita() {
        System.out.println("aceitaRejeita");
        double Z = 2.0;
        Empresa e = new Empresa();
        Estatistica instance = new Estatistica(e);
        String expResult = "Rejeitado";
        String result = instance.aceitaRejeita(Z);
        assertEquals(expResult, result);
        
    }
    
}
