/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class DistribuicaoTest {
    
    public DistribuicaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getArtigo method, of class Distribuicao.
     */
    @Test
    public void testGetArtigo() {
        System.out.println("getArtigo");
        Artigo teste = new Artigo("teste", "teste", "teste");
        
        Distribuicao instance = new Distribuicao();
        instance.setArtigo(teste);
        Artigo expResult = teste;
        Artigo result = instance.getArtigo();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaRevisor method, of class Distribuicao.
     */
    @Test
    public void testSetAndGetListaRevisor() {
        System.out.println("getListaRevisor");
        Distribuicao instance = new Distribuicao();
        CP cp = new CP();
        Utilizador u = new Utilizador("teste", "teste", "teste", "teste@xxx.pt");
        Revisor r1 = new Revisor(u);
        cp.registaMembroCP(r1);
        
        instance.setListaRevisores(cp.getListaRevisores());
        List<Revisor> expResult = cp.getListaRevisores();
        List<Revisor> result = instance.getListaRevisor();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Distribuicao.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Distribuicao instance = new Distribuicao();
        Artigo artigo = new Artigo("Artigo","resumoArtigo", "short");
        Revisor revisor = new Revisor(new Utilizador("Teste", "Teste", "Teste", "Teste@xxx.pt"));
        CP cp = new CP();
        cp.registaMembroCP(revisor);
        
        instance.setArtigo(artigo);
        instance.setListaRevisores(cp.getListaRevisores());
        System.out.println(instance.toString());
        String expResult = "Distribuição do artigo: \n" + artigo.getTitulo()+ "\n"
                + "Lista de Revisores: \n" 
                + revisor.getNome()+"\n";
        String result = instance.toString();
        System.out.println(expResult);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Distribuicao.
     */
    @Test
    public void testEqualsNot() {
        System.out.println("equalsNot");
        Object obj = new Distribuicao();
        Distribuicao instance = new Distribuicao();
        Artigo artigo = new Artigo("Artigo","resumoArtigo", "short");
        Revisor revisor = new Revisor(new Utilizador("Teste", "Teste", "Teste", "Teste@xxx.pt"));
        CP cp = new CP();
        cp.registaMembroCP(revisor);
        
        instance.setArtigo(artigo);
        instance.setListaRevisores(cp.getListaRevisores());
        
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    } 
    /**
     * Test of equals method, of class Distribuicao.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        
        Distribuicao instance = new Distribuicao();
        Artigo artigo = new Artigo("Artigo","resumoArtigo", "short");
        Revisor revisor = new Revisor(new Utilizador("Teste", "Teste", "Teste", "Teste@xxx.pt"));
        CP cp = new CP();
        cp.registaMembroCP(revisor);
        instance.setArtigo(artigo);
        instance.setListaRevisores(cp.getListaRevisores());
        
        
        Distribuicao obj = new Distribuicao(artigo, (ArrayList<Revisor>) cp.getListaRevisores());
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
}
