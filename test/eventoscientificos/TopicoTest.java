/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import eventoscientificos.model.Topico;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


    
    public class TopicoTest {
    
    public TopicoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setDescricao method, of class Topico.
     */
    @Test
    public void testSetDescricao() {
        System.out.println("setDescricao");
        String strDescricao = "";
        Topico instance = new Topico();
        instance.setDescricao(strDescricao);
        
    }

    /**
     * Test of setCodigoACM method, of class Topico.
     */
    @Test
    public void testSetCodigoACM() {
        System.out.println("setCodigoACM");
        String codigoACM = "";
        Topico instance = new Topico();
        instance.setCodigoACM(codigoACM);
      
    }

    /**
     * Test of valida method, of class Topico.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Topico instance = new Topico();
        instance.setCodigoACM("");
        instance.setDescricao("");
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of toString method, of class Topico.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Topico instance = new Topico();
        instance.setCodigoACM("ACM");
        instance.setDescricao("Descricao");
        String expResult = "\nCodigo: ACM\nDescrição: Descricao" ;
        String result = instance.toString();
        assertEquals(expResult, result);
        
    }
    
    /**
     * Test of equals method, of class Topico.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Topico t = new Topico();
        t.setCodigoACM("ACM");
        t.setDescricao("Descrição");
        Object t2 = new Topico("Descrição", "ACM");
        boolean expResult = true;
        boolean result = t.equals(t2);
        assertEquals(expResult, result);

    }
    
}

