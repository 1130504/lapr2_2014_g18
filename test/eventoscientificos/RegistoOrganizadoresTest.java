/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import eventoscientificos.model.Organizador;
import eventoscientificos.model.RegistoOrganizadores;
import eventoscientificos.model.Utilizador;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author i130588
 */
public class RegistoOrganizadoresTest {
    
    public RegistoOrganizadoresTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addOrganizador method, of class RegistoOrganizadores.
     */
    @Test
    public void testAddOrganizador() {
        int i;
        System.out.println("addOrganizador");
        Utilizador u1 = new Utilizador("ola3", "ola3", "utilizador s", "ola@gmail.com");
        RegistoOrganizadores instance = new RegistoOrganizadores();
        boolean expResult = true;
        boolean result = instance.addOrganizador(u1.getNome(), u1);
        assertEquals(expResult, result);
    }

    /**
     * Test of getRegistoOrganizadores method, of class RegistoOrganizadores.
     */
    @Test
    public void testGetListaOrganizadores() {
        
        System.out.println("getListaOrganizadores");
        
        Utilizador u1 = new Utilizador("ola", "ola", "utilizador s", "ola@gmail.com");
        Utilizador u2 = new Utilizador("xau", "xau", "utilizador x", "xau@gmail.com");
        Organizador o1 = new Organizador("org.1", u1);
        Organizador o2 = new Organizador("org.2", u2);
        
        RegistoOrganizadores instance = new RegistoOrganizadores();
        
        List<Organizador> expResult = new ArrayList<Organizador>();
        expResult.add(o1);
        expResult.add(o2);
        instance.setM_listaOrganizadores(expResult);
        List<Organizador> result = instance.getListaOrganizadores();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getM_listaOrganizadores method, of class RegistoOrganizadores.
     */
    @Test
    public void testGetM_listaOrganizadores() {
        System.out.println("getM_listaOrganizadores");
        
        Utilizador u1 = new Utilizador("ola", "ola", "utilizador s", "ola@gmail.com");
        Utilizador u2 = new Utilizador("xau", "xau", "utilizador x", "xau@gmail.com");
        Organizador o1 = new Organizador("org.1", u1);
        Organizador o2 = new Organizador("org.2", u2);
        
        RegistoOrganizadores instance = new RegistoOrganizadores();
        
        List<Organizador> expResult = new ArrayList<Organizador>();
        expResult.add(o1);
        expResult.add(o2);
        instance.setM_listaOrganizadores(expResult);
        List<Organizador> result = instance.getM_listaOrganizadores();
        
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setM_listaOrganizadores method, of class RegistoOrganizadores.
     */
    @Test
    public void testSetM_listaOrganizadores() {
        System.out.println("setM_listaOrganizadores");
        
        Utilizador u1 = new Utilizador("ola", "ola", "utilizador s", "ola@gmail.com");
        Utilizador u2 = new Utilizador("xau", "xau", "utilizador x", "xau@gmail.com");
        Organizador o1 = new Organizador("org.1", u1);
        Organizador o2 = new Organizador("org.2", u2);
        
        List<Organizador> m_listaOrganizadores = new ArrayList<Organizador>();
        
        m_listaOrganizadores.add(o1);
        m_listaOrganizadores.add(o2);
        
        RegistoOrganizadores instance = new RegistoOrganizadores();
        instance.setM_listaOrganizadores(m_listaOrganizadores);
        
        assertEquals(m_listaOrganizadores, instance.getM_listaOrganizadores());
        
    }
    
    /**
     * Test of toString method, of class ListaUtilizadores.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        
        Utilizador u1 = new Utilizador("ola", "ola", "utilizador s", "ola@gmail.com");
        Utilizador u2 = new Utilizador("xau", "xau", "utilizador x", "xau@gmail.com");
        Organizador o1 = new Organizador("org.1", u1);
        Organizador o2 = new Organizador("org.2", u2);
        
        RegistoOrganizadores instance = new RegistoOrganizadores();
        instance.addOrganizador(o1);
        instance.addOrganizador(o2);
        String expResult = "1. utilizador s; \n2. utilizador x; \n";
        String result = instance.toString();
        
        assertEquals(expResult, result);
        
    }

    /**
     * Test of equals method, of class ListaUtilizadores.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        
        Utilizador u1 = new Utilizador("ola", "ola", "utilizador s", "ola@gmail.com");
        Utilizador u2 = new Utilizador("xau", "xau", "utilizador x", "xau@gmail.com");
        Organizador o1 = new Organizador("org.1", u1);
        Organizador o2 = new Organizador("org.2", u2);
        
        RegistoOrganizadores lista1 = new RegistoOrganizadores();
        lista1.addOrganizador(o1);
        lista1.addOrganizador(o2);
        
        RegistoOrganizadores instance = new RegistoOrganizadores();
        instance.addOrganizador(o1);
        instance.addOrganizador(o2);
        
        boolean expResult = true;
        boolean result = instance.equals(lista1);
        
        
        assertEquals(expResult, result);
        
        
    }
    
}
