/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import eventoscientificos.pagamentos.SistemaPagamento;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Alexandre
 */
public class ProcessoPagamento {

    /**
     * Registo de Processo de Pagamento
     */
    private RegistoProcessoPagamento m_registoProcessoPag;

    
  

    /**
     * Evento
     */
    private Evento m_evento;

    /**
     * Formula
     *
     * @param registo
     */
    private Formula m_formula;
    
    

    /**
     * Valor de Pagamento
     */
    private double m_valor;


    
    Format format = new SimpleDateFormat("yyyy-MM-dd");
    /**
     *
     * @param registo
     * @param evento
     */
    public ProcessoPagamento(RegistoProcessoPagamento registo, Evento evento) {
        this.m_registoProcessoPag = registo;
        this.m_evento = evento;
    }

    public void setFormula() {

         this.m_formula = this.m_evento.getM_formula();
    }
    
    

    public void calcularPagamento() {
        this.m_valor = this.m_evento.getM_formula().calcularPagamento(m_registoProcessoPag.getNumeroFull(),
                m_registoProcessoPag.getNumeroPoster(), m_registoProcessoPag.getNumeroShort(), this.m_evento);
    } 
    
    public double getM_valor() {
        
        return this.m_valor;
    }

   
}
