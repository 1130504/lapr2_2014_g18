package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class ListaSubmissoes implements Serializable{

    /**
     * Array list de objetos do tipo Submissao
     */
    private List<Submissao> listaSubmissao = new ArrayList();

    /**
     * Construtor vazio
     */
    public ListaSubmissoes() {
    }

    /**
     * Devolve a lista de submissões
     *
     * @return List<Submissao>
     */
    public List<Submissao> getListaSubmissao() {
        return this.listaSubmissao;
    }

    /**
     * Altera a lista de submissões
     *
     * @param listaSubmissao
     */
    public void setListaSubmissao(List<Submissao> listaSubmissao) {
        this.listaSubmissao = listaSubmissao;
    }

    /**
     * Adiciona uma Submissão ao ArrayList de submissões
     *
     * @param s
     * @return boolean
     */
    public boolean add(Submissao s) {
        return this.listaSubmissao.add(s);
    }

    /**
     * Remove uma Submissão ao ArrayList de submissões
     *
     * @param s
     * @return boolean
     */
    public boolean remove(Submissao s) {
        return this.listaSubmissao.remove(s);
    }

    /**
     * Devolve nova Submissao
     *
     * @return
     */
    public Submissao novaSubmissao() {
        return new Submissao();
    }

    /**
     * Adiciona uma Submissão à lista de submissões
     *
     * @param submissao
     * @return true se adicionado com sucesso
     */
    public boolean addSubmissao(Submissao submissao) {
        if (validaSubmissao(submissao)) {
            return this.listaSubmissao.add(submissao);
        } else {
            return false;
        }

    }

    /**
     * Devolve a descrição textual da lista de Submissoes
     *
     * @return
     */
    @Override
    public String toString() {
        String lsStr = "";
        for (Submissao submissao : listaSubmissao) {
            lsStr += submissao.toString() + "\n";
        }
        return lsStr;
    }

    /**
     * Determina se o objeto recebido por parâmetro é o mesmo
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else {
            if (obj instanceof ListaSubmissoes) {
                ListaSubmissoes ls = (ListaSubmissoes) obj;
                if (ls.listaSubmissao.size() == this.listaSubmissao.size()) {
                    return this.listaSubmissao.containsAll(listaSubmissao);
                }
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * Devolve o hashcode do objecto
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.listaSubmissao);
        return hash;
    }

   

    private boolean validaSubmissao(Submissao submissao) {
        return submissao.valida();
    }
}
