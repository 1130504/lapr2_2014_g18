/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import eventoscientificos.GUI.GUI;
import eventoscientificos.GUI.ImportarEventosUI;
import eventoscientificos.GUI.ValidarEventoImportadoUI;
import eventoscientificos.imports.BINFile;
import eventoscientificos.legacy.CarregamentoDados;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Nuno Silva
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Empresa empresa = new Empresa();
            BINFile bf = new BINFile();

            empresa = bf.readData(empresa); 
            GUI janela = new GUI(empresa);
            janela.setVisible(true);

        } catch (FileNotFoundException ex) {
            System.out.println("Ficheiro bin não encontrado");;
        } catch (IOException ex) {
            System.out.println("Erro ao ler o ficheiro bin");

        } catch (ClassNotFoundException ex) {
            System.out.println("Erro");
        }
    }
    }
        


