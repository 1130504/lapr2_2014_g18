/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;

public class Topico implements Serializable{

    /**
     * Descrição do tópico.
     */
    private String m_strDescricao;

    /**
     * Código ACM do tópico.
     */
    private String m_strCodigoACM;

    /**
     * Constrói uma instância vazia
     */
    public Topico() {
    }

    /**
     * Constrói uma instância que recebe como parâmetros a escriçºao e o código ACM.
     * @param m_strDescricao Descrição
     * @param m_strCodigoACM Código ACM
     */
    public Topico(String m_strDescricao, String m_strCodigoACM) {
        this.m_strDescricao = m_strDescricao;
        this.m_strCodigoACM = m_strCodigoACM;
    }

    /**
     * Devolve o CódigoACM
     * @return CódigoACM
     */
    public String getM_strCodigoACM() {
        return m_strCodigoACM;
    }

    /**
     * Devolve a Descrição
     * @return Descrição
     */
    public String getM_strDescricao() {
        return m_strDescricao;
    }
    

    /**
     * Modifica a Descrição
     * @param strDescricao Descrição
     */
    public void setDescricao(String strDescricao) {
        this.m_strDescricao = strDescricao;
    }

    /**
     * Modifica o Código ACM
     * @param codigoACM Código ACM
     */
    public void setCodigoACM(String codigoACM) {
        this.m_strCodigoACM = codigoACM;
    }

    /**
     * Valida os atributos
     * @return true
     */
    public boolean valida() {
        return this.m_strCodigoACM instanceof String || this.m_strDescricao instanceof String;
    }

    /**
     * Devolve uma descrição textual do Tópico.
     * @return String
     */
    @Override
    public String toString() {
        return "\nCodigo: " + this.m_strCodigoACM + "\nDescrição: " + this.m_strDescricao;
    }

    /**
     * Verifica se o objeto passado por parâmetro é igual ao comparado
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
         if (this == obj) {
            return true;
        } else {
            if (obj instanceof Topico) {
                Topico s = (Topico) obj;
                return this.getM_strCodigoACM().equals(s.m_strCodigoACM)&& this.getM_strDescricao().equals(s.m_strDescricao);
            } else {
                return false;
            }
        }
    }
    
    
}
