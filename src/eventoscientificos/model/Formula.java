/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author Alexandre
 */
public interface Formula{
    
    double calcularPagamento(double nrFull,double nrShort,double nrPoster, Evento e);
    
}
