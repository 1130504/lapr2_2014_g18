/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author Nuno Silva
 */

public class Local
implements Serializable{
    
    private String m_strLocal;
    private static final String  LOCAL_POR_OMISSAO = "local";
    public Local()
    {
        this.m_strLocal = LOCAL_POR_OMISSAO;
    }

    public Local(String m_strLocal) {
        this.m_strLocal = m_strLocal;
    }
    
    public void setLocal(String strLocal)
    {
        m_strLocal = strLocal;
    }

    @Override
    public String toString()
    {
        return m_strLocal;
    }

    @Override
    public boolean equals(Object obj) {
         if (this == obj) {
            return true;
        } else {
            if (obj instanceof Evento) {
                Local e = (Local) obj;
                return (e.toString().equals(this.toString()));
            } else {
                return false;
            }
        }

    }
    }
    

