/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;
import java.util.*;

/**
 *
 * @author Fábio Ferreira <1131242@isep.ipp.pt>
 */
public class Estatistica implements Serializable{
    
    /**
     * atributo com a empresa
     */
    private Empresa m_empresa;
    
    /**
     * todasAsRevisoes
     * @param e
     * @return 
     */
    private List<Revisao> todasAsRevisoes(Evento e) {

        List<Revisao> listaRevisoesTodas = new ArrayList<>();
        List<Evento> listaEvento = this.m_empresa.getRegistaEvento().getListaEventos();

        for (Evento evento : listaEvento) {
            List<Submissao> listaSubmissoes = evento.getListaSubmissoes().getListaSubmissao();
            for (Submissao submissao : listaSubmissoes) {
                List<Revisao> listaRevisoe = submissao.getArtigo().getListaRevisoes();
                for (Revisao revisao : listaRevisoe) {
                    listaRevisoesTodas.add(revisao);
                }
            }
        }

        return listaRevisoesTodas;

    }
    /**
     * calcula a taxa de Aceitacão
     * @param evento
     * @return 
     */
    public double[] taxaDeAceitacao(Evento evento) {
        List<Artigo> listaArtigoTotal = new ArrayList<>();
        for (Submissao sub : evento.getListaSubmissoes().getListaSubmissao()) {
            listaArtigoTotal.add(sub.getArtigo());
        }

        double aceiteShort = 0, aceiteFull = 0, aceitePoster = 0, totalShort = 0, totalFull = 0, totalPoster = 0;

        for (Artigo artigo : listaArtigoTotal) {
           

            if (artigo.getTipo().equals("short")) {
                List<Revisao> lr = artigo.getListaRevisoes();
                for (Revisao r : lr) {
                    if (r.getRecomendacao().equals("Aceite")) {
                        aceiteShort++;
                    }
                    
                totalShort++;    
                }
            }
            if (artigo.getTipo().equals("full")) {
                List<Revisao> lr = artigo.getListaRevisoes();
                for (Revisao r : lr) {
                    if (r.getRecomendacao().equals("Aceite")) {
                        aceiteFull++;
                    }
                    
                totalFull++;    
                }
            }
            if (artigo.getTipo().equals("poster")) {
                List<Revisao> lr = artigo.getListaRevisoes();
                for (Revisao r : lr) {
                    if (r.getRecomendacao().equals("Aceite")) {
                        aceitePoster++;
                    }
                    
                totalPoster++;    
                }
            }

        }

        double[] media = new double[3];
        media[0] = (aceiteShort / totalShort) * 100;
        media[1] = (aceiteFull / totalFull) * 100;
        media[2] = (aceitePoster / totalPoster) * 100;

        return media;
    }
    /**
     * calcula a média dos parametros
     * @param tipo
     * @param evento
     * @return 
     */
    public double[] mediaParametros(String tipo, Evento evento) {

        List<Artigo> listaArtigoTotal = new ArrayList<>();
        for (Submissao sub : evento.getListaSubmissoes().getListaSubmissao()) {
            listaArtigoTotal.add(sub.getArtigo());
        }

        double media1 = 0, media2 = 0, media3 = 0, media4 = 0;
        int c = 0;
        double soma1 = 0, soma2 = 0, soma3 = 0, soma4 = 0;
        int[] vecaux = new int[4];
        for (Artigo artigo : listaArtigoTotal) {
            if (artigo.getTipo().equals(tipo)) {

                List<Revisao> lr = artigo.getListaRevisoes();
                for (Revisao revisao : lr) {
                    vecaux = revisao.getRespostas();
                    soma1 += vecaux[0];
                    soma2 += vecaux[1];
                    soma3 += vecaux[2];
                    soma4 += vecaux[3];
                    c++;
                }
            }
        }
        media1 = soma1 / c;
        media2 = soma2 / c;
        media3 = soma3 / c;
        media4 = soma4 / c;

        double[] vecmedias = {media1, media2, media3, media4};

        return vecmedias;

    }
    /**
     * construtor
     * @param m_empresa 
     */
    public Estatistica(Empresa m_empresa) {

        this.m_empresa = m_empresa;

    }
    /**
     * vai buscar as revisoes de um revisor
     * @param revisor
     * @return 
     */
    public List<Revisao> revisoesDoRevisor(Revisor revisor) {

        List<Evento> listaEventos = this.m_empresa.getRegistaEvento().getListaEventos();
        List<Revisao> listaRevisoes = new ArrayList<>();

        for (Evento e : listaEventos) {
            List<Submissao> listaSubmissoes = e.getListaSubmissoes().getListaSubmissao();

            for (Submissao s : listaSubmissoes) {
                
                List<Revisao> lr = s.getArtigo().getListaRevisoes();
                for (Revisao revisao : lr) {

                    if (revisao.getM_revisor().getUtilizador().getM_strUsername().equals(revisor.getUtilizador().getM_strUsername())) {
                        listaRevisoes.add(revisao);
                    }
                }
                 
            }

        }
        
        return listaRevisoes;
    }
    /**
     * calcula a media de todos os artigos
     * @return 
     */
    public double mediaDeTodosArtigos() {

        List<Evento> listaEventos = this.m_empresa.getRegistaEvento().getListaEventos();
        int c = 0;
        double soma = 0;
        int[] vec = new int[4];

        for (Evento e : listaEventos) {
            List<Submissao> listaSubmissoes = e.getListaSubmissoes().getListaSubmissao();
            for (Submissao s : listaSubmissoes) {
                List<Revisao> listaRevisoes = s.getArtigo().getListaRevisoes();
                for (Revisao r : listaRevisoes) {

                    vec = r.getRespostas();
                    soma += vec[0] + vec[1] + vec[2] + vec[3];
                    c += 4;

                }
            }
        }

        return soma / c;

    }
    /**
     * calcula o desvio
     * @param G
     * @param Cij
     * @return 
     */
    public double calculoDoDesvio(double G, double Cij) {

        return Math.abs(G - Cij);

    }
    
    /**
     * calcula media do Artigo
     * @param revisao
     * @return 
     */
    public double mediaDoArtigo(Revisao revisao) {
        double soma =0;
               soma+=revisao.getRespostas()[0];
               soma+=revisao.getRespostas()[1];
               soma+=revisao.getRespostas()[2];
               soma+=revisao.getRespostas()[3];

        return soma / 4;
    }
    /**
     * calcula todos os desvios
     * @param listaRevisoes
     * @return 
     */
    public double[] calculaTodosDesvios(List<Revisao> listaRevisoes) {

        double mediaTotal = mediaDeTodosArtigos();
        double[] desvios = new double[listaRevisoes.size()];
        int c = 0;
        for (Revisao r : listaRevisoes) {
            desvios[c] = Math.abs(mediaTotal - mediaDoArtigo(r));
            
            c++;
        }

        return desvios;

    }
    /**
     * faz a media dos desvios
     * @param desvios
     * @return 
     */
    public double mediaDosDesvios(double[] desvios) {

        int c = 0; 
        double soma = 0;

        for (int i = 0; i < desvios.length; i++) {

            soma += desvios[i];
            c++;
        }

        return soma / c;

    }
    /**
     * calcula o S
     * @param desvios
     * @return 
     */
    public double calculaS(double[] desvios) {

        double media = mediaDosDesvios(desvios), somatorio = 0;

        for (int i = 0; i < desvios.length; i++) {

            somatorio += desvios[i] - (media * media);

        }

        return (desvios.length * (somatorio)) / (desvios.length * (desvios.length - 1));
    }
    /**
     * calcula o Z
     * @param mediaDesvios
     * @param s
     * @param desvios
     * @return 
     */
    public double calculaZ(double mediaDesvios, double s, double[] desvios) {

        return (mediaDesvios - 1) / (s / (Math.sqrt(desvios.length)));

    }
    /**
     * comparar com a zona de restrição
     * @param Z
     * @return 
     */
    public String aceitaRejeita(double Z) {
        String decide = "";

        if (Z < 1.645) {
            decide = "Aceite";
        } else {
            decide = "Rejeitado";
        }

        return decide;
    }

}
