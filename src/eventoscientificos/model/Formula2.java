/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author Alexandre
 */
public class Formula2 implements Formula, Serializable{

   private double valorDoPagamento;
    
    private Evento evento;
    
    @Override
    public double calcularPagamento(double nrFull,double nrPoster,double nrShort, Evento e) {
        
        
        this.evento = e;
        
        //Falta implementar a aparte de ir buscar o evento
        
        if(nrShort > 0 && nrPoster > 0){
           return this.valorDoPagamento = (this.evento.getValorFull()*nrFull)+
                    (this.evento.getValorShort()*(nrShort-(nrFull/2)))+
                    (this.evento.getValorPoster()*(nrPoster-(nrFull/2)));
        } else if (nrShort == 0 && nrPoster > 0){
           return this.valorDoPagamento = (this.evento.getValorFull()*nrFull)+
                    (this.evento.getValorPoster()*(nrPoster-(nrFull/2)));
        }else if (nrShort > 0 && nrPoster == 0){
           return this.valorDoPagamento = (this.evento.getValorFull()*nrFull)+
                    (this.evento.getValorShort()*(nrShort-(nrFull/2)));
        }return 0;
        
    }

    public double getValorDoPagamento() {
        return this.valorDoPagamento;
    }

   
    
    
    
}
