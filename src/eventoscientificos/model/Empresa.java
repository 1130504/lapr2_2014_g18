package eventoscientificos.model;

import java.io.Serializable;
import java.util.*;

/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */
public class Empresa implements Serializable{

    /**
     * Lista Utilizadores.
     */
    private ListaUtilizadores m_listaUtilizadores;
    /**
     * RegistoEventos.
     */
    private RegistoEventos m_registaEvento;

    /**
     * Lista de Formulas de pagamento
     */
    private List<Formula> m_listaFormulas;

    /**
     * Constrói uma instância Empresa portadora de uma lista de Utilizadores e
     * de Eventos.
     */
    public Empresa() {
        m_listaUtilizadores = new ListaUtilizadores();
        m_registaEvento = new RegistoEventos();

       // fillInData();
    }

    /**
     * Método passa Classe RegistaEvento
     *
     * @param e
     * @return
     */
    private boolean addEvento(Evento e) {
        return m_registaEvento.getListaEventos().add(e);
    }

    /**
     * Retorna Lista de Eventos que pode submeter. MÉTODO ALTERADO!!
     *
     * @return
     */
    public List<Evento> getListaEventosPodeSubmeter() {
        List<Evento> le = new ArrayList<Evento>();

        for (Evento e : m_registaEvento.getListaEventos()) {
            if (e.aceitaSubmissoes()) {
                le.add(e);
            }
        }

        return le;
    }

    private void fillInData() {
        int max_users = 50;
        int max_organizadores = 5;
        int max_revisores = 25;
        int max_submissoes = 10;
        for (int users = 0; users < max_users; users++) {
            String id = "user" + users;
            String ds = "Utilizador " + users;

            Utilizador u = new Utilizador(id, "12345", ds, id + "@xxx.pt");
            this.m_listaUtilizadores.add(u);

            // System.out.println( u );
        }

        Evento e1 = new Evento("ESOFT Workshop", "Workshop sobre Engenharia de Software");
        this.m_registaEvento.addEvento(e1);

        //System.out.println("Organizadores de evento1");
        for (int organizadores = 0; organizadores < max_organizadores; organizadores++) {
            Utilizador utl = this.m_listaUtilizadores.get(organizadores);

            //System.out.println(utl.toString());
            e1.getM_listaOrganizadores().addOrganizador(utl.getUsername(), utl);

            //System.out.println( utl.toString() );
        }
        CP cp = e1.novaCP();
        for (int revisores = 0; revisores < max_revisores; revisores++) {
            Utilizador utl = this.m_listaUtilizadores.get(revisores);
            Revisor r = cp.addMembroCP(utl.getUsername(), utl);
            cp.registaMembroCP(r);
        }
        e1.setCP(cp);

        for (int submissoes = 0; submissoes < max_submissoes; submissoes++) {
            Submissao sub = e1.getListaSubmissoes().novaSubmissao();

            Artigo art = sub.novoArtigo();

            art.setTitulo("Artigo " + submissoes);
            sub.setArtigo(art);
            e1.getListaSubmissoes().addSubmissao(sub);
        }
    }

    public RegistoEventos getRegistaEvento() {
        return m_registaEvento;
    }

    public Utilizador getUtilizador(String strID) {
        return m_listaUtilizadores.getUtilizador(strID);
    }

    public ListaUtilizadores getM_listaUtilizadores() {
        return m_listaUtilizadores;
    }

    public void setM_listaFormulas(List<Formula> m_listaFormulas) {
        this.m_listaFormulas = m_listaFormulas;
    }

    public List<Formula> getFormulasPagamento() {
        Formula1 f1 = new Formula1();
        Formula2 f2 = new Formula2();

        List<Formula> listFormulas = new ArrayList<>();
        listFormulas.add(f1);
        listFormulas.add(f2);

        return listFormulas;
    }

}
