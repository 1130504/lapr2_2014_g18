/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;


import eventoscientificos.model.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import eventoscientificos.pagamentos.*;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Alexandre
 */
public class RegistoAutor implements Serializable{
    
    /**
     * Evento
     */
    private Evento m_evento;
  
    /**
     * Autor
     */
    private Autor autor;
    
    
    /**
     * Sistema de pagamento
     */
    private SistemaPagamento sistemaPagamento;
    
    /**
     * Constrói uma instância de Registo
     * @param e
     */
   public RegistoAutor(Evento e) {
        this.m_evento= e;
        
    }
   
    /**
     * Modifica o Sistema de pagamento
     * @param sistema sistema de pagamento
     */ 
    public void setSistemaPagamento(SistemaPagamento sistema){
        this.sistemaPagamento = sistema;
    }
    
    
   
    @Override
    public boolean equals(Object obj) {
         if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RegistoAutor registo = (RegistoAutor) obj;
        return Objects.equals(this.autor, registo.autor);
    }

    
    
    
}
