/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Pedro Miguel Gomes < aka Dante w-email: 1130383@isep.ipp.pt>
 *
 */
public class RegistoEventos implements Serializable{

    /**
     * Lista de Eventos.
     */
    private List<Evento> listaEventos;

    /**
     * Constrói uma instância Registo Eventos portadora da lista de Eventos.
     */
    public RegistoEventos() {

        listaEventos = new ArrayList<Evento>();
    }

    /**
     * Retorna novo Evento.
     *
     * @return
     */
    public Evento novoEvento() {
        return new Evento();
    }

    /**
     * Valida Evento, retorna true em caso de sucesso.
     *
     * @param e
     * @return
     */
    public boolean validaEvento(Evento e) {
        return e instanceof Evento;

    }

    /**
     * Adiciona Evento à lista de Eventos, retorna true se executado com
     * sucesso.
     *
     *
     * @param e
     * @return
     */
    public boolean addEvento(Evento e) {

        return listaEventos.add(e);

    }

    /**
     * Regista Evento somente se passar validações Método alterado.
     *
     * @param e
     * @return
     */
    public boolean registoEventos(Evento e) {
        if (e.valida() && validaEvento(e)) {
            return addEvento(e);
        } else {
            return false;
        }
    }

    /**
     * Retorna Lista de Eventos.
     *
     * @return
     */
    public List<Evento> getListaEventos() {

        return listaEventos;
    }

    /**
     * Altera/Define nova lista de Eventos.
     *
     * @param le
     */
    public void setListaEventos(List<Evento> le) {
        listaEventos = le;
    }

    /**
     * Retorna uma String dos Títulos dos Eventos da Lista de Eventos.
     *
     * @return
     */
    @Override
    public String toString() {
        String str = "";
        for (Evento e : this.listaEventos) {
            str += e.toString();
        }

        return str;

    }

    /**
     * equals
     *
     * @param re
     * @return
     */
    public boolean equals(RegistoEventos re) {
        boolean value = false;
        if (listaEventos.size() == re.getListaEventos().size()) {
            int i = 0;
            for (Evento e : listaEventos) {
                if (e.equals(re.getListaEventos().get(i))) {
                    i++;
                    value = true;
                } else {
                    value = false;

                }

            }
        } else {
            value = false;

        }
        return value;

    }

    /**
     * Obtém os Eventos para aquele Organizador. MÉTODO ALTERADO!!
     *
     * @param strId
     * @return
     */
    public List<Evento> getEventosOrganizador(String strId, Empresa ep) {
        List<Evento> leOrganizador = new ArrayList<Evento>();

        Utilizador u = ep.getM_listaUtilizadores().getUtilizador(strId);

        if (u != null) {
            for (Iterator<Evento> it = this.getListaEventos().listIterator(); it.hasNext();) {
                Evento e = it.next();

                List<Organizador> lOrg = e.getM_listaOrganizadores().getListaOrganizadores();

                boolean bRet = false;
                for (Organizador org : lOrg) {
                    if (org.getUtilizador().equals(u)) {
                        bRet = true;
                        break;
                    }
                }
                if (bRet) {
                    leOrganizador.add(e);
                }
            }
        }
        return leOrganizador;
    }

    public List<Evento> getEventosRevisor(String strId, Empresa ep) {
        List<Evento> leRevisor = new ArrayList<>();

        Utilizador u = ep.getM_listaUtilizadores().getUtilizador(strId);

        if (u != null) {
            for (Iterator<Evento> it = this.getListaEventos().listIterator(); it.hasNext();) {
                Evento e = it.next();
                CP cp = e.getCP();
                List<Revisor> listaRevisores = cp.getListaRevisores();

                boolean bRet = false;
                for (Revisor rev : listaRevisores) {
                    if (rev.getUtilizador().equals(u)) {
                        bRet = true;
                        break;
                    }
                }
                if (bRet) {
                    leRevisor.add(e);
                }
            }
        }
        return leRevisor;
    }

}
