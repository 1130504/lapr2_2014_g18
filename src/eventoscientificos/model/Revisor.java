/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Nuno Silva
 */
public class Revisor implements Serializable{

    /**
     * Nome do Revisor
     */
    private String m_strNome;

    /**
     * Utilizador
     */
    private Utilizador m_utilizador;

    /**
     * Lista de tópicos
     */
    private ListaTopicos m_listaTopicos = new ListaTopicos();

    /**
     * Lista de Eventos
     */
    private List<Evento> m_listaEvento;

    /**
     * Lista de Revisoes
     */
    private List<Revisao> m_listaRevisao;

    /**
     * Constrói uma intância de Revisor recebendo por parâmetro o utilizador.
     *
     * @param u Utilizador
     */
    public Revisor(Utilizador u) {
        m_strNome = u.getNome();
        m_utilizador = u;
        m_listaEvento = new ArrayList<Evento>();
    }

    /**
     * Devolve a lista de tópicos do Revisor
     *
     * @return lista de tópiocs
     */
    public ListaTopicos getM_listaTopicos() {
        return m_listaTopicos;
    }

    /**
     * Devolve a lista de eventos
     *
     * @return
     */
    public List<Evento> getM_listaEvento() {
        return m_listaEvento;
    }

    /**
     * Modifica a lista de eventos
     *
     * @param m_listaEvento List de Eventos
     */
    public void setM_listaEvento(List<Evento> m_listaEvento) {
        this.m_listaEvento = m_listaEvento;
    }

    /**
     * Modific a lista de Tópicos
     *
     * @param listaTopicos Lista de Tópicos
     */
    public void setListaTopicos(ListaTopicos listaTopicos) {
        m_listaTopicos.addAll(listaTopicos.getTopicos());
    }

    /**
     * Valida
     *
     * @return boolean
     */
    public boolean valida() {
        return this.m_listaTopicos != null;
    }

    /**
     * Devolve o nome do Revisor
     *
     * @return String
     */
    public String getNome() {
        return m_strNome;
    }

    /**
     * Devolve um Utilizador
     *
     * @return Utilizador
     */
    public Utilizador getUtilizador() {
        return m_utilizador;
    }

    /**
     * Devolve uma descrição textual do Revisor
     *
     * @return String
     */
    @Override
    public String toString() {
        String strRevisor = m_utilizador.toString() + ": ";

        String strTopicos = "";
        for (Topico t : m_listaTopicos.getTopicos()) {
            strTopicos += t.toString();
        }

        strRevisor += strTopicos;

        return strRevisor;
    }

    /**
     * Atribui um evento ao revisor
     * @param e
     */
    public void addEvento(Evento e) {
        this.m_listaEvento.add(e);
    }

    /**
     * @return the m_listaRevisao
     */
    public List<Revisao> getM_listaRevisao() {
        return m_listaRevisao;
    }
    
     /**
     * Verifica se os dois objetos são iguais
     *
     * @param obj Objeto a comparar
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Revisor revisor = (Revisor) obj;
        return Objects.equals(this.m_utilizador, revisor.m_utilizador);
        
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.m_utilizador);
        hash = 43 * hash + Objects.hashCode(this.m_listaTopicos);
        return hash;
    }
}
