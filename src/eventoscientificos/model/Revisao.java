/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author Daniela Maia <1130588@isep.ipp.pt>
 */
public class Revisao implements Serializable{

    private int confianca;
    private int adequacao;
    private int originalidade;
    private int qualidade;
    private String texto;
    private String recomendacao;
    private Revisor m_revisor;

    public Revisao() {
    }

    public Revisao(int confianca, int adequacao, int originalidade, int qualidade, String recomendacao, Revisor m_revisor) {
        this.confianca = confianca;
        this.adequacao = adequacao;
        this.originalidade = originalidade;
        this.qualidade = qualidade;
        this.recomendacao = recomendacao;
        this.m_revisor = m_revisor;
    }

    public String toString() {

        return "Confiança do revisor nos tópicos do artigo: " + this.confianca + "\n"
                + "\nAdequação ao evento: " + this.adequacao + "\nOriginalidade do artigo: "
                + this.originalidade + "\nQualidade da apresentação: " + this.qualidade + "\nRecomendação global: "
                + recomendacao + "\nTexto justificativo: \n" + texto;

    }

    public int getConfianca() {
        return confianca;
    }

    public int getAdequacao() {
        return adequacao;
    }

    public int getOriginalidade() {
        return originalidade;
    }

    public int getQualidade() {
        return qualidade;
    }

    public String getRecomendacao() {
        return recomendacao;
    }

    public Revisor getM_revisor() {
        return m_revisor;
    }

    public String getTexto() {
        return texto;
    }

    
    public void setConfianca(int confianca) {
        this.confianca = confianca;
    }

    public void setAdequacao(int adequacao) {
        this.adequacao = adequacao;
    }

    public void setOriginalidade(int originalidade) {
        this.originalidade = originalidade;
    }

    public void setQualidade(int qualidade) {
        this.qualidade = qualidade;
    }

    public void setRecomendacao(String recomendacao) {
        this.recomendacao = recomendacao;
    }

    public void setM_revisor(Revisor m_revisor) {
        this.m_revisor = m_revisor;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
    
    public void setDados(int[] dados, String recomendacao){
        this.confianca = dados[0];
        this.adequacao = dados[1];
        this.originalidade = dados[2];
        this.qualidade = dados[3];
        this.recomendacao = recomendacao;
    }
    public int[] getRespostas(){
        int r [] = {this.confianca, this.adequacao, this.originalidade, this.qualidade};
        return r;
    }
    

}
