/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class Distribuicao implements Serializable{

    /**
     * Artigo da instância distribuição.
     */
    private Artigo m_artigo;
    /**
     * Lista de Revisores da distribuição.
     */
    private List<Revisor> listaRevisores;

    /**
     * Constrói uma instância de Distribuição com artigo e lista de revisores
     * passada por parâmetro
     *
     * @param artigo
     * @param listaRevisores
     */
    public Distribuicao(Artigo artigo, ArrayList<Revisor> listaRevisores) {
        this.m_artigo = artigo;
        this.listaRevisores = listaRevisores;
    }

    /**
     * Construtor vazio
     */
    public Distribuicao() {
        this.m_artigo = new Artigo();
        this.listaRevisores = new ArrayList();
    }

    /**
     * Devolve o artigo
     *
     * @return
     */
    public Artigo getArtigo() {
        return this.m_artigo;
    }

    /**
     * Devolve a lista de Revisores
     *
     * @return
     */
    public List<Revisor> getListaRevisor() {
        return this.listaRevisores;
    }

    /**
     * Modifica o artigo
     *
     * @param artigo
     */
    public void setArtigo(Artigo artigo) {
        this.m_artigo = artigo;
    }

    /**
     * Modifica a lista de revisores
     *
     * @param listaRevisores
     */
    public void setListaRevisores(List<Revisor> listaRevisores) {
        this.listaRevisores = new ArrayList(listaRevisores);
    }

    /**
     * Devolve a descrição textual da Distribuição
     *
     * @return
     */
    @Override
    public String toString() {
        String revisores = "";
        for (Revisor revisor : this.listaRevisores) {
            revisores += revisor.getNome() + "\n";
        }
        return "Distribuição do artigo: \n" + this.m_artigo.getTitulo() + "\n"
                + "Lista de Revisores: \n"
                + revisores;
    }

    /**
     * Compara a Distribuição com o objeto recebido.
     *
     * @param obj o objeto a comparar
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else {
            Distribuicao d = (Distribuicao) obj;
            return this.listaRevisores.equals(d.listaRevisores) && this.m_artigo.equals(d.m_artigo);
        }
    }
}
