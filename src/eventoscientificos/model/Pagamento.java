/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import eventoscientificos.controller.RegistoAutorController;
import eventoscientificos.pagamentos.*;
import eventoscientificos.pagamentos.SistemaPagamento;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import java.util.Date;

/**
 *
 * @author Alexandre
 */
public class Pagamento {
    
    /**
     * Evento
     */
    private Evento m_evento;
    
    private ProcessoPagamento m_processo;
    
    /**
     * Data de Validade do CC
     */
    private Date m_data;

    /**
     * Valor de Pagamento
     */
    private double m_valor;

    /**
     * Número do Cartão de Crédito
     */
    private String m_numeroCC;
     Format format = new SimpleDateFormat("yyyy-MM-dd");
    

    public Pagamento(ProcessoPagamento pg,Evento e) {
        this.m_evento = e;
        this.m_processo = pg;
        
    }
    
     public void setNumeroCartaoCredito(String num) {
        this.m_numeroCC = num;
    }
    
    public void setDataValidadeCC(Date data){
        this.m_data = data;
    }

    public boolean sistemaPagamento(String sistema, String numeroCartao, Date dataValidade) {

        this.m_numeroCC = numeroCartao;
        this.m_data = dataValidade;
        
        if (sistema.equalsIgnoreCase("CanadaExpress")) {
            PagamentoCanadaExpress canadaExpress = new PagamentoCanadaExpress(this.m_data, this.m_numeroCC, (float)this.m_processo.getM_valor(), this.m_evento.getDataLimiteRegisto());
             if (canadaExpress.iniciaPedido()) {
                return canadaExpress.pagamento();
                 
            } 
        } else if (sistema.equalsIgnoreCase("VisaoLight")){
            PagamentoVisaoLight visaoLight = new PagamentoVisaoLight(this.m_numeroCC, format.format(this.m_data), (float)this.m_processo.getM_valor(), this.m_evento.getDataLimiteRegisto().toString());
           
            return visaoLight.pagamento();
        }
        return false;
    }


    @Override
    public String toString() {
        return "Data de Validade de CC: "+ format.format(this.m_data) + "\nNúmero do Cartão: "+
                this.m_numeroCC;
    }   
    
    public void escreverFicheiroCSV(RegistoProcessoPagamento registo){
        try{
            DateFormat formatoData = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        
        
        
        FileWriter ficheiro = new FileWriter("RegistoAutoresOutput.csv");
        BufferedWriter escrever = new BufferedWriter( ficheiro );
        for (int i = 0; i < registo.getM_listaSubmissoesAutor().getListaSubmissao().size(); i++) {
            for (int j = 0; j < registo.getM_listaSubmissoesAutor().getListaSubmissao().get(i).getArtigo().getM_listaAutores().size(); j++) {
                escrever.write("Autor: "+registo.getM_listaSubmissoesAutor().getListaSubmissao().get(i).getArtigo().getM_listaAutores().get(j).getM_strNome().toString()+
                        "\nArtigo: "+registo.getM_listaSubmissoesAutor().getListaSubmissao().get(i).getArtigo().getTitulo().toString()+
                "\nData de Pagamento: "+formatoData.format(cal.getTime()));
            }
            
        }
        
        
        
        escrever.flush();
        escrever.close();
    } catch(Exception e){
        
    }
        }
        
}
