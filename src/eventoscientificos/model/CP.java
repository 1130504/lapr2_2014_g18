package eventoscientificos.model;


import java.io.Serializable;
import java.util.*;

/**
 *
 * @author PedroGomes<1130383@isep.ipp.pt>
 */

public class CP implements Serializable{
    private List<Revisor> m_listaRevisores;

    public CP()
    {
        m_listaRevisores = new ArrayList();
    }

    public Revisor addMembroCP( String strId, Utilizador u )
    {
        Revisor r = new Revisor(u);
        
        if( r.valida() && validaMembroCP(r) )
            return r;
        else
            return null;
    }
     /**
      * 
      * @param id
      * @return 
      */
    public Revisor getMembroCP(String id) {
        for (Revisor r : this.m_listaRevisores) {
            if(r.getUtilizador().getUsername().equals(id)) {
                return r;
            }
        }
        return null;
    }
    
    private boolean validaMembroCP(Revisor r)
    {
        return true;
    }
    
    public boolean registaMembroCP(Revisor r)
    {
        return getM_listaRevisores().add(r);
    }
    
    @Override
    public String toString()
    {
        String strCP = "Membros de CP: ";
        for( ListIterator<Revisor> it = getM_listaRevisores().listIterator(); it.hasNext(); )
        {
            strCP += it.next().toString();
            if( it.hasNext() )
                strCP += ", ";
        }
        return strCP;
    }

    public List<Revisor> getListaRevisores() 
    {
        return this.getM_listaRevisores();
    }

    /**
     * @return the m_listaRevisores
     */
    public List<Revisor> getM_listaRevisores() {
        return m_listaRevisores;
    }
}