/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import eventoscientificos.model.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Alexandre
 */
public class Registo implements Serializable{
    
    /**
     * Evento
     */
    private Evento m_evento;
    
     /**
     * Lista de Submissões
     */
    private ListaSubmissoes listaSubmissoes;
    
    /**
     * Autor
     */
    private Autor m_autor;
    
    /**
     * Empresa
     */
    private Empresa m_empresa;
    
    /**
     * Número de artigos full
     */
    private int numeroFull;
    
    /**
     * Numero de artigos poster
     */
    private int numeroShort;
    
    /**
     * Numero de artigos Poster
     */
    private int numeroPoster;
    
    
    
    /**
     * Constrói uma instância de Registo
     * @param e
     */
    public Registo(Evento e) {
        this.m_evento= e;
    }
    
    
    public ListaSubmissoes obterSubmissoes(){
        
        return this.listaSubmissoes =this.m_evento.getListaSubmissoes();
    }
    
    public void verficaArtigo(){
        this.numeroFull=0;
        this.numeroPoster=0;
        this.numeroShort=0;
        for (int i = 0; i < this.listaSubmissoes.getListaSubmissao().size(); i++) {
            String tipo = this.listaSubmissoes.getListaSubmissao().get(i).getArtigo().getTipo();
            if (tipo.equalsIgnoreCase("Short")){
                this.numeroShort++;
            }else if(tipo.equalsIgnoreCase("Full")){
                this.numeroFull++;
            }else if(tipo.equalsIgnoreCase("Poster")){
                this.numeroPoster++;
            }
        }
    }

    public int getNumeroFull() {
        return this.numeroFull;
    }

    public int getNumeroPoster() {
        return this.numeroPoster;
    }

    public int getNumeroShort() {
        return this.numeroShort;
    }
    
    
    
}
