/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author 1131242@isep.ipp.pt
 */

public class RegistoOrganizadores implements Serializable {
    /**
     * Variavel que guarda a lista de organizadores
     */
    private List<Organizador> m_listaOrganizadores;
    
    
    /**
     * construtor
     */
    public RegistoOrganizadores() {
        this.m_listaOrganizadores = new ArrayList<Organizador>();
    }
    
    
    /**
     * Adicionaorganizador atraves de do id
     * @param strId
     * @param u
     * @return 
     */
    public boolean addOrganizador(String strId, Utilizador u) {
        Organizador o = new Organizador(strId, u);

        o.valida();

        return addOrganizador(o);
    }
    /**
     * Adiciona o organizador atraves do objeto
     * @param o
     * @return 
     */
    public boolean addOrganizador(Organizador o)
    {
        return getM_listaOrganizadores().add(o);
    }
    /**
     * 
     * @return A lista de organizadores
     */
    public List<Organizador> getListaOrganizadores() {
        List<Organizador> lOrg = new ArrayList<Organizador>();

        for (ListIterator<Organizador> it = getM_listaOrganizadores().listIterator(); it.hasNext();) {
            lOrg.add(it.next());
        }

        return lOrg;
    }

    /**
     * @return the m_listaOrganizadores
     */
    public List<Organizador> getM_listaOrganizadores() {
        return this.m_listaOrganizadores;
    }

    /**
     * @param m_listaOrganizadores the m_listaOrganizadores to set
     */
    public void setM_listaOrganizadores(List<Organizador> m_listaOrganizadores) {
        this.m_listaOrganizadores = m_listaOrganizadores;
    }
    
    /**
     * Retorna os organizadores em string
     * @return 
     */
    @Override
    public String toString () {
        
        String str = "";
        
        for (int i = 0; i < this.m_listaOrganizadores.size(); i++) {
            
            str += i+1 + ". " + this.m_listaOrganizadores.get(i).getNome() + "; \n";
            
        }
        
        return str;
    }
    
    /**
     * Verifica se são iguais
     * @param ro
     * @return 
     */
    public boolean equals (RegistoOrganizadores ro) {
        boolean b=false;
        if (this.getM_listaOrganizadores().size() == ro.getM_listaOrganizadores().size()) {
            for (int i = 0; i < ro.getM_listaOrganizadores().size(); i++) {
                if (this.getM_listaOrganizadores().get(i).equals(ro.getM_listaOrganizadores().get(i))) {
                    b=true;
                } else {
                    b=false;
                    break;
                } 
                
                
            }
        }else  b= false;
        
        return b;
    }
}
