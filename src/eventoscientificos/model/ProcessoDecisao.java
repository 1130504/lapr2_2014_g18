/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class ProcessoDecisao implements Serializable{
      private ArrayList<Decisao> listaDecisoes;

    public ProcessoDecisao() {
        listaDecisoes = new ArrayList<>();
    }

    public ProcessoDecisao(ArrayList<Decisao> listaDecisoes) {
        this.listaDecisoes = listaDecisoes;
    }
    
    /**
     * Devolve a lista de decisões do evento
     * @return
     */
    public ArrayList<Decisao> getListaDecisao() {
        return this.listaDecisoes;
    }
    
    public boolean addDecisao(Decisao d){
        return this.listaDecisoes.add(d);
    }
    
}
