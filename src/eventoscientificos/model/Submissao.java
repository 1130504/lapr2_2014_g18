package eventoscientificos.model;

import eventoscientificos.submissaostate.SubmissaoState;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Classe responsável pela submissão de Artigos
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class Submissao implements Serializable{

    /**
     * Artigo da Submissão
     */
    private Artigo m_artigo;
    /**
     * Estado da Submissão
     */
    private SubmissaoState estado;
    
    /**
     * Lista de revisoes do artigo
     */
    
    private List<Revisao> listaRevisoes;
    
    /**
     * Construtor vazio
     */
    public Submissao() {

    }

    /**
     * Construtor com Artigo passado por parâmetro
     *
     * @param m_artigo
     */
    public Submissao(Artigo m_artigo) {
        this.m_artigo = m_artigo;
    }

    /**
     * Devolve o atributo Artigo
     *
     * @return Artigo
     */
    public Artigo getArtigo() {
        return this.m_artigo;
    }

    /**
     * Devolve o estado em que se encontra a submissão
     *
     * @return SubmissaoState
     */
    public SubmissaoState getState() {
        return this.estado;
    }

    /**
     * Modifica a variável Artigo
     *
     * @param artigo
     */
    public void setArtigo(Artigo artigo) {
        this.m_artigo = artigo;
    }

    /**
     * Modifica a variável Estado
     *
     * @param estado
     */
    public void setEstado(SubmissaoState estado) {
        this.estado = estado;
    }

    

    /**
     * Valida
     *
     * @return
     */
    public boolean valida() {
        return this.m_artigo != null;
    }

    /**
     * Cria uma nova instância de Artigo
     *
     * @return Artigo
     */
    public Artigo novoArtigo() {
        return new Artigo();
    }

    /**
     * Devolve a descrição do Artigo
     *
     * @return String
     */
    public String getInfo() {
        return this.toString();
    }

    /**
     * Devolve a descrição textual da Submissão
     *
     * @return String
     */
    @Override
    public String toString() {
        return "Submissão:\n" + m_artigo.getInfo();
    }

    /**
     * Verifica se o objeto passado por parâmetro é igual ao comparado
     *
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else {
            if (obj instanceof Submissao) {
                Submissao s = (Submissao) obj;
                return this.m_artigo.equals(s.m_artigo);
            } else {
                return false;
            }
        }
    }

    /**
     * Devolve o hashcode do objeto
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.m_artigo);
        return hash;
    }

    /**
     * @return the listaRevisoes
     */
    public List<Revisao> getListaRevisoes() {
        return listaRevisoes;
    }

    /**
     * @param listaRevisoes the listaRevisoes to set
     */
    public void setListaRevisoes(List<Revisao> listaRevisoes) {
        this.listaRevisoes = listaRevisoes;
    }
    
 
    public void setState(SubmissaoState state) {
        this.estado = state;
    }

    public boolean setAceite() {
        return this.estado.setAceite();
    }

    public boolean setCriada() {
        return this.estado.setCriada();
    }

    public boolean setDistribuida() {
        return this.estado.setDistribuida();
    }

    public boolean setRejeitada() {
        return this.estado.setRejeitada();
    }

    public boolean setRevista() {
        return this.estado.setRevista();
    }

    public boolean setSubmetidaParaRevisao() {
        return this.estado.setSubmetidoParaRevisao();
    }
}
