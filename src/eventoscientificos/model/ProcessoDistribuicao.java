/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class ProcessoDistribuicao implements Serializable {

    /**
     * lista de distribuições
     */
    private List<Distribuicao> m_listaDistribuicoes;

    /**
     * Constrói uma instância do Processo de Distribuição recebendo uma lista de
     * distribuições por parâmetro.
     *
     * @param listaDistribuicoes
     */
    public ProcessoDistribuicao(ArrayList<Distribuicao> listaDistribuicoes) {
        this.m_listaDistribuicoes = listaDistribuicoes;
    }

    /**
     * Construtor vazio
     */
    public ProcessoDistribuicao() {
        this.m_listaDistribuicoes = new ArrayList();
    }

    /**
     * Devolve a lista de distribuicoes
     *
     * @return
     */
    public List<Distribuicao> getListaDistribuicoes() {
        return this.m_listaDistribuicoes;
    }

    /**
     * Modifica a lista de distribuicoes
     *
     * @param listaDistribuicoes
     */
    public void setListaDistribuicoes(ArrayList<Distribuicao> listaDistribuicoes) {
        this.m_listaDistribuicoes = listaDistribuicoes;
    }

    /**
     * Devolve a lista de artigos que um revisor, passado por parâmetro, é
     * encarregue de rever
     *
     * @param r
     * @return
     */
    public List<Artigo> getListaArtigos(Revisor r) {
        List<Artigo> listaArtigos = new ArrayList();
        for (Distribuicao d : this.m_listaDistribuicoes) {
            if (d.getListaRevisor().contains(r)) {
                listaArtigos.add(d.getArtigo());
            }
        }
        return listaArtigos;

    }

    /**
     * Devolve a descrição textual do Processo de Distribuição.
     *
     * @return
     */
    @Override
    public String toString() {
        return "ProcessoDistribuicao[" + "listaDistribuicoes=" + m_listaDistribuicoes + ']';
    }

    /**
     * Compara o ProcessoDistribuicao com o objeto recebido.
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj != this) {
            return false;
        }
        if (obj == null) {
            return false;
        }
        ProcessoDistribuicao pd = (ProcessoDistribuicao) obj;
        return this.m_listaDistribuicoes.equals(pd.m_listaDistribuicoes);
    }
}
