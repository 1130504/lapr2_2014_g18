/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class ListaTopicos implements Serializable {
    
    /**
     * Cria uma Lisa de tópicos.
     */
    private List<Topico> m_listaTopicos;

    /**
     * Constrói uma instância da ListaTopicos.
     */
    public ListaTopicos() {
        this.m_listaTopicos = new ArrayList<Topico>();
    }

    /**
     * Adiciona um tópico recebido por parâmetro à lista de tópicos.
     * @param t tópico.
     * @return lista de tópicos.
     */
    public boolean add(Topico t) {
        try {
            return getTopicos().add(t);
        } catch (Exception e) {
            return false;
        }

    }
    
    /**
     * Valida o tópico
     * @param t
     * @return 
     */
    public boolean validaTopico(Topico t) {
        if (t.valida() && validaGlobalTopico(t)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Verifica se o Tópico não está vazio
     * @param t
     * @return 
     */
    private boolean validaGlobalTopico(Topico t) {
        if(t!=null){
             return true;
        }else
            return false;
       
    }

    /**
     * Devolve a lista de tópicos.
     * @return 
     */
    public List<Topico> getTopicos() {
        return m_listaTopicos;
    }

    /**
     * Adiciona todos os elementos recebidos por parâmetro à lista de tópicos. 
     * @param listaTopicos lista de tópicos
     */
    public void addAll(List<Topico> listaTopicos) {
        this.m_listaTopicos.addAll(listaTopicos);
    }

    /**
     * Determina a quantidade de elemntos na lista.
     * @return um inteiro que é o tamanho da lista.
     */
    public int size() {
        return this.m_listaTopicos.size();
    }

    /**
     * Devolve o tópico correspondente ao índice passado por parâmetro.
     * @param indice índice do tópico
     * @return tópico
     */
    public Topico get(int indice) {
        return this.m_listaTopicos.get(indice);
    }

    /**
     * Devolve uma descrição textual da Lista de Tópicos.
     * @return String
     */
    @Override
    public String toString() {
        String ltStr = "";
        for (Topico topico : this.m_listaTopicos) {
            ltStr += topico.toString()+"\n";
        }
        return ltStr;
    }

    /**
     * Verifica se o objeto passado por parâmetro é igual ao comparado
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else {
            if (obj instanceof ListaTopicos) {
                ListaTopicos lt = (ListaTopicos) obj;
                if (lt.m_listaTopicos.size() == this.m_listaTopicos.size()) {
                    return this.m_listaTopicos.containsAll((Collection<?>) lt);

                } else {
                    return false;
                }

            }else{
                return false;
            }
        }

    }

    /**
     * 
     * Devolve o hashcode do objeto
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.m_listaTopicos != null ? this.m_listaTopicos.hashCode() : 0);
        return hash;
    }
    
}
