/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe representativa de uma lista de utilizadores
 *
 * @author Daniela Maia <1130588@isep.ipp.pt>
 */
public class ListaUtilizadores implements Serializable{

    /**
     * lista onde vão ser armazenados utilizadores e respetivos dados
     */
    private final List<Utilizador> m_listaUtilizadores;

    /**
     * construtor - constroi uma lista de utilizadores
     */
    public ListaUtilizadores() {
        m_listaUtilizadores = new ArrayList<Utilizador>();
    }
    
        public Utilizador novoUtilizador() {
        return new Utilizador();
    }
        
         public boolean registaUtilizador(Utilizador u) {
        if (u.valida() && validaUtilizador(u)) {
            return addUtilizador(u);
        } else {
            return false;
        }
    }
         
         
   

    // alterada na iteração 2
    private boolean validaUtilizador(Utilizador u) {
        for (Utilizador uExistente : getM_listaUtilizadores()) {
            if (uExistente.mesmoQueUtilizador(u)) {
                return false;
            }
        }
        return true;
    }

    public Utilizador getUtilizador(String strId) {
        for (Utilizador u : this.getM_listaUtilizadores()) {
            String s1 = u.getUsername();
            if (s1.equalsIgnoreCase(strId)) {
                return u;
            }
        }
        return null;
    }

    public Utilizador getUtilizadorEmail(String strEmail) {
        for (Utilizador u : this.getM_listaUtilizadores()) {
            String s1 = u.getEmail();
            if (s1.equalsIgnoreCase(strEmail)) {
                return u;
            }
        }

        return null;
    }

    private boolean addUtilizador(Utilizador u) {
        return getM_listaUtilizadores().add(u);
    }

    /**
     *
     * @param utilizador utilizador
     * @return true - se a lista tem utilizadores; false - se a lista está vazia
     */
    public boolean add(Utilizador utilizador) {

        return getM_listaUtilizadores().add(utilizador);
    }

    /**
     *
     * @param username nome de utilizador
     * @param pwd password
     * @param nome nome civil do utilizador
     * @param email email do utilizador
     * @return true - se foi possível adicionar o utilizador; false - se não foi
     * possivel adicionar o utilizador
     */
    public boolean addUtilizador(String username, String pwd, String nome, String email) {
        Utilizador utilizador = new Utilizador(username, pwd, nome, email);

        utilizador.valida();

        return addUtilizador(utilizador);
    }

 

    /**
     * @return the m_listaUtilizadores
     */
    public List<Utilizador> getM_listaUtilizadores() {
        return this.m_listaUtilizadores;
    }

    /**
     *
     * @param utilizadores utilizadores
     * @return o utilizador que estava guardado na posição passada como
     * parâmetro
     */
    public Utilizador get(int utilizadores) {
        return getM_listaUtilizadores().get(utilizadores);

    }

    /**
     *
     * @return descrição textual da classe ListaUtilizadores
     */

    @Override
    public String toString() {
        String str = "";
        for (int i = 0; i < this.getM_listaUtilizadores().size(); i++) {
            str += i+1 + ". " + this.getM_listaUtilizadores().get(i).getNome() + ";\n";

        }

        return str;
    }

    /**
     *
     * @param lista1 lista de utilizadores passada como parametro
     * @return se as listas são iguais retorna true, senão false.
     */
    public boolean equals(ListaUtilizadores lista1) {
        boolean resposta = false;
        if (this.getM_listaUtilizadores().size() == lista1.getM_listaUtilizadores().size()) {
            for (int i = 0; i < lista1.getM_listaUtilizadores().size(); i++) {
                if (this.getM_listaUtilizadores().get(i) == lista1.getM_listaUtilizadores().get(i)) {
                    resposta = true;
                } else {
                    resposta = false;
                    break;
                }
            }
        }
        return resposta;

    }

}
