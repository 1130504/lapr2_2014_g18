/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */
public class Autor implements Serializable{
    private Utilizador m_Utilizador;
    private String m_strNome;
    private String m_strAfiliacao;
    private String m_strEMail;
    
    public Autor()
    {
        this.m_Utilizador = null;
    }
    
    public void setNome(String strNome)
    {
        this.m_strNome = strNome;
    }
    
    public void setAfiliacao(String strAfiliacao)
    {
        this.m_strAfiliacao = strAfiliacao;
    }
    
    public void setEMail(String strEMail)
    {
        this.m_strEMail = strEMail;
    }
    
    public void setUtilizador(Utilizador utilizador)
    {
        this.m_Utilizador = utilizador;
    }
    
    public boolean valida()
    {
        return true;
    }

    public boolean podeSerCorrespondente() 
    {
        return (getM_Utilizador() != null);
    }
    
    @Override
    public String toString()
    {
        return this.getM_strNome() + " - " + this.getM_strAfiliacao() +  " - " + this.getM_strEMail(); 
    }
    
    /**
     * 
     * @param obj
     * @return 
     * 
     * Um Autor é identificado pelo seu endereço de e-mail que deve ser único no artigo, 
     * mas não tem que corresponder a um Utilizador.
     */
    @Override
    public boolean equals(Object obj){
        if(this == obj)
            return true;
        else
            if(obj instanceof Autor) {
                Autor aux = (Autor) obj;
                return this.getM_strEMail().equals(aux.getM_strEMail());
            } else
                return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.getM_Utilizador() != null ? this.getM_Utilizador().hashCode() : 0);
        hash = 41 * hash + (this.getM_strEMail() != null ? this.getM_strEMail().hashCode() : 0);
        return hash;
    }

    /**
     * @return the m_Utilizador
     */
    public Utilizador getM_Utilizador() {
        return m_Utilizador;
    }

    /**
     * @return the m_strNome
     */
    public String getM_strNome() {
        return m_strNome;
    }

    /**
     * @return the m_strAfiliacao
     */
    public String getM_strAfiliacao() {
        return m_strAfiliacao;
    }

    /**
     * @return the m_strEMail
     */
    public String getM_strEMail() {
        return m_strEMail;
    }
    
    
}
