/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class Decisao implements Serializable{

    /**
     * Artigo a que a decisao se refere
     */
    private Artigo m_artigo;
    private String decisao;
    private static final String[] DECISOES = {"Aceite", "Rejeitado"};

    public Decisao(Artigo m_artigo, int decisaoIndex) {
        this.m_artigo = m_artigo;
        this.decisao = DECISOES[decisaoIndex];
    }
    /**
     * Devolve a decisao
     * @return 
     */
    public String getDecisao() {
        return decisao;
    }
    /**
     * Devolve o artigo 
     * @return 
     */
    public Artigo getArtigo() {
        return m_artigo;
    }
    /**
     * Modifica a decisao
     * @param decisaoIndex 
     */
    public void setDecisao(int decisaoIndex) {
        this.decisao = DECISOES[decisaoIndex    ];
    }
    /**
     * Modifica o artigo
     * @param artigo 
     */
    public void setArtigo(Artigo artigo) {
        this.m_artigo = artigo;
    }

}
