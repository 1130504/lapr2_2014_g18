/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Alexandre
 */
public class RegistoProcessoPagamento implements Serializable{

    /**
     * Evento
     */
    private Evento m_evento;

    /**
     * Número de artigos full
     */
    private int numeroFull;

    /**
     * Numero de artigos poster
     */
    private int numeroShort;

    /**
     * Numero de artigos Poster
     */
    private int numeroPoster;

    /**
     * Lista de Submissões
     */
    private ListaSubmissoes m_listaSubmissoes;
    
    /**
     * Lista de Submissões
     */
    private ListaSubmissoes m_listaSubmissoesAutor;

    /**
     * Constrói uma instância de Registo de Pagamento que recbe como parâmetro o
     * Evnto e A Lista de Submissões.
     *
     * @param evento Evento
     * @param listaSubmissoes Lista de Submissões
     */
    public RegistoProcessoPagamento(Evento evento, ListaSubmissoes listaSubmissoes) {
        this.m_evento = evento;
        this.m_listaSubmissoes = listaSubmissoes;
        m_listaSubmissoesAutor = new ListaSubmissoes();
    }

    
    
    

    /**
     * Obtém as Submissões do Autor
     *
     * @param a
     * @return liata de submissoes
     */
    public ListaSubmissoes obterSubmissoes(Autor a) {
        
        for (int i = 0; i < this.m_listaSubmissoes.getListaSubmissao().size(); i++) {
            if(this.m_listaSubmissoes.getListaSubmissao().get(i).getArtigo().getM_listaAutores().contains(a)){
                this.m_listaSubmissoesAutor.add(this.m_listaSubmissoes.getListaSubmissao().get(i));
            }
        }
        return this.m_listaSubmissoesAutor;
    }

   
    /**
     * Devolve o número de artigos do tipo Full
     *
     * @return numero de artigos full
     */
    public int getNumeroFull() {
        return this.numeroFull;
    }

    /**
     * Devolve o número de artigos do tipo Poster
     *
     * @return numero de artigos Poster
     */
    public int getNumeroPoster() {
        return this.numeroPoster;
    }

    /**
     * Devolve o número de artigos do tipo Short
     *
     * @return numero de artigos Short
     */
    public int getNumeroShort() {
        return this.numeroShort;
    }

    /**
     * Devolve a Lista de Submissões
     * @return lista de submissoes
     */
    public ListaSubmissoes getM_listaSubmissoesAutor() {
        return m_listaSubmissoesAutor;
    }
    
     /**
     * Verifica o tipo de artigo e guarda-o numa variável
     * @param listaArtigos
     */
    public void verficaArtigo(ListaSubmissoes listaArtigos) {
        this.m_listaSubmissoesAutor = listaArtigos;
        this.numeroFull = 0;
        this.numeroPoster = 0;
        this.numeroShort = 0;
        for (int i = 0; i < this.m_listaSubmissoesAutor.getListaSubmissao().size(); i++) {
            String tipo = this.m_listaSubmissoesAutor.getListaSubmissao().get(i).getArtigo().getTipo();
            if (tipo.equalsIgnoreCase("Short")) {
                this.numeroShort++;
            } else if (tipo.equalsIgnoreCase("Full")) {
                this.numeroFull++;
            } else if (tipo.equalsIgnoreCase("Poster")) {
                this.numeroPoster++;
            }
        }
    }

     


}
