/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import eventoscientificos.eventostate.EventoState;
import eventoscientificos.eventostate.EventoCriadoState;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Pedro Miguel Gomes < aka Dante w-email: 1130383@isep.ipp.pt>
 */
public class Evento implements Serializable {

    private String m_webpage;
    private String m_strTitulo;
    private String m_strDescricao;
    private Local m_local;
    private Date m_strDataInicio;
    private Date m_strDataFim;
    private Date m_strDataLimiteSubmissão;
    private Date m_strDataLimiteRevisao;
    private Date m_strDataLimiteSubmissaoFinal;
    private Date m_strDataLimiteRegisto;
    private RegistoOrganizadores m_listaOrganizadores;
    private ListaSubmissoes m_listaSubmissoes;
    private CP m_cp;
    private ListaTopicos m_listaTopicos;
    private List<Revisor> m_listaReviroes;
    private List<Revisao> m_listaRevisoes;
    private String m_ano;
    private ProcessoDistribuicao m_processoDistribuicao;
    private ProcessoDecisao m_processoDecisao;
    private ProcessoNotificacao m_processoNotificacao;

    /**
     * Estado do Evento.
     */
    private EventoState m_estado;
    /**
     * Valor de artigos do tipo Short
     */
    private double m_valorShort;
    /**
     * Valor de artigos do tipo Full
     */
    private double m_valorFull;
    /**
     * Valor de artigos do tipo Poster
     */
    private double m_valorPoster;
    
    private Formula m_formula = new Formula1();

    private Formula1 m_formula1 = new Formula1();
    
    private Formula2 m_formula2 = new Formula2();

    private static final int VALOR_POSTER_POR_OMISSAO = 0;
    private static final int VALOR_FULL_POR_OMISSAO = 0;
    private static final int VALOR_SHORT_POR_OMISSAO = 0;

    public Evento() {
        m_local = new Local();
        m_listaOrganizadores = new RegistoOrganizadores();
        m_listaSubmissoes = new ListaSubmissoes();
        m_listaTopicos = new ListaTopicos();
        m_valorFull = VALOR_FULL_POR_OMISSAO;
        m_valorShort = VALOR_SHORT_POR_OMISSAO;
        m_valorPoster = VALOR_POSTER_POR_OMISSAO;
    }

    public Evento(String titulo, String descricao) {
        m_local = new Local();
        m_listaOrganizadores = new RegistoOrganizadores();
        m_listaSubmissoes = new ListaSubmissoes();
        m_listaTopicos = new ListaTopicos();
        this.setTitulo(titulo);
        this.setDescricao(descricao);
    }

    public CP novaCP() {
        m_cp = new CP();

        return m_cp;
    }

    public final void setTitulo(String strTitulo) {
        this.m_strTitulo = strTitulo;
    }

    public final void setDescricao(String strDescricao) {
        this.m_strDescricao = strDescricao;
    }

    public void setDataInicio(Date strDataInicio) {
        this.m_strDataInicio = strDataInicio;
    }

    public void setDataFim(Date strDataFim) {
        this.m_strDataFim = strDataFim;
    }

    // adicionada na iteração 2
    public void setDataLimiteSubmissão(Date strDataLimiteSubmissão) {
        this.setM_strDataLimiteSubmissão(strDataLimiteSubmissão);
    }

    /**
     * Altera/Define local do Evento a realizar.
     *
     * @param m_strlocal
     */
    public void setLocal(String m_strlocal) {
        this.getM_local().setLocal(m_strlocal);
    }

    /**
     * Altera/Define valor dos artigos do tipo Full
     *
     * @param valorFull
     */
    public void setValorFull(double valorFull) {
        this.m_valorFull = valorFull;
    }

    /**
     * Altera/Define valor dos artigos do tipo Poster
     *
     * @param valorPoster
     */
    public void setValorPoster(double valorPoster) {
        this.m_valorPoster = valorPoster;
    }

    /**
     * Altera/Define valor dos artigos do tipo Short
     *
     * @param valorShort
     */
    public void setValorShort(double valorShort) {
        this.m_valorShort = valorShort;
        
    }

    /**
     * Devolve o valor dos artigos do tipo Full
     *
     * @return
     */
    public double getValorFull() {
        return m_valorFull;
    }

    /**
     * Devolve o valor dos artigos do tipo Poster
     *
     * @return
     */
    public double getValorPoster() {
        return m_valorPoster;
    }

    /**
     * Devolve o valor dos artigos do tipo Short
     *
     * @return
     */
    public double getValorShort() {
        return m_valorShort;
    }

    /**
     * ?validações?
     *
     * @return
     */
    public boolean valida() {
        if (this instanceof Evento) {
            return true;
        } else {
            return false;
        }
    }

    public void setCP(CP cp) {
        m_cp = cp;
    }
    /**
     * Devolve a descrição textual do evento
     * @return 
     */
    @Override
    public String toString() {
        String strOrg = "";
        for (Organizador org : this.m_listaOrganizadores.getListaOrganizadores()) {
            strOrg += org.getNome() + ", ";
        }
        return "Titulo: " + getM_strTitulo()+ "\n"
                + "Descrição: " + getM_strDescricao();
                
               
    }
    
    public boolean aceitaSubmissoes() {
        return true;
    }
    
    public ListaSubmissoes getListaSubmissoes() {
        return this.m_listaSubmissoes;
    }

    public CP getCP() {
        return this.m_cp;
    }

    public ListaTopicos getTopicos() {
        return m_listaTopicos;
    }

    // adicionada na iteração 2
    public Topico novoTopico() {
        return new Topico();
    }

    // adicionada na iteração 2
    public boolean addTopico(Topico t) {
        return this.m_listaTopicos.add(t);
    }

    // adicionada na iteração 2
    public boolean validaTopico(Topico t) {
        if (t.valida() && validaGlobalTopico(t)) {
            return true;
        } else {
            return false;
        }
    }

    // adicionada na iteração 2
    private boolean validaGlobalTopico(Topico t) {
        return true;
    }

    public boolean validaLocal(Local l) {
        return true;
    }

    /**
     * @return the m_strTitulo
     */
    public String getM_strTitulo() {
        return m_strTitulo;
    }

    /**
     * @return the m_strDescricao
     */
    public String getM_strDescricao() {
        return m_strDescricao;
    }

    /**
     * @return the m_local
     */
    public Local getM_local() {
        return m_local;
    }

    /**
     * @return the m_strDataInicio
     */
    public Date getM_strDataInicio() {
        return m_strDataInicio;
    }

    /**
     * @return the m_strDataFim
     */
    public Date getM_strDataFim() {
        return m_strDataFim;
    }

    /**
     * @return the m_strDataLimiteSubmissão
     */
    public Date getM_strDataLimiteSubmissão() {
        return m_strDataLimiteSubmissão;
    }

    /**
     * Retorna o Estado do Evento.
     *
     * @return the m_estado
     */
    public EventoState getEstado() {
        return m_estado;
    }

    /**
     * Altera o Estado do Evento.
     *
     * @param e
     * @return
     */
    public EventoState setEstado(EventoState e) {
        return this.m_estado = e;}
    /**
     * Return RegistoOrganizadores.
     *
     * @return
     */
    public RegistoOrganizadores getM_listaOrganizadores() {
        return m_listaOrganizadores;
    }

    /**
     * Return RegistoOrganizadores.
     *
     * @param m_listaOrganizadores
     */
    public void setM_listaOrganizadores(RegistoOrganizadores m_listaOrganizadores) {
        this.m_listaOrganizadores = m_listaOrganizadores;
    }

    /**
     * @param m_strDataLimiteSubmissão the m_strDataLimiteSubmissão to set
     */
    public void setM_strDataLimiteSubmissão(Date m_strDataLimiteSubmissão) {
        this.m_strDataLimiteSubmissão = m_strDataLimiteSubmissão;
    }

    /**
     * @return the m_listaRevisoes
     */
    public List<Revisao> getM_listaRevisoes() {
        return m_listaRevisoes;
    }

    /**
     * @param m_listaRevisoes the m_listaRevisoes to set
     */
    public void setM_listaRevisoes(List<Revisao> m_listaRevisoes) {
        this.m_listaRevisoes = m_listaRevisoes;
    }

    /**
     * Devolve a data limite de registo
     *
     * @return
     */
    public Date getDataLimiteRegisto() {
        return this.m_strDataLimiteRegisto;
    }

    /**
     * Devolve a data limite de submissão final
     *
     * @return
     */
    public Date getDataLimiteSubmissaoFinal() {
        return this.m_strDataLimiteSubmissaoFinal;
    }

    /**
     * Devolve a data limite de revisão
     *
     * @return
     */
    public Date getDataLimiteRevisao() {
        return this.m_strDataLimiteRevisao;
    }

    /**
     * Modifica a data limite de registo
     *
     * @param dataLimiteRegisto
     */
    public void setDataLimiteRegisto(Date dataLimiteRegisto) {
        this.m_strDataLimiteRegisto = dataLimiteRegisto;
    }

    /**
     * Modifica a data limite de submissão final
     *
     * @param dataLimiteSubmissaoFinal
     */
    public void setDataLimiteSubmissaoFinal(Date dataLimiteSubmissaoFinal) {
        this.m_strDataLimiteSubmissaoFinal = dataLimiteSubmissaoFinal;
    }

    /**
     * Modifica a data limite de revisão
     *
     * @param dataLimiteRevisao
     */
    public void setDataLimiteRevisao(Date dataLimiteRevisao) {
        this.m_strDataLimiteRevisao = dataLimiteRevisao;
    }

    /**
     * Retorna a webpage do Evento.
     *
     * @return
     */
    public String getM_website() {
        return this.m_webpage;
    }

    /**
     * Retorna o ano.
     *
     * @return
     */
    public String getAno() {
        return this.m_ano;
    }

    /**
     * Altera o ano.
     *
     * @param ano
     */
    public void setAno(String ano) {
        this.m_ano = ano;
    }

    /**
     * Altera a web page do Evento.
     *
     * @param web
     */
    public void setWebpage(String web) {
        this.m_webpage = web;
    }

    public void setM_processoDecisao(ProcessoDecisao m_processoDecisao) {
        this.m_processoDecisao = m_processoDecisao;
    }

    public ProcessoDecisao getM_processoDecisao() {
        return m_processoDecisao;
    }

    public void setM_processoDistribuicao(ProcessoDistribuicao m_processoDistribuicao) {
        this.m_processoDistribuicao = m_processoDistribuicao;
    }

    public ProcessoDistribuicao getM_processoDistribuicao() {
        return m_processoDistribuicao;
    }

    public void setM_processoNotificacao(ProcessoNotificacao m_processoNotificacao) {
        this.m_processoNotificacao = m_processoNotificacao;
    }

    public ProcessoNotificacao getM_processoNotificacao() {
        return m_processoNotificacao;
    }
    
   

    public void setM_formula(Formula m_formula) {
        this.m_formula = m_formula;
    }
    
    
    
    public Formula getM_formula() {
        return this.m_formula;
    }

   
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }

        Evento e =  (Evento) obj;
        return (this.getM_strTitulo().equals(e.getM_strTitulo())
                && this.getM_strDescricao().equals(e.getM_strDescricao())
                && this.getM_local().equals(e.getM_local())
                && this.getM_strDataInicio().equals(e.getM_strDataInicio())
                && this.getM_strDataFim().equals(e.getM_strDataFim()));

    } 
    public void setListaSubmissoes(ListaSubmissoes ls){
        this.m_listaSubmissoes = ls;
    }
    
}