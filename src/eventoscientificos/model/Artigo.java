/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe responsável pelo artigo constituido por título, resumo, uma lista de
 * autores, um autor correspondente e tipo
 * 
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class Artigo implements Serializable {

    /**
     * Título do artigo
     */
    private String m_strTitulo;
    /**
     * Resumo do artigo
     */
    private String m_strResumo;
    /**
     * Lista de autores do artigo
     */
    private List<Autor> m_listaAutores = new ArrayList<Autor>();
    /**
     * Autor correspondente
     */
    private Autor m_autorCorrespondente;
    /**
     * PathFile do ficheiro com o artigo
     */
    private String m_strFicheiro;
    /**
     * Lista de tópicos abordados do artigo
     */
    private ListaTopicos m_listaTopicos;
    /**
     * Tipo de artigo(full,poster ou short)
     */
    private String tipo;
    /**
     * Lista de revisões do artigo
     */
    private List<Revisao> m_listaRevisoes = new ArrayList<Revisao>();

    /**
     * Construtor vazio
     */
    public Artigo() {
        m_listaAutores = new ArrayList<Autor>();
        m_listaTopicos = new ListaTopicos();
        this.m_strResumo = "";
        this.m_strTitulo = "";
        this.tipo = "";
        this.m_strFicheiro = "";
    }

    /**
     * Construtor a receber como parâmetro o título, o resumo e o tipo
     *
     * @param titulo
     * @param resumo
     * @param tipo
     */
    public Artigo(String titulo, String resumo, String tipo) {
        this.m_strResumo = resumo;
        this.m_strTitulo = titulo;
        this.tipo = tipo;
    }

    /**
     * Devolve o titulo do artigo
     *
     * @return
     */
    public String getTitulo() {
        return this.m_strTitulo;
    }

    /**
     * Devolve o resumo do artigo
     *
     * @return
     */
    public String getResumo() {
        return this.m_strResumo;
    }

    /**
     * Devolve o tipo de artigo
     *
     * @return
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Devolve a informação textual do artigo
     *
     * @return
     */
    public String getInfo() {
        return this.toString();
    }

    /**
     * Devolve a lista de possíveis autores correspondentes
     *
     * @return
     */
    public List<Autor> getPossiveisAutoresCorrespondentes() {
        List<Autor> la = new ArrayList<>();

        for (Autor autor : this.getM_listaAutores()) {
            if (autor.podeSerCorrespondente()) {
                la.add(autor);
            }
        }
        return la;
    }

    /**
     * Devolve a lista de autores do artigo
     *
     * @return the m_listaAutores
     */
    public List<Autor> getM_listaAutores() {
        return m_listaAutores;
    }

    /**
     * Devolve o autor correspondente do artigo
     *
     * @return the m_autorCorrespondente
     */
    public Autor getM_autorCorrespondente() {
        return this.m_autorCorrespondente;
    }
    /**
     * Devolve a lista de revisões do artigo
     * @return 
     */
    public List<Revisao> getListaRevisoes() {
        return m_listaRevisoes;
    }
   

    /**
     * Modifica o título do artigo
     *
     * @param strTitulo
     */
    public void setTitulo(String strTitulo) {
        this.m_strTitulo = strTitulo;
    }

    /**
     * Modifica o resumo do artigo
     *
     * @param strResumo
     */
    public void setResumo(String strResumo) {
        this.m_strResumo = strResumo;
    }

    /**
     * Modifica o tipo de artigo
     *
     * @param tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * Modifica o autor corresponte
     *
     * @param autor
     */
    public void setAutorCorrespondente(Autor autor) {
        this.m_autorCorrespondente = autor;
    }

    /**
     * Modifica a String com o caminho do ficheiro
     *
     * @param strFicheiro
     */
    public void setFicheiro(String strFicheiro) {
        this.m_strFicheiro = strFicheiro;
    }

    /**
     * Modifica a lista de tópicos do artigo
     *
     * @param listaTopicos
     */
    public void setListaTopicos(ListaTopicos listaTopicos) {
        this.m_listaTopicos = listaTopicos;
    }

    /**
     * Modifica a lista de autores do artigo
     *
     * @param m_listaAutores the m_listaAutores to set
     */
    public void setM_listaAutores(List<Autor> m_listaAutores) {
        this.m_listaAutores = m_listaAutores;
    }   
    /**
     * Modifica a lista de revisoes do artigo
     * @param listaRevisoes 
     */
    public void setListaRevisoes(ArrayList<Revisao> listaRevisoes) {
        this.m_listaRevisoes = listaRevisoes;
    }
    


    /**
     * Método que criar um novo autor com o nome e instituição de afiliação
     * passada por parâmetro
     *
     * @param strNome
     * @param strAfiliacao
     * @return
     */
    public Autor novoAutor(String strNome, String strAfiliacao) {
        Autor autor = new Autor();

        autor.setNome(strNome);
        autor.setAfiliacao(strAfiliacao);

        return autor;
    }

    /**
     * Método que criar um novo autor com o nome, instituição de afiliação,
     * email e utilizador passada por parâmetro
     *
     * @param strNome
     * @param strAfiliacao
     * @param strEmail
     * @param utilizador
     * @return
     */
    public Autor novoAutor(String strNome, String strAfiliacao, String strEmail, Utilizador utilizador) {
        Autor autor = new Autor();

        autor.setNome(strNome);
        autor.setAfiliacao(strAfiliacao);
        autor.setEMail(strEmail);
        autor.setUtilizador(utilizador);

        return autor;
    }

    /**
     * Adiciona um autor passado por parâmetro à lista de autores do artigo
     *
     * @param autor
     * @return
     */
    public boolean addAutor(Autor autor) {
        if (validaAutor(autor)) {
            return getM_listaAutores().add(autor);
        } else {
            return false;
        }

    }

    /**
     * Valida o autor
     *
     * @param autor
     * @return
     */
    private boolean validaAutor(Autor autor) {
        return autor.valida();
    }

    /**
     * Valida local
     *
     * @return
     */
    public boolean valida() {
        return true;
    }

    /**
     * Devolve a descrição textual do artigo
     *
     * @return
     */
    @Override
    public String toString() {
        String strAut = "";
        for (Autor aut : this.m_listaAutores) {
            strAut += "[" + aut.getM_strNome() + "]" + " ";
        }
        return "Título: " + this.m_strTitulo + "\n"
                + "Resumo: " + this.m_strResumo + "\n"
                + "Tipo: " + this.tipo + "\n"
                + "Autores: " + strAut;
    }

    /**
     * Método que verifica se o objeto recebido por parâmetro é igual ao
     * comparado
     *
     * @param obj
     * @return resultado booleano da comparação
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else {
            if (obj instanceof Artigo) {
                Artigo aux = (Artigo) obj;
                return this.getM_autorCorrespondente().equals(aux.getM_autorCorrespondente())
                        && this.m_strTitulo.equals(aux.m_strTitulo);
            } else {
                return false;
            }
        }
    }

    /**
     * Devolve o hashcode da instância Artigo
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + (this.m_strTitulo != null ? this.m_strTitulo.hashCode() : 0);
        hash = 19 * hash + (this.m_strResumo != null ? this.m_strResumo.hashCode() : 0);
        return hash;
    }

}
