/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.legacy;

import eventoscientificos.model.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Classe responsável pela importação de uma lista de submissões
 * 
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class ImportarSubmissoes {
    /**
     * Nome do ficheiro
     */
    private String filename;
    /**
     * Construtor
     * @param filename 
     */
    public ImportarSubmissoes(String filename) {
        this.filename = filename;
    }
    /**
     * Construtor vazio
     */
    public ImportarSubmissoes() {
        this.filename = "default";
    }
    /**
     * Devolve filename 
     * @return 
     */
    public String getFilename() {
        return filename;
    }
    /**
     * Modifica filename
     * @param filename 
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }
    /**
     * Lê o ficheiro de submissoes e devolve um hashmap
     * @param filename
     * @param ep
     * @return 
     * @throws java.io.FileNotFoundException 
     */
    public Map<String, Submissao> lerFicheiroCsv(String filename, Empresa ep) throws FileNotFoundException {
        String[] ordem = null;
        String[] partes;
        ArrayList<Submissao> subTemp = new ArrayList();
        Map<String, Submissao> map = new HashMap<>();
        Scanner in;
        int nrLinhas = 0;
            in = new Scanner(new File(filename + ".csv"), "ISO-8859-15");
            String linha;
            boolean primeiraLinha = true;
            String artigoId = "";
            String eventoId = "";

            while (in.hasNextLine()) {
                linha = in.nextLine();
                nrLinhas++;
                Artigo artigo = new Artigo();
                if (primeiraLinha) {
                    ordem = linha.split(";");
                    primeiraLinha = false;
                } else {

                    Autor autor;
                    ArrayList<Autor> listaAutor = new ArrayList<>();
                    partes = linha.split(";");
                    for (int i = 0; i < partes.length; i++) {
                        switch (ordem[i]) {
                            case "ConferenceID":
                                eventoId = partes[i];
                                break;
                            case "ID":
                                artigoId = partes[i];
                                break;
                            case "Type":
                                if (partes[i].equalsIgnoreCase("full paper")) {
                                    artigo.setTipo("full");
                                } else if (partes[i].equalsIgnoreCase("poster")) {
                                    artigo.setTipo("poster");
                                } else if (partes[i].equalsIgnoreCase("short paper")) {
                                    artigo.setTipo("short");
                                }
                                break;
                            case "Title":
                                artigo.setTitulo(partes[i]);
                                break;
                            case "Author":
                                autor = new Autor();
                                autor.setNome(partes[i]);
                                autor.setAfiliacao(partes[i + 1]);
                                autor.setEMail(partes[i + 2]);
                                listaAutor.add(autor);
                                i += 2;
                                break;
                            default:
                                break;
                        }
                    }
                    artigo.setM_listaAutores(listaAutor);

                    Submissao submissao = new Submissao(artigo);
                    map.put(eventoId + "_" + artigoId, submissao);
                }

            }in.close();    
        return map;
    }
}
