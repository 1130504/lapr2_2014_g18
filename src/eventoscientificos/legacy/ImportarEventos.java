/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.legacy;

import eventoscientificos.model.*;
import java.io.*;
import java.util.*;

/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */
public class ImportarEventos {

    private String filename;

    public ImportarEventos(String filename) {
        this.filename = filename;
    }

    public ImportarEventos() {
        this.filename = "EventHist_CDIO_v3_Events";
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * Método que retorna a DataFinal do Evento conhecendo a Duração (em dias) do mesmo.
     *
     * @param duracao
     * @param dataInicio
     * @return
     */
    public Date calcularDataFim(String duracao, Date dataInicio) {
        //Parse String da Duração(em dias) para int.
        int d = Integer.parseInt(duracao);
        Date dataFim = new Date();
        dataFim.setMonth(dataInicio.getMonth());
        dataFim.setYear(dataInicio.getYear());
        dataFim.setDate(dataInicio.getDate() + (d - 1));
        return dataFim;
    }

    /**
     * Lê Ficheiro CSV.
     *
     * @param filename
     * @param ep
     * @return
     * @throws FileNotFoundException
     */
    public Map<String, Evento> lerFicheiroCsv(String filename, Empresa ep) throws FileNotFoundException {
        String[] ordem = null;
        String[] partes;
        Map<String, Evento> map = new HashMap<String, Evento>();
        Scanner in;
        String eventoId = "";

        in = new Scanner(new File(filename + ".csv"), "ISO-8859-15");
        String linha;
        boolean primeiraLinha = true;
        while (in.hasNextLine()) {
            linha = in.nextLine();
            if (primeiraLinha) {
                ordem = linha.split(";");
                primeiraLinha = false;
            } else {

                Evento e = new Evento();
                Organizador organizador;
                Utilizador utilizador;
                partes = linha.split(";");
                for (int i = 0; i < partes.length; i++) {
                    switch (ordem[i]) {
                        case "ID":
                            eventoId = partes[i];
                            break;
                        case "Year":
                            e.setAno(partes[i]);
                            break;
                        case "Name":
                            e.setTitulo(partes[i]);
                            break;
                        case "Host":
                            e.setDescricao(partes[i]);
                            break;
                        case "City":
                            e.setLocal(partes[i]);
                            break;
                        case "Country":
                            e.setLocal(e.getM_local() + " " + partes[i]);

                            break;
                        case "Submission deadline":
                            e.setDataLimiteSubmissão(new Date(partes[i]));
                            break;
                        case "Revision deadline":
                            e.setDataLimiteRevisao(new Date(partes[i]));
                            break;
                        case "Final paper deadline":
                            e.setDataLimiteSubmissaoFinal(new Date(partes[i]));

                            break;
                        case "Author registration deadline":
                            e.setDataLimiteRegisto(new Date(partes[i]));

                            break;
                        case "Date":
                            e.setDataInicio(new Date(partes[i]));
                            break;
                        case "Duration (days)":
                            //partes[i] vai ter a duração dos dias em string.
                            Date dt = calcularDataFim(partes[i], e.getM_strDataInicio());
                            e.setDataFim(dt);

                            break;
                        case "Website":
                            e.setWebpage(partes[i]);

                            break;
                        case "Organizer":
                            utilizador = new Utilizador(partes[i], "", partes[i], partes[i + 1]);
                            organizador = new Organizador(partes[i], utilizador);
                            e.getM_listaOrganizadores().addOrganizador(organizador);
                            ep.getM_listaUtilizadores().getM_listaUtilizadores().add(utilizador);
                            break;
                        case "Email":

                            break;
                        default:
                            break;
                    }

                }
                ep.getRegistaEvento().addEvento(e);

                map.put(eventoId, e);
            }
        }
        in.close();

        return map;
    }
}
