/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.legacy;

import eventoscientificos.model.*;
import java.io.FileNotFoundException;
import java.util.*;

/**
 *
 * @author Alexandre
 */
public class CarregamentoDados {

    /**
     * HashMap <EventoID,Evento>
     */
    private Map<String, Evento> mapEventos = new HashMap<>();

    /**
     * HashMap <RevisorID,Revisao>
     */
    private Map<String, Revisao> mapRevisoes = new HashMap<>();

    /**
     * HashMap <ArtigoID,Artigo>
     */
    private Map<String, Submissao> mapSubmissoes = new HashMap<>();

    /**
     * HashMap <UtilizadorID,Utilizador>
     */
    private Map<String, Utilizador> mapUtilizadores = new HashMap<>();

    /**
     * HashMap <RevisorID,Revisor>
     */
    private Map<String, Revisor> mapRevisores = new HashMap<>();

    /**
     * Lista de Eventos
     */
    private List<Evento> listaEventos = new ArrayList<>();

    /**
     * Lista de Utilizadores
     */
    private List<Utilizador> listaUtilizadores = new ArrayList<>();

    /**
     * Lista de Revisores
     */
    private List<Revisor> ListaRevisores = new ArrayList<>();

    /**
     * Empresa
     */
    private Empresa empresa;
    
    /**
     * CP
     */
    private CP m_cp;

    /**
     * Este método é composto por quatro ciclos while, o primeiro lê a
     * informação do ficheiro das revisões e cria, só com o email, os
     * utilizadores e revisores dos artigo, o segundo atrbui as submissoes
     * (artigos) ao evento, o terceiro atribui as revisões às submissões e o
     * quarto adicona o evento com toda a informação a uma lista de informações.
     *
     * @param ep Empresa
     * @return
     * @throws java.io.FileNotFoundException
     */
    public List<Evento> Carrega(Empresa ep) throws FileNotFoundException {
        this.empresa = ep;
        m_cp = new CP();
        ImportarEventos importEventos = new ImportarEventos();
        ImportarRevisoes importRevisoes = new ImportarRevisoes();
        ImportarSubmissoes importSubmissoes = new ImportarSubmissoes();

        mapEventos = importEventos.lerFicheiroCsv("EventHist_CDIO_v3_Events", ep);
        mapRevisoes = importRevisoes.leFicheiroCsv("EventHist_CDIO_v3_Reviews", ep);
        mapSubmissoes = importSubmissoes.lerFicheiroCsv("EventHist_CDIO_v3_Papers", ep);

        Set mapSetUtilizadores = (Set) mapRevisoes.entrySet();
        Iterator mapIteratorUtilizadores = mapSetUtilizadores.iterator();

        while (mapIteratorUtilizadores.hasNext()) {
            Map.Entry mapEntry = (Map.Entry) mapIteratorUtilizadores.next();
            String keyValue = (String) mapEntry.getKey();

            Utilizador tempUtilizador = new Utilizador("", "", "", keyValue.split("_")[2]);

            if (!mapUtilizadores.containsKey(keyValue.split("_")[2])) {
                mapUtilizadores.put(keyValue.split("_")[2], tempUtilizador);
                mapRevisores.put(keyValue.split("_")[2], new Revisor(tempUtilizador));
                this.ListaRevisores.add(mapRevisores.put(keyValue.split("_")[2], new Revisor(tempUtilizador)));
                m_cp.registaMembroCP(mapRevisores.put(keyValue.split("_")[2], new Revisor(tempUtilizador)));
            }
            this.empresa.getM_listaUtilizadores().getM_listaUtilizadores().add(tempUtilizador);
            
            
        }

        Set mapSet = (Set) mapSubmissoes.entrySet();
        Iterator mapIterator = mapSet.iterator();

        while (mapIterator.hasNext()) {
            Map.Entry mapEntry = (Map.Entry) mapIterator.next();
            String keyValue = (String) mapEntry.getKey();
            Submissao value = (Submissao) mapEntry.getValue();
            String eventId = keyValue.split("_")[0];

            mapEventos.get(eventId).getListaSubmissoes().addSubmissao(value);
        }

        Set mapSetRevisoes = (Set) mapRevisoes.entrySet();
        Iterator mapIteratorRevisoes = mapSetRevisoes.iterator();

        while (mapIteratorRevisoes.hasNext()) {
            Map.Entry mapEntry = (Map.Entry) mapIteratorRevisoes.next();
            String keyValue = (String) mapEntry.getKey();
            Revisao value = (Revisao) mapEntry.getValue();

            String eventoId = keyValue.split("_")[0];
            String artigoId = keyValue.split("_")[1];
            String revisorId = keyValue.split("_")[2];
            Artigo artigo = mapSubmissoes.get(eventoId + "_" + artigoId).getArtigo();
//            value.setArtigo(mapSubmissoes.get(eventoId + "_" + artigoId).getArtigo());
            artigo.getListaRevisoes().add(value);
            value.setM_revisor(mapRevisores.get(revisorId));
        }

        Set mapSetEventos = (Set) mapEventos.entrySet();
        Iterator mapEventosIterator = mapSetEventos.iterator();

        while (mapEventosIterator.hasNext()) {
            Map.Entry mapEntry = (Map.Entry) mapEventosIterator.next();
            this.listaEventos.add((Evento) mapEntry.getValue());
        }
        for (int i = 0; i < listaEventos.size(); i++) {
                listaEventos.get(i).setCP(m_cp);
            }
        return this.listaEventos;
    }
}
