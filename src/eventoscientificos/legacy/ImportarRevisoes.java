/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.legacy;

import eventoscientificos.model.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class ImportarRevisoes {

    /**
     * Nome do ficheiro
     */
    private String filename;

    /**
     * Construtor
     *
     * @param filename
     */
    public ImportarRevisoes(String filename) {
        this.filename = filename;
    }

    /**
     * Construtor vazio
     */
    public ImportarRevisoes() {
        this.filename = "default";
    }

    /**
     * Devolve filename
     *
     * @return
     */
    public String getFilename() {
        return filename;
    }

    /**
     * Modifica filename
     *
     * @param filename
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * Lê o ficheiro de revisões de artigos e devolve um hashmap
     *
     * @param filename
     * @param ep
     * @return
     * @throws java.io.FileNotFoundException
     */
    public Map<String, Revisao> leFicheiroCsv(String filename, Empresa ep) throws FileNotFoundException {
        String[] ordem = null;
        String[] partes;
        Map<String, Revisao> map = new HashMap<>();

        Scanner in;
        String eventoId = "";
        String artigoId = "";
        String revisor = "";

        in = new Scanner(new File(filename + ".csv"), "ISO-8859-15");
        String linha;
        boolean primeiraLinha = true;
        while (in.hasNextLine()) {
            linha = in.nextLine();
            if (primeiraLinha) {
                ordem = linha.split(";");
                primeiraLinha = false;
            } else {

                Revisao revisao = new Revisao();
                String recomendacao = "";
                int[] temp = new int[4];
                partes = linha.split(";");
                for (int i = 0; i < partes.length; i++) {
                    switch (ordem[i]) {
                        case "ConferenceID":
                            eventoId = partes[i];
                            break;
                        case "ID":
                            artigoId = partes[i];
                            break;
                        case "Reviewer":
                            revisor = partes[i];
                            break;
                        case "Reviewer confidence":
                            temp[0] = Integer.parseInt(partes[i]);
                            break;
                        case "Suitability for the event":
                            temp[1] = Integer.parseInt(partes[i]);
                            break;
                        case "Originality":
                            temp[2] = Integer.parseInt(partes[i]);
                            break;
                        case "Quality":
                            temp[3] = Integer.parseInt(partes[i]);
                            break;
                        case "Overall Recommendation":
                            if (partes[i].equals("Accept")) {
                                recomendacao = "Aceite";
                            } else {
                                recomendacao = "Rejeitado";
                            }
                            break;
                        default:
                            break;
                    }

                }
                revisao.setDados(temp, recomendacao);
                map.put(eventoId + "_" + artigoId + "_" + revisor, revisao);

            }
        }
        in.close();

        return map;
    }
}
