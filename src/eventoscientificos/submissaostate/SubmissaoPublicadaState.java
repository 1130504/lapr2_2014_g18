/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.submissaostate;

import eventoscientificos.model.Submissao;
import java.io.Serializable;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class SubmissaoPublicadaState implements SubmissaoState, Serializable  {

    /**
     * Submissao
     */
    private Submissao m_submissao;

    /**
     *
     * @param s
     */
    public SubmissaoPublicadaState(Submissao s) {
        this.m_submissao = s;
    }

    @Override
    public boolean setCriada() {
        return false;
    }

    @Override
    public boolean setSubmetidoParaRevisao() {
        return false;
    }

    @Override
    public boolean setDistribuida() {
        return false;
    }

    @Override
    public boolean setRevista() {
        return false;
    }

    @Override
    public boolean setAceite() {
        return false;
    }

    @Override
    public boolean setRejeitada() {
        return false;
    }

    @Override
    public boolean setPublicado() {
        return true;
    }

    @Override
    public boolean valida() {
        return true; // a implementar   
    }

}
