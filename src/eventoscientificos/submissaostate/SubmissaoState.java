/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.submissaostate;

import java.io.Serializable;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public interface SubmissaoState{
    
    boolean setCriada();
    boolean setSubmetidoParaRevisao();
    boolean setDistribuida();
    boolean setRevista();
    boolean setAceite();
    boolean setRejeitada();
    boolean setPublicado();
    
    boolean valida();
}
