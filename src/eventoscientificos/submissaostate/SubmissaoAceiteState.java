/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.submissaostate;

import eventoscientificos.model.Submissao;
import java.io.Serializable;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class SubmissaoAceiteState implements SubmissaoState, Serializable {

    /**
     * Submissao
     */
    private Submissao m_submissao;

    /**
     *
     * @param s
     */
    public SubmissaoAceiteState(Submissao s) {
        this.m_submissao = s;
    }

    @Override
    public boolean setCriada() {
        return false;
    }

    @Override
    public boolean setSubmetidoParaRevisao() {
        return false;
    }

    @Override
    public boolean setDistribuida() {
        return false;
    }

    @Override
    public boolean setRevista() {
        return false;
    }

    @Override
    public boolean setAceite() {
        return true;
    }

    @Override
    public boolean setRejeitada() {
        return false;
    }

    @Override
    public boolean setPublicado() {
        if (valida()) {
            this.m_submissao.setEstado(new SubmissaoPublicadaState(this.m_submissao));
            return true;
        } else {
            return false;
        }

    }

    @Override
    public boolean valida() {
        return true; // a implementar
    }

}
