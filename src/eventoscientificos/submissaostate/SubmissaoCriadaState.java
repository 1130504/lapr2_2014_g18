/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.submissaostate;

import eventoscientificos.model.Submissao;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class SubmissaoCriadaState implements SubmissaoState {

    /**
     * Submissao
     */
    private Submissao m_submissao;

    /**
     *
     * @param s
     */
    public SubmissaoCriadaState(Submissao s) {
        this.m_submissao = s;
    }

    @Override
    public boolean setCriada() {
        return true;
    }

    @Override
    public boolean setSubmetidoParaRevisao() {
        if (valida()) {
            this.m_submissao.setEstado(new SubmissaoParaRevisaoState(this.m_submissao));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean setDistribuida() {
        return false;
    }

    @Override
    public boolean setRevista() {
        return false;
    }

    @Override
    public boolean setAceite() {
        return false;
    }

    @Override
    public boolean setRejeitada() {
        return false;
    }

    @Override
    public boolean setPublicado() {
        return false;
    }

    @Override
    public boolean valida() {
        return this.m_submissao.valida();
    }

}
