/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.pagamentos;

import pt.ipp.isep.dei.eapli.visaolight.VisaoLight;

/**
 *
 * @author Alexandre
 */
public class PagamentoVisaoLight implements PagamentoInterface {

    /**
     * Valor da Autorização
     */
    private float valorAutorizacao;

    /**
     * Número Cartão Crédito
     */
    private String numCC;

    /**
     * Data de validade do Cartão de Crédito
     */
    private String dtValidadeCC;

    /**
     * Data Limite de Autorização
     */
    private String dtLmtAutorizacao;

    /**
     * Constrói uma instância de pagamentoVisaoLight que recbe como parametros o
     * número do cartão, a data de validade do cartão, o valor do pagamento e a
     * data limite do pagamento.
     *
     * @param numCC número do cartão
     * @param dtValidadeCC validade do cartão
     * @param valorAutorizacao valor do pagamento
     * @param dtLmtAutorizacao data limite do pagamento
     */
    public PagamentoVisaoLight(String numCC, String dtValidadeCC, float valorAutorizacao, String dtLmtAutorizacao) {
        this.valorAutorizacao = valorAutorizacao;
        this.numCC = numCC;
        this.dtValidadeCC = dtValidadeCC;
        this.dtLmtAutorizacao = dtLmtAutorizacao;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean pagamento() {
        return validaPedido() != null;
    }

    public String validaPedido() {
        return VisaoLight.getAutorizacaoDCC(numCC, dtValidadeCC, valorAutorizacao, dtLmtAutorizacao);
    }

}
