/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.pagamentos;

import java.util.Date;
import pt.ipp.isep.dei.eapli.canadaexpress.CanadaExpress;
import pt.ipp.isep.dei.eapli.canadaexpress.Pedido;

/**
 *
 * @author Alexandre
 */
public class PagamentoCanadaExpress implements PagamentoInterface{
    
    /**
     * CanadaExpress
     */
    CanadaExpress canadaExpress;
    
    /**
     * Pedido
     */
    Pedido pedido;

    /**
     * Contrói uma instância de pagamentoCanadaExpress que recebe com parametros a data de validade do cartão,
     * o número do cartão, o valor a descontar no cartão, data limite de pagamento.
     * @param dtValidadeCC data de validade do cartão de crédito
     * @param numCC número do cartão de crédito
     * @param fValorADCC valor a deboitar no cartão de crédito 
     * @param dtLimite data limite de pagamento
     */
    public PagamentoCanadaExpress(Date dtValidadeCC, String numCC, float fValorADCC, Date dtLimite) {
        canadaExpress = new CanadaExpress();
        pedido = new Pedido(dtValidadeCC, numCC, fValorADCC, dtLimite);
    }

   /**
     *
     * @return 
     */
    @Override
        public boolean pagamento() {
        return canadaExpress.Finish();
    }
    
    public boolean iniciaPedido(){
        return canadaExpress.Init("#CANADA#EXPRESS#EAPLI#");
    }
    
    public String validaPedido(){
        return canadaExpress.ValidaPedido(pedido);
    }

    
}
