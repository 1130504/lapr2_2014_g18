/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.pagamentos;

/**
 *
 * @author Alexandre
 */
public interface PagamentoInterface {
    
    /**
     * Método de pagamento
     * @return 
     */
    boolean pagamento();
    
}
