/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.pagamentos;

import java.util.Date;

/**
 *
 * @author Alexandre
 */
public class SistemaPagamento {

    /**
     * Devolve um novo Pagamento de CanadaExpress
     * 
     * @param dtValidadeCC data de validade do cartão
     * @param numCC número do cartão  
     * @param fValorADCC valor a debitar no cartão
     * @param dtLimite data limite de pagamento
     * @return 
     */
     public static PagamentoInterface getPagamentoCanadaExpress(Date dtValidadeCC, String numCC, float fValorADCC, Date dtLimite) {
        return new PagamentoCanadaExpress( dtValidadeCC, numCC, fValorADCC, dtLimite);
    }

    /**
     *Devolve um novo pagamento de VisaoLight
     * 
     * @param numCC número do cartão  
     * @param dtValidadeCC data de validade do cartão
     * @param valorAutorizacao valor da Autorização
     * @param dtLmtAutorizacao data Limite de Autorização
     * @return
     */
    public static PagamentoInterface getPagamentoVisaoLight(String numCC, String dtValidadeCC, float valorAutorizacao, String dtLmtAutorizacao) {
        return new PagamentoVisaoLight(numCC, dtValidadeCC, valorAutorizacao, dtLmtAutorizacao);
    }
}
