/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.imports;


import eventoscientificos.model.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */
public class BINFile implements Serializable {

    

    /**
     * Nome do Ficheiro / Diretório do mesmo.
     */
    private String filename;

    /**
     * Constrói uma instância BinFile com o caminho do ficheiro e a Empresa.
     *
     */
    public BINFile() {
        
        this.filename = "data.bin";
    }

    /**
     * Define diretório do ficheiro.
     *
     * @param filename
     */
    public void setPathName(String filename) {
        this.filename = filename;
        /**
         * tirar apenas o ficheiro do caminho.
         */
        /*String [] temp =  filename.split("/");
        int p = (temp.length - 1);
        this.filename = temp[p];*/
    }

    /**
     * Retorna String do diretório do ficheiro.
     *
     * @return
     */
    public String getPathName() {
        return this.filename;

    }
    
   
     
    /**
     * Carrega Ficheiro Binário.
     *
     * @param a
     * @return 
     * @throws FileNotFoundException
     * @throws java.lang.ClassNotFoundException
     */
    public  Empresa readData(Empresa a) throws FileNotFoundException, IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("data.bin"));
        //Guarda informação dentro da instância Empresa.
        a = (Empresa) in.readObject();
        in.close();
        
        return a;
     }

    /**
     * Guarda informação num Ficheiro Binário.
     *
     * @param a
     * @throws FileNotFoundException
     */
    public  void writeData(Empresa a) throws FileNotFoundException, IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("data.bin"));
        //informação a guardar (guarda os dados da empresa que encorpora instância BinFile.
        out.writeObject(a);
        //Fechar ficheiro
        out.close();

    }

    

}
