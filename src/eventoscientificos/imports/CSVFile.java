/*/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.imports;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import eventoscientificos.model.*;
import eventoscientificos.eventostate.*;
import java.util.*;


/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */
public class CSVFile {

    /**
     * Caminho do ficheiro.
     */
    private String pathname;
    /**
     * Empresa.
     */
    private Empresa empresa;

    /**
     * Constrói uma instância CSVFile com o caminho do ficheiro e Empresa.
     *
     * @param pathname
     * @param empresa
     */
    public CSVFile(String pathname, Empresa empresa) {

        this.empresa = empresa;
        this.pathname = pathname;
    }
    
    /**
     * Método que retorna a DataFinal do Evento conhecendo a Duração (em dias) do mesmo.
     *
     * @param duracao
     * @param dataInicio
     * @return
     */
    public Date calcularDataFim(String duracao, Date dataInicio) {
        //Parse String da Duração(em dias) para int.
        int d = Integer.parseInt(duracao);
        Date dataFim = new Date(dataInicio.getYear(), dataInicio.getMonth(), (dataInicio.getDate() + (d-1)));
        
        return dataFim;
    }

    /**
     * Método faz parse e retorna informação de Ficheiro CSV.
     *
     * @return List <Evento>.
     * @throws java.io.FileNotFoundException
     */
    public List<Evento> lerFicheiroCsv() throws FileNotFoundException {
        String[] ordem = null;
        String[] partes;
        List<Evento> le = new ArrayList<>();

        Scanner in = new Scanner(new File(this.pathname), "ISO-8859-15");
        String linha;
        boolean primeiraLinha = true;
        while (in.hasNextLine()) {
            linha = in.nextLine();
            if (primeiraLinha) {
                ordem = linha.split(";");
                primeiraLinha = false;
            } else {

                Evento e = new Evento();

                partes = linha.split(";");
                for (int i = 0; i < partes.length; i++) {
                    switch (ordem[i]) {

                        case "Year":
                            e.setAno(partes[i]);
                            break;
                        case "Name":
                            e.setTitulo(partes[i]);
                            break;
                        case "Host":
                            e.setDescricao(partes[i]);
                            break;
                        case "City":
                            e.setLocal(partes[i]);
                            break;
                        case "Country":
                            e.setLocal(e.getM_local() + " " + partes[i]);
                            break;
                        case "Date":
                            e.setDataInicio(new Date(partes[i]));
                            break;
                        case "Duration (days)": 
                           //partes[i] vai ter a duração dos dias em string.
                            Date dt = calcularDataFim(partes[i], e.getM_strDataInicio());
                            e.setDataFim(dt);
                            break;
                        case "Organizer":
                           
                            Utilizador u = new Utilizador(partes[i], "", partes[i], partes[i + 1]);
                          
                            Organizador o = new Organizador(partes[i], u);
                           
                            e.getM_listaOrganizadores().addOrganizador(o);
                           
                            this.empresa.getM_listaUtilizadores().getM_listaUtilizadores().add(u);
                            break;
                        case "Email":

                            break;
                        default:
                            break;
                    }

                }e.setEstado(new EventoCriadoCSVState(e));
                le.add(e);
            }
        }
        in.close();

        return le;

    }

}
