/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.imports;

import eventoscientificos.model.*;
import eventoscientificos.eventostate.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */
public class XMLFile {

    /**
     * Caminho do ficheiro.
     */
    private String path_name;
    /**
     * Empresa.
     */
    private Empresa empresa;

    /**
     * Constrói uma instância XMLFile que contém o caminho do ficheiro e a Empresa onde vai guardar
     * os eventos.
     *
     * @param pathname
     * @param empresa
     */
    public XMLFile(String path_name, Empresa empresa) {
        this.path_name = path_name;
        this.empresa = empresa;
    }

    /**
     * Define diretório do ficheiro.
     *
     * @param path_name
     */
    public void setPathName(String path_name) {
        this.path_name = path_name;
    }

    /**
     * Retorna String do diretório do ficheiro.
     *
     * @return
     */
    public String getPathName() {
        return this.path_name;
    }
    /**
     * Método faz parse e retorna informação de Ficheiros XML.
     * @return
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     * @throws XPathExpressionException 
     */
    public List<Evento> lerXMLFile() throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        List<Evento> le = new ArrayList<>();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            String nomeFicheiro = this.path_name; //nome do ficheiro 
            File ficheiro = new File(nomeFicheiro); //ficheiro
            Document documento = builder.parse(ficheiro); //builder faz parse do ficheiro para documento;

            XPathFactory m_xpathfactory = XPathFactory.newInstance();
            XPath m_path = m_xpathfactory.newXPath();

            String totalEventosNum = m_path.evaluate("count(/event_list/*)", documento);
            int totalEventosInt = Integer.parseInt(totalEventosNum);

            for (int i = 1; i < (totalEventosInt + 1); i++) {

                int totalDadosEvento = Integer.parseInt(m_path.evaluate("count(/event_list/event[" + i + "]/*)", documento));

                int totalOrgEvento = Integer.parseInt(m_path.evaluate("count(/event_list/event[" + i + "]/organizer)", documento));

                int totalDadosBasicos = (totalDadosEvento - totalOrgEvento);

                Evento e = new Evento();

                e.setAno(m_path.evaluate("/event_list/event[" + i + "]/year", documento));

                e.setTitulo(m_path.evaluate("/event_list/event[" + i + "]/name", documento));

                e.setDescricao(m_path.evaluate("/event_list/event[" + i + "]/host", documento));

                e.setLocal(m_path.evaluate("/event_list/event[" + i + "]/city", documento) + ", " + m_path.evaluate("/event_list/event[" + i + "]/country", documento));
                /**
                 * Para outros eventos date1 e date2 vão ter diferentes valores.
                 */
                String date1 = (m_path.evaluate("/event_list/event[" + i + "]/begin_date", documento));
               //*como estava* e.setDataInicio(m_path.evaluate("/event_list/event[" + i + "]/begin_date", documento));
                e.setDataInicio(new Date(date1));
              
                String date2 = (m_path.evaluate("/event_list/event[" + i + "]/end_date", documento));
                //*como estava* e.setDataFim(m_path.evaluate("/event_list/event[" + i + "]/end_date", documento));
                e. setDataFim(new Date(date2));

                e.setWebpage(m_path.evaluate("/event_list/event[" + i + "]/web_page", documento));

                for (int j = 1; j < (totalOrgEvento + 1); j++) {
                    String nome = m_path.evaluate("/event_list/event[" + i + "]/organizer[" + j + "]/org_name", documento);

                    String email = m_path.evaluate("/event_list/event[" + i + "]/organizer[" + j + "]/email", documento);

                    for (Utilizador u : this.empresa.getM_listaUtilizadores().getM_listaUtilizadores()) {

                        if (u.getEmail().equalsIgnoreCase(email)) {

                            Organizador o = new Organizador(nome, u);

                            e.getM_listaOrganizadores().getListaOrganizadores().add(o);
                        }

                    }
                }

                e.setEstado(new EventoCriadoState(e));
                le.add(e);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return le;
    }

}
