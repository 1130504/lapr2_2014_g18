package eventoscientificos.controller;


import eventoscientificos.model.ListaTopicos;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.CP;
import eventoscientificos.model.RegistoEventos;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Utilizador;

import java.util.List;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */

public class CriarCPController implements StateController{
    /**
     * Empresa
     */
    private Empresa m_empresa;
    
    /**
     * Evento
     */
    private Evento m_evento;
    
    /**
     * Comissão do Programa
     */
    private CP m_cp;
    
    /**
     * Registo de Evento
     */
    private RegistoEventos registoEventos = new RegistoEventos();

    /**
     * Constrói uma instância do CriarCPController
     * @param empresa Empresa
     */
    public CriarCPController(Empresa empresa) {
        m_empresa = empresa;
    }

    /**
     * Cria uma lista de eventos de organizador recenbendo por parâmetro a ID do mesmo.
     * @param strId ID Organizador
     * @return lista de eventos do Organizador.
     */
    public List<Evento> getEventosOrganizador(String strId) {
        return registoEventos.getEventosOrganizador(strId, this.m_empresa);
    }
    
    /**
     * Devolve uma lista de tópicos abordados no evento.
     * @return lista de tópicos
     */
    public ListaTopicos getTopicosEvento() {
        if (m_evento != null) {
            return m_evento.getTopicos();
        } else {
            return null;
        }
    }

    /**
     * Define o Evento recebico por parâmetro e Cria um nova CP para o Evento
     * @param e Evento
     */
    public void selectEvento(Evento e) {
        m_evento = e;
        m_cp = m_evento.novaCP();
    }

    /**
     * Criar um Revisor na CP
     * @param strId ID do Utilizador/Revisor
     * @return boolean
     */
    public Revisor addMembroCP(String strId) {
        Utilizador u = m_empresa.getUtilizador(strId);
        if (u != null) {
            return m_cp.addMembroCP(strId, u);
        } else {
            return null;
        }
    }

    /**
     * Regista o Revisor na CP
     * @param r Revisor
     * @return boolean
     */
    public boolean registaMembroCP(Revisor r) {
        r.addEvento(m_evento);
        return m_cp.registaMembroCP(r);
    }

    /**
     * Modifica a CP
     */
    public void setCP() {
        m_evento.setCP(m_cp);
    }

    /**
     * Modifica a Lista de Tópicos do Revisor
     * @param r
     * @param listaTopicosRevisor 
     */
    public void setListaTopicosRevisor(Revisor r, ListaTopicos listaTopicosRevisor) {
        if (m_cp.getListaRevisores().contains(r)) {
            r.setListaTopicos(listaTopicosRevisor);
        }
    }
     /**
     * Altera o Estado do Evento para CP Definida.  
     * @return 
     */
    public boolean alterarEstado() {
    return this.m_evento.getEstado().setCPDefinida();
    }
}

