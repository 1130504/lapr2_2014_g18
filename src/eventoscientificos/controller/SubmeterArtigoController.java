/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;


import eventoscientificos.model.Submissao;
import eventoscientificos.model.Artigo;
import eventoscientificos.model.ListaTopicos;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Topico;
import eventoscientificos.model.Autor;
import java.util.List;
import eventoscientificos.GUI.*;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class SubmeterArtigoController
{

    private Empresa m_empresa;
    private Evento m_evento;
    private Submissao m_submissao;
    private Artigo m_artigo;

    public SubmeterArtigoController(Empresa empresa)
    {
        m_empresa = empresa;
    }

    public List<Evento> iniciarSubmissao()
    {
        return this.m_empresa.getListaEventosPodeSubmeter();
    }

    public void selectEvento(Evento e)
    {
        m_evento = e;
        this.m_submissao = this.m_evento.getListaSubmissoes().novaSubmissao();
        this.setM_artigo(this.m_submissao.novoArtigo());
    }

    public ListaTopicos getTopicosEvento()
    {
        if (m_evento != null)
        {
            return m_evento.getTopicos();
        } else
        {
            return null;
        }
    }

    public void setDados(String strTitulo, String strResumo, String tipo)
    {
        this.m_artigo.setTitulo(strTitulo);
        this.m_artigo.setResumo(strResumo);
        this.m_artigo.setTipo(tipo);
    }

    public Autor novoAutor(String strNome, String strAfiliacao)
    {
        return this.m_artigo.novoAutor(strNome, strAfiliacao);
    }
    
    
    
    public Autor novoAutor(String strNome, String strAfiliacao, String strEmail)
    {
        return this.m_artigo.novoAutor(strNome, strAfiliacao,strEmail,this.m_empresa.getM_listaUtilizadores().getUtilizadorEmail(strEmail));
    }
    
    
    public boolean addAutor(Autor autor)
    {
        return this.m_artigo.addAutor(autor);
    }

    public List<Autor> getPossiveisAutoresCorrespondentes()
    {
        return this.m_artigo.getPossiveisAutoresCorrespondentes();
    }

    public void setCorrespondente(Autor autor)
    {
        this.m_artigo.setAutorCorrespondente(autor);
    }

    public void setFicheiro(String strFicheiro)
    {
        this.m_artigo.setFicheiro(strFicheiro);
    }

    public String getInfoResumo()
    {
        return this.m_submissao.getInfo() + this.m_artigo.getInfo();
    }

    public boolean registarSubmissao()
    {
        this.m_submissao.setArtigo(m_artigo);
        return this.m_evento.getListaSubmissoes().addSubmissao(m_submissao);
    }

    // adicionado na iteração 2
    public void setListaTopicosArtigo(ListaTopicos listaTopicosArtigo)
    {
        m_artigo.setListaTopicos(listaTopicosArtigo);
    }
    
    
    
    public boolean verificaMailAutor (String mail) {
        List<Autor> la = m_artigo.getM_listaAutores();
        boolean b = true;
        for (int i = 0; i < la.size(); i++) {
            
            if (la.get(i).getM_strEMail().equals(mail)) {
                b= false;
                break;
            }
        }
        return b;
    }

    /**
     * @param m_artigo the m_artigo to set
     */
    public void setM_artigo(Artigo m_artigo) {
        this.m_artigo = m_artigo;
    }
    
    public void setEstado(){
        this.m_submissao.setCriada();
    }

}
