/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import java.util.List;
import eventoscientificos.eventostate.*;
import eventoscientificos.model.*;
import java.util.ArrayList;

/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */
public class ValidarEventoImportadoController {

    /**
     * Empresa.
     */
    private Empresa empresa;
    /**
     * RegistoEventos temp que vai ter somente os Eventos no estado Criado.
     * *mudar mais tarde*
     */
    private List<Evento> temp;
    private Evento m_evento;

    /**
     * Constrói uma instância de ValidarEventoImportadoController.
     *
     * @param empresa
     */
    public ValidarEventoImportadoController(Empresa empresa) {
        this.empresa = empresa;

    }

    public void setM_evento(Evento m_evento) {
        this.m_evento = m_evento;
    }

    public Evento getM_evento() {
        return m_evento;
    }

    /**
     * Método que guarda uma Lista Temporária de Eventos com Estado
     * EventoCriadoState.*mais tarde mudar*.
     *
     * @return
     */
    public void registoEventosTemp() {
        List<Evento> le = new ArrayList<>();
        for (Evento e : this.empresa.getRegistaEvento().getListaEventos()) {
            if (e.getEstado() instanceof EventoCriadoCSVState) {
                le.add(e);
            }
        }
        this.temp = le;
    }

    /**
     * Regista Evento na Empresa.
     */
    public void RegistarCompleto() {
        for (Evento e : this.temp) {
            for (Evento i : this.empresa.getRegistaEvento().getListaEventos()) {
                if (e.equals(i)) {
                    i.setEstado(new EventoLidoCSVState(i));
                    /**
                     * Optar por uma das duas.
                     */
                    //i.setEstado(e.getEstado());
                }
                /* if ( i.getEstado() instanceof EventoCriadoCSVState)
                 this.empresa.getRegistaEvento().getListaEventos().remove(i);*/
            }

        }
    }

    public List<Evento> getListaTemp() {
        return this.temp;
    }

    public void setEstadoRegistado() {
        this.m_evento.setEstado(new EventoRegistadoState(m_evento));
    }
}
