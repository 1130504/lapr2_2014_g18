/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.model.*;
import java.util.List;

/**
 * Control
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class DefinirValorRegistoController implements StateController{
    
    /**
     * Evento.
     */
    private Evento m_evento;
    /**
     * Empresa.
     */
    private Empresa m_empresa;

    public DefinirValorRegistoController(Empresa empresa) {
        this.m_empresa = empresa;
    }

    /**
     * Define o evento
     *
     * @param evento
     */
    public void setEvento(Evento evento) {
        this.m_evento = evento;
    }

    /**
     * Define a empresa
     *
     * @param empresa
     */
    public void setEmpresa(Empresa empresa) {
        this.m_empresa = empresa;
    }

    /**
     * Devolve o evento
     *
     * @return
     */
    public Evento getEvento() {
        return m_evento;
    }

    /**
     * Devolve a empresa
     *
     * @return
     */
    public Empresa getEmpresa() {
        return m_empresa;
    }

    /**
     * Devolve uma lista de eventos no estado Registado
     *
     * @param ID
     * @return
     */
    public List<Evento> iniciarDefinirValorRegisto(String ID) {
        List<Evento> leTemp = this.m_empresa.getRegistaEvento().getEventosOrganizador(ID, this.m_empresa);
//        List<Evento> le = null;
//        for (Evento evento : leTemp) {
//            if (evento.getEstado().equals(new EventoRegistadoState(this.m_evento))) {
//                le.add(evento);
//            }
//        }
        // a implementar devido aos estados
        return leTemp;
    }

    /**
     * Define os valores de cada tipo de artigo do evento
     *
     * @param valorShort
     * @param valorFull
     * @param valorPoster
     */
    public void setValores(double valorShort, double valorFull, double valorPoster, int formula) {
        this.m_evento.setValorShort(valorShort);
        this.m_evento.setValorFull(valorFull);
        this.m_evento.setValorPoster(valorPoster);
        if (formula ==1 ) {
            Formula1 f1 = new Formula1();
            this.m_evento.setM_formula(f1);
        }else{
            Formula2 f2 = new Formula2();
            this.m_evento.setM_formula(f2);
        }
        //this.m_evento.setM_formula(null);
    }

    @Override
    public boolean alterarEstado() {
    return this.m_evento.getEstado().setValoresRegistoDefinido();
    }
}
