/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;


import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.RegistoEventos;
import eventoscientificos.model.Topico;
import java.util.List;

/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */

public class CriarTopicoEventoController implements StateController{
    /**
     * Empresa.
     */
    private Empresa m_empresa;
    /**
     * Evento.
     */
    private Evento m_evento;
    /**
     * RegistoEventos.
     */
    private RegistoEventos registoEvento = new RegistoEventos();
    

    public CriarTopicoEventoController(Empresa empresa) 
    {
        m_empresa = empresa;
    }
    
    public List<Evento> getEventosOrganizador(String strId)
    {
        return registoEvento.getEventosOrganizador(strId, this.m_empresa);
    }
    
   public void setEvento(Evento e)
   {
        m_evento = e;
   }
   public Topico addTopico(String strCodigo, String strDescricao)
   {
       Topico t = m_evento.novoTopico();
       
       t.setCodigoACM(strCodigo);
       t.setDescricao(strDescricao);
       
       
       if ( m_evento.validaTopico(t))
           return t;
       else
           return null;
   }
   
   public boolean registaTopico(Topico t)
   {
       return m_evento.addTopico(t);
   }
   /**
    * Altera o Estado do Evento para EventoTopicosCriados state.
    * @return 
    */
    @Override
    public boolean alterarEstado() {
        return this.m_evento.getEstado().setTopicosCriados();
    }
    
}

