/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;
import eventoscientificos.model.Distribuicao;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.ProcessoDistribuicao;
import eventoscientificos.model.Revisor;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class DistribuirRevisoesController implements StateController{

    private Evento m_evento;
    private Empresa m_empresa;
    private ProcessoDistribuicao m_distribuicoes = new ProcessoDistribuicao();

    public DistribuirRevisoesController(Empresa empresa) {
        this.m_empresa = empresa;
    }

    /**
     * Devolve o evento
     *
     * @return
     */
    public Evento getEvento() {
        return m_evento;
    }

    /**
     * Devolve a empresa
     *
     * @return
     */
    public Empresa getEmpresa() {
        return m_empresa;
    }

    /**
     * Define o evento
     *
     * @param evento
     */
    public void setEvento(Evento evento) {
        this.m_evento = evento;
    }

    /**
     * Define a empresa
     *
     * @param empresa
     */
    public void setEmpresa(Empresa empresa) {
        this.m_empresa = empresa;
    }

    /**
     * Cria uma lista de eventos de organizador recenbendo por parâmetro a ID do
     * mesmo.
     *
     * @param strId ID Organizador
     * @return lista de eventos do Organizador.
     */
    public List<Evento> getEventosOrganizador(String strId) {
        return m_empresa.getRegistaEvento().getEventosOrganizador(strId, m_empresa);
    }

    public ArrayList<Revisor> getListaRevisores() {
        ArrayList<Revisor> lr = (ArrayList<Revisor>) this.m_evento.getCP().getM_listaRevisores();
        return lr;
    }

    public void setDistribuicao(Distribuicao d) {
        this.m_distribuicoes.getListaDistribuicoes().add(d);
        this.m_evento.setM_processoDistribuicao(m_distribuicoes);
    }

    public List<Distribuicao> getDistribuicoes() {
        return this.m_distribuicoes.getListaDistribuicoes();
    }
    
    public String mostraDistribuicao(Distribuicao d){
        return d.toString();
    }
    
    public Distribuicao novaDistribuicao(){
        return new Distribuicao();
    }
       /**
        * Torna o Estado do Evento - EventoDistribuidoController.
        * @return 
        */
    @Override
    public boolean alterarEstado() {
     return this.m_evento.getEstado().setDistribuido();
    }
    

}
