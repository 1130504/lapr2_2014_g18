package eventoscientificos.controller;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.eventostate.*;
import eventoscientificos.model.RegistoEventos;
import eventoscientificos.model.Utilizador;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Pedro Miguel Gomes < aka Dante w-email: 1130383@isep.ipp.pt>
 */
public class CriarEventoCientificoController implements StateController{

    /**
     * Empresa.
     */
    private Empresa m_empresa;
    /**
     * Evento.
     */
    private Evento m_evento;
    /**
     * Lista de Eventos.
     */
    private List<Evento> m_listaEventos;
    /**
     * Registo Eventos.
     */
    private RegistoEventos m_registoEventos;

    /**
     * Estado do Evento.
     */
    private EventoState m_estado;

    /**
     * Constrói uma instância CriarEventoCientíficoController.
     *
     * @param empresa
     */
    public CriarEventoCientificoController(Empresa empresa) {
        m_empresa = empresa;
        m_registoEventos = empresa.getRegistaEvento();
    }
    /**
     * Seleciona Evento.
     * @param e 
     */
    public void selectEvento(Evento e){
        m_evento = e;
        m_estado = new EventoCriadoState(m_evento);
    }

    /**
     * Novo Evento.
     */
    public void novoEvento() {
        m_evento = m_registoEventos.novoEvento();
    }

    /**
     * Retorna numa string os Título dos Eventos de toda a Lista de Eventos.
     *
     * @return
     */
    @Override
    public String toString() {
        return m_registoEventos.toString();
    }

    /**
     * Altera/Define Título do Evento.
     *
     * @param strTitulo
     */
    public void setTitulo(String strTitulo) {
        m_evento.setTitulo(strTitulo);
    }

    /**
     * Altera/Define Descrição do Evento.
     *
     * @param strDescricao
     */
    public void setDescricao(String strDescricao) {
        m_evento.setDescricao(strDescricao);
    }

    /**
     * Altera/Define Local do Evento.
     *
     * @param strLocal
     */
    public void setLocal(String strLocal) {
        m_evento.setLocal(strLocal);
    }

    /**
     * Altera/Define Data Inicial do Evento.
     *
     * @param strDataInicio
     */
    public void setDataInicio(Date strDataInicio) {
        m_evento.setDataInicio(strDataInicio);
    }

    /**
     * Altera/Define Data Final do Evento.
     *
     * @param strDataFim
     */
    public void setDataFim(Date strDataFim) {
        m_evento.setDataFim(strDataFim);
    }

    /**
     * Altera/Define DataLimiteSubmissãoArtigo do Evento.
     *
     * @param strDataLimiteSubmissão
     */
    public void setDataLimiteSubmissão(Date strDataLimiteSubmissão) {
        m_evento.setDataLimiteSubmissão(strDataLimiteSubmissão);
    }

    /**
     * setRegistaEvento.
     *
     * @param re
     */
    public void setRegistaEvento(RegistoEventos re) {
        m_registoEventos = re;
    }

    /**
     * Modifica a data limite de registo
     *
     * @param dataLimiteRegisto
     */
    public void setDataLimiteRegisto(Date dataLimiteRegisto) {
        m_evento.setDataLimiteRegisto(dataLimiteRegisto);
    }

    /**
     * Modifica a data limite de submissão final
     *
     * @param dataLimiteSubmissaoFinal
     */
    public void setDataLimiteSubmissaoFinal(Date dataLimiteSubmissaoFinal) {
        m_evento.setDataLimiteSubmissaoFinal(dataLimiteSubmissaoFinal);
    }

    /**
     * Modifica a data limite de revisão
     *
     * @param dataLimiteRevisao
     */
    public void setDataLimiteRevisao(Date dataLimiteRevisao) {
        m_evento.setDataLimiteRevisao(dataLimiteRevisao);
    }

    /**
     * Adiciona Organizador ao Evento.
     *
     * @param strId
     * @return
     */
    public boolean addOrganizador(String strId) {
        Utilizador u = m_empresa.getUtilizador(strId);

        if (u != null) {
            return m_evento.getM_listaOrganizadores().addOrganizador(strId, u);
        } else {
            return false;
        }
    }

    /**
     * Adiciona Evento à Lista de Eventos após validação do mesmo.
     *
     * @return
     */
    public boolean registaEvento() {
        return m_registoEventos.registoEventos(m_evento);
    }

    /**
     * Retorna Lista de Eventos
     *
     * @return
     */
    public List<Evento> getListaEventos() {
        m_listaEventos = m_registoEventos.getListaEventos();
        return m_listaEventos;
    }

    /**
     * setEmpresa.
     *
     * @param e
     */
    void setEmpresa(Empresa e) {
        this.m_empresa = e;
    }

    /**
     * Retorna Título do Evento.
     *
     * @return
     */
    public String getEventoString() {
        return m_evento.getM_strTitulo();
    }

    /**
     * Altera o estado do Evento para Criado senão tiver Estado definido,
     * se tiver passa para Registado.
     *
     * @return 
     */
    @Override
    public boolean alterarEstado() {
        if (m_evento.getEstado() == null){
        m_evento.getEstado().setcriado();
        return true;
        }else if (m_evento.getEstado() instanceof EventoCriadoState){ 
           m_evento.getEstado().setRegistado();
           return true;
        }else{
            return false;
        }
       }

}
