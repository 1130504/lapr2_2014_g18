/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.eventostate.EventoCriadoCSVState;
import eventoscientificos.eventostate.EventoCriadoState;
import eventoscientificos.model.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class EditarEventoController implements StateController{

    /**
     * Empresa
     */
    private Empresa m_empresa;
    /**
     * Evento
     */
    private Evento m_evento;

    /**
     * Contrutor a receber a empresa por parâmetro
     *
     * @param m_empresa
     */
    public EditarEventoController(Empresa m_empresa) {
        this.m_empresa = m_empresa;
    }

    /**
     * Define o evento
     *
     * @param evento
     */
    public void setEvento(Evento evento) {
        this.m_evento = evento;
    }

    /**
     * Devolve uma lista de eventos no estado Registado
     *
     * @param ID
     * @return
     */
    public List<Evento> iniciarEditarEvento(String ID) {
        List<Evento> leTemp = this.m_empresa.getRegistaEvento().getEventosOrganizador(ID, this.m_empresa);
        List<Evento> le = new ArrayList<>();
        for (Evento evento : leTemp) {
            if (evento.getEstado() instanceof EventoCriadoCSVState) {
                le.add(evento);
            }
        }

        return le;
    }

    public String getTitulo() {
        if (this.m_evento.getM_strTitulo() == null) {
            return "";
        } else {
            return this.m_evento.getM_strTitulo();
        }
    }

    public String getDescricao() {
        if (this.m_evento.getM_strDescricao() == null) {
            return "";
        } else {
            return this.m_evento.getM_strDescricao();
        }
    }

    public String getLocal() {
        if (this.m_evento.getM_local() == null) {
            return "";
        } else {
            return this.m_evento.getM_local().toString();
        }
    }

    public String getDataInicio() {
        if (this.m_evento.getM_strDataInicio() == null) {
            return "";
        } else {
            return this.m_evento.getM_strDataInicio().toString();
        }
    }

    public String getDataFim() {
        if (this.m_evento.getM_strDataFim() == null) {
            return "";
        } else {
            return this.m_evento.getM_strDataFim().toString();
        }
    }

    public String getDataLimSubmissao() {
        if (this.m_evento.getM_strDataLimiteSubmissão() == null) {
            return "";
        } else {
            return this.m_evento.getM_strDataLimiteSubmissão().toString();
        }
    }

    public String getDataLimRevisao() {
        if (this.m_evento.getDataLimiteRevisao() == null) {
            return "";
        } else {
            return this.m_evento.getDataLimiteRevisao().toString();
        }
    }

    public String getDataLimSubFinal() {
        if (this.m_evento.getDataLimiteSubmissaoFinal() == null) {
            return "";
        } else {
            return this.m_evento.getDataLimiteSubmissaoFinal().toString();
        }
    }

    public String getDataLimRegisto() {
        if (this.m_evento.getDataLimiteRegisto() == null) {
            return "";
        } else {
            return this.m_evento.getDataLimiteRegisto().toString();
        }
    }

       public void adicionaOrganizadores(List<Utilizador> lu) {
        for (Utilizador utilizador : lu) {
            m_evento.getM_listaOrganizadores().addOrganizador(utilizador.getM_strUsername(), utilizador);
        }

    }
   /**
    * Evento para Estado Criado *Devia ser para Registado*.
    * @return 
    */
    @Override
    public boolean alterarEstado() {
    this.m_evento.setEstado(new EventoCriadoState(m_evento));
    return true;
    }

    
}
