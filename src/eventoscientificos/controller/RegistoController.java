/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.model.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Alexandre
 */
public class RegistoController {

    /**
     * Evento
     */
    private Evento m_evento;

    /**
     * Empresa
     */
    private Empresa m_empresa;

    /**
     * Registo
     */
    private Registo m_registo;

    /**
     *
     * @param ep
     */
    public RegistoController(Empresa ep) {
        this.m_empresa = ep;

    }

    public void novoRegistoAutor() {
        this.m_registo = new Registo(this.m_evento);
    }

    public void obterSubmissoes() {
        this.m_registo.obterSubmissoes();
    }

    public void setEvento(Evento e) {
        this.m_evento = e;
    }
    
    

}
