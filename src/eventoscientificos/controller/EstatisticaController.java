 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.model.*;
import eventoscientificos.GUI.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fábio Ferreira <1131242@isep.ipp.pt>
 */
public class EstatisticaController {
    /**
     * variavel com a empresa
     */
    private Empresa m_empresa;
    /**
     * variacel com o evento
     */
    private Evento m_evento;
    /**
     * variavel com a classe estatistica
     */
    private Estatistica m_estatistica;
    
    /**
     * construtor
     * @param empresa 
     */
    public EstatisticaController(Empresa empresa) {
        this.m_empresa = empresa;
        this.m_estatistica = new Estatistica(m_empresa);
    }
    
    /**
     * get do evento
     * @return 
     */
    public Evento getEvento() {
        return this.m_evento;
    }
    
    /**
     * set do evento
     * @param e 
     */
    public void selecionaEvento(Evento e) {
        this.m_evento = e;
    }
    
    /**
     * vai buscar os eventos do organizador
     * @param strId
     * @return 
     */
    public List<Evento> getEventosOrganizador(String strId) {
        return this.m_empresa.getRegistaEvento().getEventosOrganizador(strId, this.m_empresa);
    }
    
    /**
     * calcula a  taxa de aceitação do evento
     * @return 
     */
    public double[] taxaDeAceitacao() {

        return m_estatistica.taxaDeAceitacao(m_evento);
    }
    
    /**
     * calcula a media dos parametros do evento
     * @param tipo
     * @return 
     */
    public double[] mediaParametros(String tipo) {

        return m_estatistica.mediaParametros(tipo, m_evento);

    }
    /**
     * vai buscar todas as revisoes da empresa
     * @return 
     */
    private List<Revisao> todasAsRevisoes() {

        List<Revisao> listaRevisoesTodas = new ArrayList<>();
        List<Evento> listaEvento = this.m_empresa.getRegistaEvento().getListaEventos();

        for (Evento evento : listaEvento) {
            List<Submissao> listaSubmissoes = evento.getListaSubmissoes().getListaSubmissao();
            for (Submissao submissao : listaSubmissoes) {
                List<Revisao> listaRevisoes = submissao.getArtigo().getListaRevisoes();
                for (Revisao revisao : listaRevisoes) {
                    listaRevisoesTodas.add(revisao);
                }
            }
        }
        
        return listaRevisoesTodas;

    }
    /**
     * vai buscar todos os revisores da empresa
     * @return 
     */
    private List<Revisor> todosRevisores() {
        List<Evento> lisataEvento = this.m_empresa.getRegistaEvento().getListaEventos();
        List<Revisor> listaRevisores = new ArrayList<>();
        for (Evento evento : lisataEvento) {

            CP cp = evento.getCP();
            List<Revisor> listaRevisorestemp = cp.getListaRevisores();
            for (Revisor revisor : listaRevisorestemp) {
                    listaRevisores.add(revisor);
            }

        }
       
        return listaRevisores;
    }
    
    /**
     * ve quais os revisores com mais de 30 artigos
     * @param listaRevisores
     * @param listaRevisao
     * @return 
     */
    private List<Revisor> revisoresParaCalcular(List<Revisor> listaRevisores, List<Revisao> listaRevisao) {
        List<Revisor> revisoresParaCalcular = new ArrayList<>();
        int c = 0;
        for (Revisor revisor : listaRevisores) {
            c = 0;
            for (Revisao revisao : listaRevisao) {

                if (revisao.getM_revisor().getUtilizador().getM_strUsername().equals(revisor.getUtilizador().getM_strUsername())) {
                    c++;
                }
            }
            if (c >= 30) {
                revisoresParaCalcular.add(revisor);
            }
        }
        
        return revisoresParaCalcular;

    }
    /**
     * ve quais revisores tem menos de 30 artigos
     * @param listaRevisores
     * @param listaRevisao
     * @return 
     */
    private List<Revisor> revisoresInuteis(List<Revisor> listaRevisores, List<Revisao> listaRevisao) {

        List<Revisor> revisoresInuteis = new ArrayList<>();
        int c = 0;
        for (Revisor revisor : listaRevisores) {
            c = 0;
            for (Revisao revisao : listaRevisao) {

                if (revisao.getM_revisor().getUtilizador().getM_strUsername().equals(revisor.getUtilizador().getM_strUsername())) {
                    c++;
                }
            }
            if (c <= 30) {
                revisoresInuteis.add(revisor);
            }
        }
        for (Revisor revisor : revisoresInuteis) {
            System.out.println(revisor.toString());    
        }
        for (Revisor revisor : revisoresInuteis) {
            System.out.println(revisor.toString());
        }
        return revisoresInuteis;

    }
/**
 * ve quais são os revisores a alertar
 * @param revisoresParaCalcular
 * @return 
 */
    private List<Revisor> revisoresAlertar(List<Revisor> revisoresParaCalcular) {
        
        List<Revisor> alertados = new ArrayList<>();
        for (Revisor revisor : revisoresParaCalcular) {
            if (qualidadeRevisor(revisor).equals("Rejeitado")) {
                alertados.add(revisor);
            }
        }
        return alertados;
    }
    /**
     * cria a string dos revisores a alertar
     * @return 
     */
    public String stringEstatistica() {
        
        List<Revisao> todasAsRevisoes = todasAsRevisoes();
        List<Revisor> todosRevisores = todosRevisores();
        List<Revisor> revisoresParaCalcular = revisoresParaCalcular(todosRevisores, todasAsRevisoes);
        List<Revisor> revisoresInuteis = revisoresInuteis(todosRevisores, todasAsRevisoes);
        List<Revisor> revisoresAlertar = revisoresAlertar(revisoresParaCalcular);

        String str = "### Lista de Revisores que não entram na estatistica(menos de 30 artigos)###";
        for (Revisor revisor : revisoresInuteis) {
            if (revisoresInuteis.isEmpty()) {
                str+="Todos os revisores têm mais de 30 artigos";
            } else {
            str += revisor.getNome() + "\n";
            }
        }
        str += "### Lista de Revisores a alertar: ###";
        for (Revisor revisor : revisoresAlertar) {
            if (revisoresAlertar.isEmpty()) {
                str+="Não há revisores para alertar!";
            } else {
            str += revisor.getNome() + "\n";
            }
        }

        return str;
    }
    /**
     * calcula de rejeita ou aceita
     * @param revisor
     * @return 
     */
    private String qualidadeRevisor(Revisor revisor) {
        
        List<Revisao> lr = m_estatistica.revisoesDoRevisor(revisor);
        double desvios[] = m_estatistica.calculaTodosDesvios(lr);
                
               double mediaDesvios = m_estatistica.mediaDosDesvios(desvios),
                s = m_estatistica.calculaS(desvios),
                z = m_estatistica.calculaZ(mediaDesvios, s, desvios);
               

        return m_estatistica.aceitaRejeita(z);

    }

}
