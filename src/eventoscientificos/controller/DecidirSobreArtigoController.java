/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Decisao;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.ProcessoDecisao;
import java.util.List;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class DecidirSobreArtigoController {
     /**
     * Empresa
     */
    private Empresa m_empresa;
    
    /**
     * Evento
     */
    private Evento m_evento;
    /**
     * Construtor do controller a receber empresa por parâmetro
     * @param empresa 
     */
    public DecidirSobreArtigoController(Empresa empresa) {
        this.m_empresa = empresa;
    }
     /**
     * Cria uma lista de eventos de organizador recenbendo por parâmetro a ID do mesmo.
     * @param strId ID Organizador
     * @return lista de eventos do Organizador.
     */
    public List<Evento> getEventosOrganizador(String strId) {
        return this.m_empresa.getRegistaEvento().getEventosOrganizador(strId, this.m_empresa);
    }
    /**
     * Define o evento 
     * @param e 
     */
    public void selectEvento(Evento e){
        this.m_evento = e;
    }
    
    public List<Submissao> getSubmissoes(){
        return this.m_evento.getListaSubmissoes().getListaSubmissao();
    }
    
    public boolean registaDecisao(Artigo a, int i){
        Decisao d = new Decisao(a, i);
        ProcessoDecisao pd = this.m_evento.getM_processoDecisao();
        return pd.addDecisao(d);
    }
    
}
