package eventoscientificos.controller;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.CP;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisao;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Submissao;
import eventoscientificos.submissaostate.*;
import java.util.List;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class ReverArtigoController implements StateController {

    private Empresa m_empresa;
    private Evento m_evento;
    private Revisao revisao;
    private Artigo m_artigo;
    private CP cp;

    public ReverArtigoController(Empresa empresa) {
        m_empresa = empresa;
    }

    public void setM_evento(Evento m_evento) {
        this.m_evento = m_evento;
    }

    public void setRevisao(Revisao revisao) {
        this.revisao = revisao;
    }

    public void selectEvento(Evento evento) {
        this.m_evento = evento;
    }

    public List<Evento> getEventosRevisor(String revisorID) {
        return this.m_empresa.getRegistaEvento().getEventosRevisor(revisorID, this.m_empresa);
    }

    public List<Artigo> getArtigosRevisor(String revisorID) {
        return this.m_evento.getM_processoDistribuicao().getListaArtigos(this.m_evento.getCP().getMembroCP(revisorID));
    }

    public void setArtigo(Artigo a) {
        this.m_artigo = a;
    }

    public void setDados(int adequacao, int confianca, int originalidade, int qualidade, String recomendacao, String justificacao, Revisor id) {
        this.revisao.setAdequacao(adequacao);
        this.revisao.setConfianca(confianca);
        this.revisao.setOriginalidade(originalidade);
        this.revisao.setQualidade(qualidade);
        this.revisao.setRecomendacao(recomendacao);
        this.revisao.setTexto(justificacao);
        this.revisao.setM_revisor(id);
    }

    public Revisao novaRevisao() {
        return this.revisao = new Revisao();
    }

    public void registaRevisao(Revisao r) {
        this.m_artigo.getListaRevisoes().add(r);
    }

    /**
     * Altera os Estado do Evento para EventoRevistoController (Todos os Artigos
     * têm que estar Revistos).
     *
     * @return
     */
    @Override
    public boolean alterarEstado() {
        return this.m_evento.getEstado().setRevisto();
    }

    public Submissao getSubmissao(Artigo a) {
        for (Submissao s : this.m_evento.getListaSubmissoes().getListaSubmissao()) {
            if (s.getArtigo() == a) {
                return s;
            }
        }
        return null;
    }

    public void setEstado(Submissao s) {
        s.setEstado(new SubmissaoRevistaState(s));
    }

    public boolean verificaEstado(Artigo artigo) {
        Submissao s = getSubmissao(artigo);
        return s.getState() instanceof SubmissaoDistribuidaState;
    }

}
