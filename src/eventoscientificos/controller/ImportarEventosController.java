/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.model.*;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.xml.sax.SAXException;
import eventoscientificos.imports.CSVFile;
import eventoscientificos.imports.XMLFile;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */
public class ImportarEventosController {

    /**
     * Empresa.
     */
    private Empresa m_empresa;

    /**
     * Eventos.
     */
    private List <Evento> temp;

    /**
     * Caminho do ficheiro.
     */
    private String path_file;

    /**
     * Extensão.
     */
    private String ext;
    
   

    /**
     * Constrói uma instância ImportarEventosController.
     *
     * @param m_empresa
     */
    public ImportarEventosController(Empresa m_empresa) {

        this.m_empresa = m_empresa;
     

        
    }

    /**
     * Ambos os métodos podem ser substituídos por uma interface.
     */
    /**
     * Define a Extensão do Ficheiro.
     *
     * @param FileName
     */
    public void setExtensao(String FileName) {
        this.ext = extFicheiro(FileName);
    }

    /**
     * Retorna a Extensão do Ficheiro
     *
     * @return
     */
    public String getExtensao() {
        return this.ext;
    }
    
    /**
      * Define Diretório do Ficheiro a importar.
      * @param path 
      */
     public void setPathFile(String path){
         this.path_file = path;
     }
     /**
      * Retorna Caminho do Ficheiro.
      * @return 
      */
     public String getPathFile(){
         return this.path_file;
     }
     /**
      * Retorna Lista Eventos.
      * @return 
      */
     public List<Evento> getRegistoEventos(){
         return this.temp;
     }

    /**
     * Verifica a extensão do ficheiro importado.
     *
     * @param path_file
     * @return h_file
     */
    public String extFicheiro(String path_file) {
        String s = new String();
        int p = path_file.lastIndexOf(".");
        if (p > -1) {
            s = path_file.substring((p + 1));
        }
        return s;
    }

    /**
     * Retorna uma Lista de Eventos importada através do ficheiro lido.
     *
     * @throws IOException
     * @throws NullPointerException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws javax.xml.xpath.XPathExpressionException
     */
    public void ListaEventos() throws IOException, NullPointerException, ParserConfigurationException, SAXException, XPathExpressionException {
       
        if (this.ext.equalsIgnoreCase("csv")) {
            CSVFile csvFile = new CSVFile(path_file, m_empresa);
          this.temp= csvFile.lerFicheiroCsv();
            
        } else if (this.ext.equalsIgnoreCase("xml")) {
            XMLFile xmlfile= new XMLFile(path_file, m_empresa);
            //Retorna ListaEventos no Estado EventoCriadoState.
            this.temp= xmlfile.lerXMLFile();     
        }
        
    
    
    }
    
    public void guardarEventos(){
        
    
        for (Evento e : this.temp){
            /**
             * Adiciona Eventos à Empresa somente no EventoCriadoState.
             */
        this.m_empresa.getRegistaEvento().addEvento(e);

    
        }
    }}


