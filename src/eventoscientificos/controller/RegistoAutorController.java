/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.model.*;
import eventoscientificos.pagamentos.SistemaPagamento;
import java.util.Date;

/**
 *
 * @author Alexandre Carreira 
 */
public class RegistoAutorController {

    /**
     * Empresa
     */
    private final Empresa empresa;

    /**
     * Registo
     */
    private RegistoAutor m_registo;
    
    /**
     * Registo de Processo de Pagamento
     */
    private RegistoProcessoPagamento m_registoProcessoPagamento;
    
    /**
     * Processo de Pagamento
     */
    private ProcessoPagamento m_ProcessoPagamento;
    
    /**
     * Pagamento
     */
    private Pagamento m_Pagamento;
    
    /**
     * Evento
     */
    private Evento m_evento;
    
    /**
     * Autor
     */
    private Autor m_autor;
    
    /**
     * Construtor recebendo empresa como parâmetro
     *
     * @param empresa
     */
    public RegistoAutorController(Empresa empresa) {
        this.empresa = empresa;
        
        
    }
    
    /**
     * Modifica o Evento
     * @param e 
     */
    public void setEvento(Evento e){
        this.m_evento = e;
        this.m_registoProcessoPagamento = new RegistoProcessoPagamento(this.m_evento, this.m_evento.getListaSubmissoes());
    }

    public Autor getM_autor() {
        return this.m_autor;
    }
    
    
    
    /**
     * Modifica o Autor
     * @param a
     */
    public void setAutor(Autor a){
        this.m_autor = a;
    }
    /**
     * 
     * Obtrém as submissões dos artigos
     * @param a
     * @return 
     */
    public ListaSubmissoes obterSubmissoes(Autor a){
        return this.m_registoProcessoPagamento.obterSubmissoes(a);
    }

    /**
     * Modifica o Sistema de Pagamento
     * @param sistema 
     */
    public void setSistemaPagamento(SistemaPagamento sistema) {
        this.m_registo.setSistemaPagamento(sistema);
        
    }
    
   /**
    * Modifica o número do cartão de crédito.
    * @param num numro do cartao de credito
    */ 
    public void setNumeroCartaoCredito(String num){
        this.m_Pagamento.setNumeroCartaoCredito(num);
    }

    /**
     * Modifica a validade do cartão de crédito
     * @param data data de validade do cartao de credito
     */
    public void setDataValidadeCC(Date data){
        this.m_Pagamento.setDataValidadeCC(data);
    }
    
}
