/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.GUI;

import eventoscientificos.controller.DecidirSobreArtigoController;
import eventoscientificos.model.*;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class DecidirSobreArtigoUI extends JDialog {

    /**
     * A instância da Empresa.
     */
    private Empresa m_empresa;
    /**
     * A instância de DistribuirRevisoesController.
     */
    private DecidirSobreArtigoController m_controllerDSA;
    /**
     * Artigo a considerar
     */
    private Artigo m_artigo;
    /**
     * ComboBox de submissoes
     */
    private JComboBox<Submissao> comboBoxSubmissoes;
    private List<Submissao> listaSubmissoes;
    private JButton btnAceitar, btnRejeitar;
    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;

    /**
     *
     * @param pai
     * @param empresa
     */
    public DecidirSobreArtigoUI(Frame pai, Empresa empresa) {
        super(pai, "Decidir sobre Artigo", true);

        this.m_empresa = empresa;
        this.m_controllerDSA = new DecidirSobreArtigoController(this.m_empresa);

        String OrgId = this.inserirID();
        List<Evento> le = m_controllerDSA.getEventosOrganizador(OrgId);

        if (OrgId != null) {
            if (!le.isEmpty()) {
                Evento e = this.mostraEventos(le);

                this.m_controllerDSA.selectEvento(e);
                JPanel p1 = criarPainelComboboxArtigos();
                JPanel p2 = criarPainelBotoes();

                add(p1, BorderLayout.NORTH);
                add(p2, BorderLayout.SOUTH);
                this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
                setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);

                pack();
                setResizable(false);
                setVisible(true);
            } else {
                this.mensagem("Organizador inválido ou sem Eventos.", JOptionPane.ERROR_MESSAGE);
            }
        }

    }

    /**
     * Apresenta uma janela para inserir o id.
     *
     * @return String id
     */
    private String inserirID() {
        return (JOptionPane.showInputDialog(DecidirSobreArtigoUI.this,
                "Insira o ID do Organizador:", "Decidir sobre artigo",
                JOptionPane.DEFAULT_OPTION));
    }

    /**
     * Método para apresentar numa janela a lista de eventos recebida por
     * parâmetro.
     *
     * @param listaEventos a lista de eventos a apresentar
     * @return o Evento escolhido
     */
    private Evento mostraEventos(List<Evento> listaEventos) {

        String[] opcoes = new String[listaEventos.size()];
        int aux = 1;
        for (int i = 0; i < opcoes.length; i++) {
            opcoes[i] = aux + i + " - " + listaEventos.get(i).getM_strTitulo();
        }

        String evento = (String) JOptionPane.showInputDialog(DecidirSobreArtigoUI.this,
                "Escolha um evento:", "",
                JOptionPane.PLAIN_MESSAGE,
                null,
                opcoes,
                opcoes[0]);
        if (evento == null) {
            return null;
        }
        String[] parts = evento.split(" - ");
        return (listaEventos.get(Integer.parseInt(parts[0]) - 1));
    }

    private JPanel criarPainelComboboxArtigos() {
        Submissao[] submissoes = this.m_controllerDSA.getSubmissoes().toArray((new Submissao[this.m_controllerDSA.getSubmissoes().size()]));
        this.comboBoxSubmissoes = new JComboBox<>(submissoes);
        this.comboBoxSubmissoes.setMaximumRowCount(5);

        JPanel p = new JPanel();
        p.setBorder(new EmptyBorder(10, 0, 10, 0));
        p.add(this.comboBoxSubmissoes);
        return p;
    }

    /**
     * Cria o painel de botões
     *
     * @return painel
     */
    private JPanel criarPainelBotoes() {
        btnAceitar = criarBotaoAceitar();
        getRootPane().setDefaultButton(btnAceitar);

        btnRejeitar = criarBotaoRejeitar();
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnAceitar);
        p.add(btnRejeitar);

        return p;
    }

    /**
     * Cria botão Aceitar
     *
     * @return botão
     */
    private JButton criarBotaoAceitar() {
        JButton btn = new JButton("Aceitar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Submissao s = (Submissao) comboBoxSubmissoes.getSelectedItem();
                Artigo a = s.getArtigo();

                if (m_controllerDSA.registaDecisao(a, 0)) {
                    mensagem("Decisão guardada com sucesso", JOptionPane.INFORMATION_MESSAGE);
                    dispose();
                } else {
                    mensagem("Erro ao registar decisão", JOptionPane.ERROR_MESSAGE);
                    dispose();
                }

            }
        });
        return btn;
    }

    /**
     * Cria o botão Rejeitar
     *
     * @return JButton
     */
    private JButton criarBotaoRejeitar() {
        JButton btn = new JButton("Rejeitar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Submissao s = (Submissao) comboBoxSubmissoes.getSelectedItem();
                Artigo a = s.getArtigo();

                if (m_controllerDSA.registaDecisao(a, 1)) {
                    mensagem("Decisão guardada com sucesso", JOptionPane.INFORMATION_MESSAGE);
                    dispose();
                } else {
                    mensagem("Erro ao registar decisão", JOptionPane.ERROR_MESSAGE);
                    dispose();
                }
            }
        });
        return btn;
    }

    /**
     * Apresenta uma janela com uma mensagem recebida por parâmetro.
     *
     * @param texto da mensagem a apresentar
     */
    private void mensagem(String texto, int icon) {
        JOptionPane.showMessageDialog(DecidirSobreArtigoUI.this,
                texto,
                "Decidir sobre Artigo",
                icon);
    }

}
