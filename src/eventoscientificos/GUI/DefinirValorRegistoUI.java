/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.GUI;

import eventoscientificos.controller.DefinirValorRegistoController;
import eventoscientificos.model.*;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class DefinirValorRegistoUI extends JDialog {

    /**
     * A instância da Empresa.
     */
    private Empresa m_empresa;
    /**
     * A instância de DefinirValorRegistoController.
     */
    private DefinirValorRegistoController m_controllerDVR;
    /**
     * TextField para o valor dos Short
     */
    private JTextField valorShort;
    /**
     * TextField para o valor dos Full
     */
    private JTextField valorFull;
    /**
     * TextField para o valor dos Poster
     */
    private JTextField valorPoster;
    /**
     * TextField para a formula de pagamento 
     */
    private JTextField formula;
    /**
     * Botões Ok e Cancelar
     */
    private JButton btnOk, btnCancelar;
    
    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;
    /**
     * Contrutor que recebe como parâmetro o frame pai e a empresa
     * @param pai
     * @param empresa 
     */
    public DefinirValorRegistoUI(Frame pai, Empresa empresa) {
        super(pai, "Definir Valor de Registo", true);

        this.m_empresa = empresa;
        this.m_controllerDVR = new DefinirValorRegistoController(this.m_empresa);

        String OrgId = this.inserirID();
        
        List<Evento> le = m_controllerDVR.iniciarDefinirValorRegisto(OrgId);

        if (OrgId != null) {
            if (!le.isEmpty()) {
                Evento e = this.mostraEventos(le);

                this.m_controllerDVR.setEvento(e);
                JPanel p1 = criarPainelInfo();
                JPanel p2 = criarPainelBotoes();

                add(p1, BorderLayout.NORTH);
                add(p2, BorderLayout.SOUTH);
                this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
                setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
                pack();
                setResizable(false);
                setVisible(true);
            } else {
                this.mensagem("Organizador inválido ou sem Eventos.", JOptionPane.ERROR_MESSAGE);
            }
        }

    }

    /**
     * Cria o painel Info
     *
     * @return painel
     */
    private JPanel criarPainelInfo() {
        JPanel p1 = criarPainelShort();
        JPanel p2 = criarPainelFull();
        JPanel p3 = criarPainelPoster();
        JPanel p4 = criarPainelFormula();

        JPanel p = new JPanel(new GridLayout(4, 2));
        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);

        return p;
    }

    /**
     * Cria o painel Short
     *
     * @return painel
     */
    private JPanel criarPainelShort() {
        JLabel lbl = new JLabel("Valor Short:", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        valorShort = new JTextField(CAMPO_LARGURA);
        valorShort.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(valorShort);

        return p;
    }

    /**
     * Cria o painel Full
     *
     * @return painel
     */
    private JPanel criarPainelFull() {
        JLabel lbl = new JLabel("Valor Full:", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        valorFull = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(valorFull);

        return p;
    }

    /**
     * Cria o painel Poster
     *
     * @return painel
     */
    private JPanel criarPainelPoster() {
        JLabel lbl = new JLabel("Valor Poster: ", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        valorPoster = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(valorPoster);

        return p;
    }
     /**
     * Cria o painel Poster
     *
     * @return painel
     */
    private JPanel criarPainelFormula() {
        JLabel lbl = new JLabel("Formula de pagamento(1 ou 2) ", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        formula = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(formula);

        return p;
    }

    /**
     * Cria o painel de botões
     *
     * @return painel
     */
    private JPanel criarPainelBotoes() {
        btnOk= criarBotaoOK();
        getRootPane().setDefaultButton(btnOk);

        btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOk);
        p.add(btnCancelar);

        return p;
    }

    /**
     * Cria botão OK
     *
     * @return botão
     */
    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    double s = Double.parseDouble(valorShort.getText());
                    double f = Double.parseDouble(valorFull.getText());
                    double p = Double.parseDouble(valorPoster.getText());
                    int form = Integer.parseInt(formula.getText());
                    if (confirmar() == 0) {
                        m_controllerDVR.setValores(s, f, p, form);
                        dispose();
                    }
                    mensagem("Valores definidos com sucesso!", JOptionPane.INFORMATION_MESSAGE);
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(DefinirValorRegistoUI.this,
                            "Valores introduzidos inválidos",
                            "Definir Valor de Registo",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        return btn;
    }

    /**
     * Cria o botão Cancelar
     *
     * @return JButton
     */
    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                mensagem("Cancelado!", JOptionPane.INFORMATION_MESSAGE);
                dispose();
            }
        });
        return btn;
    }

    /**
     * Apresenta uma janela para inserir o id.
     *
     * @return String id
     */
    private String inserirID() {
        return (JOptionPane.showInputDialog(DefinirValorRegistoUI.this,
                "Insira o ID do Organizador:", "Definir Valor de Registo",
                JOptionPane.DEFAULT_OPTION));
    }

    /**
     * Apresenta uma janela com uma mensagem recebida por parâmetro.
     *
     * @param texto da mensagem a apresentar
     */
    private void mensagem(String texto, int icon) {
        JOptionPane.showMessageDialog(DefinirValorRegistoUI.this,
                texto,
                "Definir Valor de Registo",
                icon);
    }

    /**
     * Apresenta uma janela de confirmação e devolve um inteiro conforme a
     * decisão
     *
     * @return int resposta
     */
    private int confirmar() {
        String[] opcoes = {"Confirmar", "Cancelar"};
        int response = JOptionPane.showOptionDialog(this, "Confirma?", "Definir Valor de Registo", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
                null, opcoes, opcoes[0]);
        return response;
    }

    /**
     * Método para apresentar numa janela a lista de eventos recebida por
     * parâmetro.
     *
     * @param listaEventos a lista de eventos a apresentar
     * @return o Evento escolhido
     */
    private Evento mostraEventos(List<Evento> listaEventos) {

        String[] opcoes = new String[listaEventos.size()];
        int aux = 1;
        for (int i = 0; i < opcoes.length; i++) {
            opcoes[i] = aux + i + " - " + listaEventos.get(i).getM_strTitulo();
        }

        String evento = (String) JOptionPane.showInputDialog(DefinirValorRegistoUI.this,
                "Escolha um evento:", "Definir Valor de Registo",
                JOptionPane.PLAIN_MESSAGE,
                null,
                opcoes,
                opcoes[0]);
        if (evento == null) {
            return null;
        }
        String[] parts = evento.split(" - ");
        return (listaEventos.get(Integer.parseInt(parts[0]) - 1));
    }

}
