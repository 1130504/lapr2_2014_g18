/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.GUI;

import eventoscientificos.controller.EditarEventoController;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Utilizador;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class EditarEventoUI extends JDialog {

    
    /**
     * A instância da Empresa.
     */
    private Empresa m_empresa;
    /**
     * */
    private EditarEventoController m_controllerED;
    /**
     * Evento
     */
    private Evento m_evento;
    /**
     * TextField para o título
     */
    private JTextField titulo;
    /**
     * TextField para a descrição
     */
    private JTextField descricao;
    /**
     * TextField para o local
     */
    private JTextField local;
    /**
     * TextField para a data de início
     */
    private JTextField dataInicio;
    /**
     * TextField para a data de fim
     */
    private JTextField dataFim;
    /**
     * TextField para a data de registo
     */
    private JTextField dataRegisto;
    /**
     * TextField para a data de fim
     */
    private JTextField dataSubmissao;
    /**
     * TextField para a data limite submissão final
     */
    private JTextField dataSubmissaoFinal;
    /**
     * TextField para a data limite de revisao
     */
    private JTextField dataRevisao;
    /**
     * Botões Ok e Cancelar
     */
    private JButton btnOk, btnCancelar;
    private JList jListlistaUtilizadores;
    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;

    public EditarEventoUI(Frame pai, Empresa empresa) {
        super(pai, "Editar/Completar Evento", true);

        this.m_empresa = empresa;
        this.m_controllerED = new EditarEventoController(this.m_empresa);

        String OrgId = this.inserirID();
        List<Evento> le = m_controllerED.iniciarEditarEvento(OrgId);

        if (OrgId != null) {
            if (!le.isEmpty()) {
                Evento e = this.mostraEventos(le);
                
                
                this.m_controllerED.setEvento(e);
                this.m_evento = e;
                JPanel p1 = criarPainelInfo();
                JPanel p2 = criarPainelBotoes();
                JPanel p3 = criarPainelOrganizadores();
                
                add(p1, BorderLayout.NORTH);
                add(p2, BorderLayout.SOUTH);
                add(p3, BorderLayout.CENTER);
                this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
                setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
                titulo.setText(m_controllerED.getTitulo());
                descricao.setText(m_controllerED.getDescricao());
                local.setText(m_controllerED.getLocal());
                dataInicio.setText(m_controllerED.getDataInicio());
                dataFim.setText(m_controllerED.getDataFim());
                dataSubmissao.setText(m_controllerED.getDataLimSubmissao());
                dataSubmissaoFinal.setText(m_controllerED.getDataLimSubFinal());
                dataRegisto.setText(m_controllerED.getDataLimRegisto());
                dataRevisao.setText(m_controllerED.getDataLimRevisao());

                pack();
                setResizable(false);
                setVisible(true);
            } else {
                this.mensagem("Organizador inválido ou sem Eventos.", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Cria o painel Info
     *
     * @return painel
     */
    private JPanel criarPainelInfo() {
        JPanel p1 = criarPainelTitulo();
        JPanel p2 = criarPainelDescriçao();
        JPanel p3 = criarPainelLocal();
        JPanel p4 = criarPainelDataInicio();
        JPanel p5 = criarPainelDataFim();
        JPanel p6 = criarPainelDataSubmissao();
        JPanel p7 = criarPainelDataSubmissaoFinal();
        JPanel p8 = criarPainelDataRevisao();
        JPanel p9 = criarPainelDataRegisto();

        JPanel p = new JPanel(new GridLayout(9, 2));
        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);
        p.add(p5);
        p.add(p6);
        p.add(p7);
        p.add(p8);
        p.add(p9);

        return p;
    }

    /**
     * Cria o painel Título
     *
     * @return painel
     */
    private JPanel criarPainelTitulo() {
        JLabel lbl = new JLabel("Titulo: ", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        titulo = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(titulo);

        return p;
    }

    /**
     * Cria o painel Descrição
     *
     * @return painel
     */
    private JPanel criarPainelDescriçao() {
        JLabel lbl = new JLabel("Descrição: ", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        descricao = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(descricao);

        return p;
    }

    /**
     * Cria o painel Local
     *
     * @return painel
     */
    private JPanel criarPainelLocal() {
        JLabel lbl = new JLabel("Local: ", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        local = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(local);

        return p;
    }

    /**
     * Cria o painel data de inicio
     *
     * @return painel
     */
    private JPanel criarPainelDataInicio() {
        JLabel lbl = new JLabel("Data de Inicio: ", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        dataInicio = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(dataInicio);

        return p;
    }

    /**
     * Cria o painel data de fim
     *
     * @return painel
     */
    private JPanel criarPainelDataFim() {
        JLabel lbl = new JLabel("Data de Fim: ", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        dataFim = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(dataFim);

        return p;
    }

    /**
     * Cria o painel data limite de submissão
     *
     * @return painel
     */
    private JPanel criarPainelDataSubmissao() {
        JLabel lbl = new JLabel("Data limite de Submissão: ", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        dataSubmissao = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(dataSubmissao);

        return p;
    }

    /**
     * Cria o painel data limite de revisão
     *
     * @return painel
     */
    private JPanel criarPainelDataRevisao() {
        JLabel lbl = new JLabel("Data limite de Revisão: ", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        dataRevisao = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(dataRevisao);

        return p;
    }

    /**
     * Cria o painel data limite de registo
     *
     * @return painel
     */
    private JPanel criarPainelDataRegisto() {
        JLabel lbl = new JLabel("Data limite de Registo: ", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        dataRegisto = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(dataRegisto);

        return p;
    }

    /**
     * Cria o painel data limite de submissao final
     *
     * @return painel
     */
    private JPanel criarPainelDataSubmissaoFinal() {
        JLabel lbl = new JLabel("Data limite de Submissao Final: ", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        dataSubmissaoFinal = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(dataSubmissaoFinal);

        return p;
    }

    /**
     * Cria o painel de botões
     *
     * @return painel
     */
    private JPanel criarPainelBotoes() {
        btnOk = criarBotaoOK();
        getRootPane().setDefaultButton(btnOk);

        btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOk);
        p.add(btnCancelar);

        return p;
    }

    /**
     * Cria botão OK
     *
     * @return botão
     */
    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    if (titulo.getText().equals("")
                            || descricao.getText().equals("")
                            || local.getText().equals("")
                            || dataInicio.getText().equals("")
                            || dataFim.getText().equals("")
                            || dataSubmissao.getText().equals("")
                            || dataRevisao.getText().equals("")
                            || dataRegisto.getText().equals("")
                            || dataSubmissaoFinal.getText().equals("")) {
                        JOptionPane.showMessageDialog(EditarEventoUI.this, "Todos os campos são obrigatórios!\n", "Criar Evento", JOptionPane.ERROR_MESSAGE);

                    } else {
                        m_evento.setTitulo(titulo.getText());
                        m_evento.setDescricao(descricao.getText());
                        m_evento.setLocal(local.getText());
                        m_evento.setDataInicio( new Date(dataInicio.getText()));
                        m_evento.setDataFim(new Date (dataFim.getText()));
                        m_evento.setDataLimiteSubmissão(new Date(dataSubmissao.getText()));
                        m_evento.setDataLimiteSubmissaoFinal(new Date(dataSubmissaoFinal.getText()));
                        m_evento.setDataLimiteRegisto(new Date(dataRegisto.getText()));
                        m_evento.setDataLimiteRevisao(new Date(dataRevisao.getText()));
                        List<Utilizador> lu = jListlistaUtilizadores.getSelectedValuesList();
                        m_controllerED.adicionaOrganizadores(lu);
                        
                        mensagem("Evento registado com sucesso", JOptionPane.INFORMATION_MESSAGE);
                        m_controllerED.alterarEstado();
                        dispose();

                    }

                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(EditarEventoUI.this,
                            "Valores introduzidos inválidos",
                            "Definir Valor de Registo",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        );
        return btn;
    }

    /**
     * Cria o botão Cancelar
     *
     * @return JButton
     */
    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                mensagem("Cancelado!", JOptionPane.INFORMATION_MESSAGE);
                dispose();
            }
        });
        return btn;
    }

    /**
     * Apresenta uma janela para inserir o id.
     *
     * @return String id
     */
    private String inserirID() {
        return (JOptionPane.showInputDialog(EditarEventoUI.this,
                "Insira o ID do Organizador:", "Editar Evento",
                JOptionPane.DEFAULT_OPTION));
    }

    /**
     * Apresenta uma janela com uma mensagem recebida por parâmetro.
     *
     * @param texto da mensagem a apresentar
     */
    private void mensagem(String texto, int icon) {
        JOptionPane.showMessageDialog(EditarEventoUI.this,
                texto,
                "Editar Evento",
                icon);
    }

    /**
     * Apresenta uma janela de confirmação e devolve um inteiro conforme a
     * decisão
     *
     * @return int resposta
     */
    private int confirmar() {
        String[] opcoes = {"Confirmar", "Cancelar"};
        int response = JOptionPane.showOptionDialog(this, "Confirma?", "Editar Evento", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
                null, opcoes, opcoes[0]);
        return response;
    }

    /**
     * Método para apresentar numa janela a lista de eventos recebida por
     * parâmetro.
     *
     * @param listaEventos a lista de eventos a apresentar
     * @return o Evento escolhido
     */
    private Evento mostraEventos(List<Evento> listaEventos) {

        String[] opcoes = new String[listaEventos.size()];
        int aux = 1;
        for (int i = 0; i < opcoes.length; i++) {
            opcoes[i] = aux + i + " - " + listaEventos.get(i).getM_strTitulo();
        }

        String evento = (String) JOptionPane.showInputDialog(EditarEventoUI.this,
                "Escolha um evento a editar:", "",
                JOptionPane.PLAIN_MESSAGE,
                null,
                opcoes,
                opcoes[0]);
        if (evento == null) {
            return null;
        }
        String[] parts = evento.split(" - ");
        return (listaEventos.get(Integer.parseInt(parts[0]) - 1));
    }
   
    
    private JPanel criarPainelOrganizadores() {
        JLabel lbl = new JLabel("Escolha os organizadores adicionar ao evento", JLabel.RIGHT);
        List<Utilizador> listaUtilizadores = m_empresa.getM_listaUtilizadores().getM_listaUtilizadores();
        Utilizador[] array = listaUtilizadores.toArray(new Utilizador[listaUtilizadores.size()]);
        jListlistaUtilizadores = new JList(array);
        jListlistaUtilizadores.setVisibleRowCount(10);
        final int LISTA_LARGURA = 300, LISTA_ALTURA = 300;
        jListlistaUtilizadores.setPreferredSize(new Dimension(LISTA_LARGURA, LISTA_ALTURA));
        JScrollPane scrRevisores = new JScrollPane(jListlistaUtilizadores);

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10, MARGEM_ESQUERDA = 10,
                MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA, MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(lbl);
        p.add(scrRevisores);
        return p;
 
    }

}
