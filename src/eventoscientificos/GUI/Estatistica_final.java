/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.GUI;

import eventoscientificos.controller.EstatisticaController;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import java.awt.Frame;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class Estatistica_final extends JDialog {

    /**
     * A instância da Empresa.
     */
    private Empresa m_empresa;
    /**
     * A instância de DefinirValorRegistoController.
     */
    private EstatisticaController m_controllerE;

    public Estatistica_final(Frame pai, Empresa empresa) {

        super(pai, "Estatistica", true);
        this.m_empresa = empresa;
        this.m_controllerE = new EstatisticaController(this.m_empresa);

        String OrgId = this.inserirID();
        List<Evento> le = m_controllerE.getEventosOrganizador(OrgId);
        try {

            if (OrgId != null) {
                if (!le.isEmpty()) {
                    Evento e = this.mostraEventos(le);

                    this.m_controllerE.selecionaEvento(e);

                    double[] taxaDeAceitacao = m_controllerE.taxaDeAceitacao();

                    double[] mediaParametrosShort = m_controllerE.mediaParametros("short");
                    double[] mediaParametrosFull = m_controllerE.mediaParametros("full");
                    double[] mediaParametrosPoster = m_controllerE.mediaParametros("poster");
                    JOptionPane.showMessageDialog(Estatistica_final.this,apresentaEstatisticasEvento(taxaDeAceitacao, mediaParametrosShort, mediaParametrosFull, mediaParametrosPoster) ,
                "Estatistica", JOptionPane.INFORMATION_MESSAGE);
                    
                    
                    System.out.println(m_controllerE.stringEstatistica());
                }else{
                    mensagem("O organizador não tem eventos", JOptionPane.ERROR_MESSAGE);
                }
            }else{
                mensagem("Organizador nao registado", JOptionPane.ERROR_MESSAGE);
            }
        } catch (ArithmeticException e) {
            mensagem("Erro ao calcular a estatística!", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Apresenta uma janela para inserir o id.
     *
     * @return String id
     */
    private String inserirID() {
        return (JOptionPane.showInputDialog(Estatistica_final.this,
                "Insira o ID do Organizador:", "Estatistica",
                JOptionPane.DEFAULT_OPTION));
    }

    /**
     * Método para apresentar numa janela a lista de eventos recebida por
     * parâmetro.
     *
     * @param listaEventos a lista de eventos a apresentar
     * @return o Evento escolhido
     */
    private Evento mostraEventos(List<Evento> listaEventos) {

        String[] opcoes = new String[listaEventos.size()];
        int aux = 1;
        for (int i = 0; i < opcoes.length; i++) {
            opcoes[i] = aux + i + " - " + listaEventos.get(i).getM_strTitulo();
        }

        String evento = (String) JOptionPane.showInputDialog(Estatistica_final.this,
                "Escolha um evento para calcular estatistica:", "Estatística",
                JOptionPane.PLAIN_MESSAGE,
                null,
                opcoes,
                opcoes[0]);
        if (evento == null) {
            return null;
        }
        String[] parts = evento.split(" - ");
        return (listaEventos.get(Integer.parseInt(parts[0]) - 1));
    }

    private String apresentaEstatisticasEvento(double[] taxaDeAceitacao, double[] mediaParametrosShort, double[] mediaParametrosFull, double[] mediaParametrosPoster) {
        String result = ("### Estatisticas do evento: " + m_controllerE.getEvento().toString() + ": ####\n"
                + "A taxa de aceitação dos Artigos Short neste evento é: " + taxaDeAceitacao[0] + "%. \n"
                + "A taxa de aceitação dos Artigos Full neste evento é: " + taxaDeAceitacao[1] + "%. \n"
                + "A taxa de aceitação dos Artigos Full neste evento é: " + taxaDeAceitacao[2] + "%. \n"
                + "## Artigos Short - Media Parametros ##"
                + "A media dos valores obtidos na pergunta 1 é : " + mediaParametrosShort[0] + ". \n"
                + "A media dos valores obtidos na pergunta 2 é : " + mediaParametrosShort[1] + ". \n"
                + "A media dos valores obtidos na pergunta 3 é : " + mediaParametrosShort[2] + ". \n"
                + "A media dos valores obtidos na pergunta 4 é : " + mediaParametrosShort[3] + ". \n"
                + "## Artigos Full - Media Parametros ##"
                + "A media dos valores obtidos na pergunta 1 é : " + mediaParametrosFull[0] + ". \n"
                + "A media dos valores obtidos na pergunta 2 é : " + mediaParametrosFull[1] + ". \n"
                + "A media dos valores obtidos na pergunta 3 é : " + mediaParametrosFull[2] + ". \n"
                + "A media dos valores obtidos na pergunta 4 é : " + mediaParametrosFull[3] + ". \n"
                + "## Artigos Poster - Media Parametros ##"
                + "A media dos valores obtidos na pergunta 1 é : " + mediaParametrosPoster[0] + ". \n"
                + "A media dos valores obtidos na pergunta 2 é : " + mediaParametrosPoster[1] + ". \n"
                + "A media dos valores obtidos na pergunta 3 é : " + mediaParametrosPoster[2] + ". \n"
                + "A media dos valores obtidos na pergunta 4 é : " + mediaParametrosPoster[3] + ". \n");
        return result;
    }
    
     

    /**
     * Apresenta uma janela com uma mensagem recebida por parâmetro.
     *
     * @param texto da mensagem a apresentar
     */
    private void mensagem(String texto, int icon) {
        JOptionPane.showMessageDialog(Estatistica_final.this,
                texto,
                "Editar Evento",
                icon);
    }
}
