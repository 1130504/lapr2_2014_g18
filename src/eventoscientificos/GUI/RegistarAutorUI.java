/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.GUI;


import eventoscientificos.controller.*;
import eventoscientificos.model.Artigo;
import eventoscientificos.model.Autor;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.ListaSubmissoes;
import eventoscientificos.model.Pagamento;
import eventoscientificos.model.ProcessoPagamento;
import eventoscientificos.model.RegistoProcessoPagamento;
import eventoscientificos.model.Submissao;
import eventoscientificos.pagamentos.SistemaPagamento;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import javax.swing.border.EmptyBorder;


/**
 *
 * @author Alexandre
 */
public class RegistarAutorUI extends JDialog {

    private final Empresa m_empresa;

    private final RegistoAutorController m_controllerRAC;

    ;

    private RegistoProcessoPagamento m_registoProcesso;
    private ProcessoPagamento m_processo;
    private Pagamento m_pagamento;
    ;

    private JButton btnOk, btnCancelar;
    /**
     * TextField para a data de validade de cartão
     */
    private JTextField dataValidadeCC;
    /**
     * TextField para o numero do cartão
     */
    private JTextField numCC;
    /**
     * TextField para a data limite de Pagamento
     */
    private JTextField dataLimPagamento;

    /**
     * TextField para o título
     */
    private JTextField autor;
    /**
     * Evento
     */
    private Evento m_evento;
    
    /**
     * Sistema nome
     */
    private String m_sistema;

    private static final int DIALOGO_DESVIO_X = 50, DIALOGO_DESVIO_Y = 100;

    public RegistarAutorUI(Frame pai, Empresa empresa)  {
        super(pai, "Registar Autor", true);

        this.m_empresa = empresa;
        this.m_controllerRAC = new RegistoAutorController(this.m_empresa);

        List<Evento> le = this.m_empresa.getRegistaEvento().getListaEventos();
        List<Autor> listaAutores = new ArrayList<>();
        ListaSubmissoes listaArtigos = new ListaSubmissoes();

        if (!le.isEmpty()) {
            Evento e = this.mostraEventos(le);
            this.m_controllerRAC.setEvento(e);

            for (int i = 0; i < e.getListaSubmissoes().getListaSubmissao().size(); i++) {
                for (int j = 0; j < e.getListaSubmissoes().getListaSubmissao().get(i).getArtigo().getM_listaAutores().size(); j++) {
                    if (!listaAutores.contains(e.getListaSubmissoes().getListaSubmissao().get(i).getArtigo().getM_listaAutores().get(j))) {
                        listaAutores.add(e.getListaSubmissoes().getListaSubmissao().get(i).getArtigo().getM_listaAutores().get(j));
                    }

                }

            }
            Autor a = this.mostraAutores(listaAutores);

            this.m_controllerRAC.setAutor(a);
            ListaSubmissoes ls = this.m_controllerRAC.obterSubmissoes(a);

            listaArtigos.setListaSubmissao(ls.getListaSubmissao());
            
            

            this.mostraArtigos(listaArtigos);
            

            this.m_evento = e;

            m_registoProcesso = new RegistoProcessoPagamento(this.m_evento, listaArtigos);
            m_registoProcesso.verficaArtigo(listaArtigos);
            m_processo = new ProcessoPagamento(m_registoProcesso, this.m_evento);

            m_processo.setFormula();
            m_processo.calcularPagamento();
            m_pagamento = new Pagamento(this.m_processo,this.m_evento);
            this.mostraPagamento();
            
            
            m_sistema = selecionaSistema();
            
            JPanel p2 = CriarPainelDadosPagamento();
            add(p2, BorderLayout.CENTER);

            this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
            setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
            
           
            pack();
            setResizable(false);
            setVisible(true);
            m_pagamento.escreverFicheiroCSV(m_registoProcesso);
        } else {
             this.mensagem("Tem de criar o ler Eventos", JOptionPane.ERROR_MESSAGE);
        }
    }

    private Submissao mostraArtigos(ListaSubmissoes listaArtigos) {
        String[] opcoes = new String[listaArtigos.getListaSubmissao().size()];
        int aux = 1;
        for (int i = 0; i < opcoes.length; i++) {
            opcoes[i] = aux + i + " - " + listaArtigos.getListaSubmissao().get(i).getArtigo().getTitulo();
             
        }

        String artigos = (String) JOptionPane.showInputDialog(RegistarAutorUI.this,
                "Escolha um Artigo", "",
                JOptionPane.PLAIN_MESSAGE,
                null,
                opcoes,
                opcoes[0]);
        if (artigos == null) {
            dispose();
        }
        String[] parts = artigos.split(" - ");
        return (listaArtigos.getListaSubmissao().get(Integer.parseInt(parts[0]) - 1));
    }

    private Autor mostraAutores(List<Autor> listaAutores) {
        String[] opcoes = new String[listaAutores.size()];
        int aux = 1;
        for (int i = 0; i < opcoes.length; i++) {
            opcoes[i] = aux + i + " - " + listaAutores.get(i).getM_strNome();
        }

        String autor = (String) JOptionPane.showInputDialog(RegistarAutorUI.this,
                "Escolha um Autor", "",
                JOptionPane.PLAIN_MESSAGE,
                null,
                opcoes,
                opcoes[0]);
        if (autor == null) {
            dispose();
        }
        String[] parts = autor.split(" - ");
        return (listaAutores.get(Integer.parseInt(parts[0]) - 1));
    }

    private Evento mostraEventos(List<Evento> listaEventos) {

        String[] opcoes = new String[listaEventos.size()];
        int aux = 1;
        for (int i = 0; i < opcoes.length; i++) {
            opcoes[i] = aux + i + " - " + listaEventos.get(i).getM_strTitulo();
        }

        String evento = (String) JOptionPane.showInputDialog(RegistarAutorUI.this,
                "Escolha um evento", "",
                JOptionPane.PLAIN_MESSAGE,
                null,
                opcoes,
                opcoes[0]);
        if (evento == null) {
            dispose();
        }
        String[] parts = evento.split(" - ");
        return (listaEventos.get(Integer.parseInt(parts[0]) - 1));
    }

    private void mostraPagamento() {

        JOptionPane.showMessageDialog(RegistarAutorUI.this, this.m_processo.getM_valor(),
                "Valor a Pagar", JOptionPane.INFORMATION_MESSAGE);

    }

    private String selecionaSistema() {

        String[] opcoes = {"CanadaExpress", "VisaoLight"};

        String sistema = (String) JOptionPane.showInputDialog(RegistarAutorUI.this,
                "Escolha um Sistema de Pagamento", "",
                JOptionPane.PLAIN_MESSAGE,
                null,
                opcoes,
                opcoes[0]);
        if (sistema == null) {
            dispose();
        }

        return sistema;
    }

    private JPanel CriarPainelDadosPagamento() {

        JPanel p1 = this.setDados();
        JPanel p2 = this.criarPainelBotoes();

        JPanel p = new JPanel(new GridLayout(4, 2));

        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.SOUTH);
        return p;
    }

    private JPanel setDados() {
        JPanel p1 = criarPainelDataValidadeCC();
        JPanel p2 = criarPainelNumCC();

        JPanel p = new JPanel(new GridLayout(2, 2));
        p.add(p1);
        p.add(p2);
        return p;
    }

    private JPanel criarPainelDataValidadeCC() {
       
        JLabel lbl = new JLabel("Data de Validade de Cartão de Crédito(yyyy-MM-dd):", JLabel.LEFT);

        final int CAMPO_LARGURA = 20;
        dataValidadeCC = new JTextField(CAMPO_LARGURA);
       
        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(dataValidadeCC);

        return p;
    }

    private JPanel criarPainelNumCC() {
        JLabel lbl = new JLabel("Número do Cartão de Crédito:", JLabel.LEFT);

        final int CAMPO_LARGURA = 20;
        numCC = new JTextField(CAMPO_LARGURA);
        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(numCC);

        return p;
    }

    /**
     * Cria o painel de botões
     *
     * @return painel
     */
    private JPanel criarPainelBotoes() {
        btnOk = criarBotaoOK();
        getRootPane().setDefaultButton(btnOk);

        btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOk);
        p.add(btnCancelar);

        return p;
    }

    /**
     * Cria botão OK
     *
     * @return botão
     */
    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {

                try {
                    if (dataValidadeCC.getText().equals("")
                            || numCC.getText().equals("")) {
                        JOptionPane.showMessageDialog(RegistarAutorUI.this, "Todos os campos são obrigatórios!\n", "Criar Evento", JOptionPane.ERROR_MESSAGE);

                    } else {
                        Date data = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(dataValidadeCC.getText());
                        m_pagamento.sistemaPagamento(m_sistema, numCC.getText(), data);
                        mensagem("Dados submetidos com sucesso", JOptionPane.INFORMATION_MESSAGE);
                        dispose();

                    }

                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(RegistarAutorUI.this,
                            "Valores introduzidos inválidos",
                            "Registar Autor",
                            JOptionPane.ERROR_MESSAGE);
                } catch (ParseException ex) {
                    JOptionPane.showMessageDialog(RegistarAutorUI.this,
                            "Formato de data inválido",
                            "Registar Autor",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        );
        return btn;
    }

    /**
     * Cria o botão Cancelar
     *
     * @return JButton
     */
    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                mensagem("Cancelado!", JOptionPane.INFORMATION_MESSAGE);
                dispose();
            }
        });
        return btn;
    }

    /**
     * Apresenta uma janela com uma mensagem recebida por parâmetro.
     *
     * @param texto da mensagem a apresentar
     */
    private void mensagem(String texto, int icon) {
        JOptionPane.showMessageDialog(RegistarAutorUI.this,
                texto,
                "Registar Autor",
                icon);
    }

}
