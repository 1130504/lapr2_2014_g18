/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.GUI;

import eventoscientificos.controller.ReverArtigoController;
import eventoscientificos.model.Artigo;
import eventoscientificos.model.Distribuicao;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.ListaSubmissoes;
import eventoscientificos.model.Revisao;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Utilizador;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class ReverArtigoUI_final extends JDialog {

    private Empresa m_empresa;

    private ReverArtigoController m_reverArtigoController;

    private String revisorID;
    private Revisor revisor;
    private Artigo artigo;

    private JButton okBtn, cancelarBtn;
    private JTextField confianca, adequacao, originalidade, qualidade, textoJustificativo, recomendacao;

    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;

    public ReverArtigoUI_final(Frame pai, Empresa empresa) {
        super(pai, "Submeter Revisão de Artigo", true);

        this.m_empresa = empresa;
        this.m_reverArtigoController = new ReverArtigoController(m_empresa);

        this.revisorID = this.inserirID();
        Revisor revisor = new Revisor(this.m_empresa.getUtilizador(revisorID));
        if (this.revisorID != null) {
            List<Evento> listaEventos = this.m_reverArtigoController.getEventosRevisor(this.revisorID);

            if (!listaEventos.isEmpty()) {
                Evento e = mostraEventos(listaEventos);

                if (e == null) {
                    mensagem("O revisor não tem eventos", JOptionPane.ERROR_MESSAGE);
                }

                this.m_reverArtigoController.selectEvento(e);
                try {
                    List<Artigo> listaArtigos = this.m_reverArtigoController.getArtigosRevisor(this.revisorID);

                    artigo = mostraArtigos(listaArtigos);
                    if (m_reverArtigoController.verificaEstado(artigo)) {
                        mensagem("Artigo escolhido não está no estado Distribuido", JOptionPane.ERROR_MESSAGE);
                    }

                    if (artigo != null) {
                        this.m_reverArtigoController.setArtigo(artigo);

                        JPanel p1 = criarPainelAvaliacao();
                        JPanel p2 = criarPainelBotoes();
                        this.add(p1, BorderLayout.NORTH);
                        this.add(p2, BorderLayout.SOUTH);

                        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                        setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
                        pack();
                        setResizable(false);
                        setVisible(true);
                    }
                } catch (NullPointerException ex) {
                    mensagem("Revisor não tem artigos para efectuar revisão", JOptionPane.ERROR_MESSAGE);
                }

            }
        }
    }

    private JPanel criarPainelAvaliacao() {
        JPanel p1 = criarPainelConfianca();
        JPanel p2 = criarPainelAdequacao();
        JPanel p3 = criarPainelOriginalidade();
        JPanel p4 = criarPainelQualidade();
        JPanel p5 = criarPainelRecomendacao();
        JPanel p6 = criarPainelTextoJustificativo();

        JPanel p = new JPanel(new GridLayout(6, 2));
        p.add(p1);
        p.add(p2);
        p.add(p3);
        p.add(p4);
        p.add(p5);
        p.add(p6);

        return p;
    }

    /**
     * Apresenta uma janela para inserir o id.
     *
     * @return String id
     */
    private String inserirID() {
        return (JOptionPane.showInputDialog(ReverArtigoUI_final.this,
                "Insira o ID do Revisor:", "Rever Artigo",
                JOptionPane.DEFAULT_OPTION));
    }

    /**
     * Apresenta uma janela com uma mensagem recebida por parâmetro.
     *
     * @param texto da mensagem a apresentar
     */
    private void mensagem(String texto, int icon) {
        JOptionPane.showMessageDialog(ReverArtigoUI_final.this,
                texto,
                "Rever Artigo",
                icon);
    }

    /**
     * Apresenta uma janela de confirmação e devolve um inteiro conforme a
     * decisão
     *
     * @return int resposta
     */
    private int confirmar() {
        String[] opcoes = {"Confirmar", "Cancelar"};
        int response = JOptionPane.showOptionDialog(this, "Confirma?", "Rever Artigo", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
                null, opcoes, opcoes[0]);
        return response;
    }

    /**
     * Método para apresentar numa janela a lista de eventos recebida por
     * parâmetro.
     *
     * @param listaEventos a lista de eventos a apresentar
     * @return o Evento escolhido
     */
    private Evento mostraEventos(List<Evento> listaEventos) {

        String[] opcoes = new String[listaEventos.size()];
        int aux = 1;
        for (int i = 0; i < opcoes.length; i++) {
            opcoes[i] = aux + i + " - " + listaEventos.get(i).getM_strTitulo();
        }

        String evento = (String) JOptionPane.showInputDialog(ReverArtigoUI_final.this,
                "Escolha um evento:", "Rever Artigo",
                JOptionPane.PLAIN_MESSAGE,
                null,
                opcoes,
                opcoes[0]);
        if (evento == null) {
            return null;
        }
        String[] parts = evento.split(" - ");
        return (listaEventos.get(Integer.parseInt(parts[0]) - 1));
    }

    /**
     * Método para apresentar numa janela a lista de artigos recebida por
     * parâmetro.
     *
     * @param listaSubmissoes a lista de submissoes a apresentar
     * @return o Artigo escolhido
     */
    private Artigo mostraArtigos(List<Artigo> listaArtigos) {

        String[] opcoes = new String[listaArtigos.size()];
        int aux = 1;
        for (int i = 0; i < opcoes.length; i++) {
            opcoes[i] = aux + i + " - " + listaArtigos.get(i).getTitulo();
        }

        String artigo = (String) JOptionPane.showInputDialog(ReverArtigoUI_final.this,
                "Escolha um artigo:", "Rever Artigo",
                JOptionPane.PLAIN_MESSAGE,
                null,
                opcoes,
                opcoes[0]);
        if (artigo == null) {
            return null;
        }
        String[] parts = artigo.split(" - ");
        return (listaArtigos.get(Integer.parseInt(parts[0]) - 1));
    }

    private JPanel criarPainelConfianca() {
        JLabel lbl = new JLabel("Confiança do revisor nos tópicos do artigo:", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        confianca = new JTextField(CAMPO_LARGURA);
        confianca.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(confianca);

        return p;
    }

    private JPanel criarPainelAdequacao() {
        JLabel lbl = new JLabel("Adequação ao evento:", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        adequacao = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(adequacao);

        return p;
    }

    private JPanel criarPainelRecomendacao() {
        JLabel lbl = new JLabel("Adequação ao evento(Aceite/Rejeitado):", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        recomendacao = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(recomendacao);

        return p;
    }

    private JPanel criarPainelOriginalidade() {
        JLabel lbl = new JLabel("Originalidade do artigo:", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        originalidade = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(originalidade);

        return p;
    }

    private JPanel criarPainelTextoJustificativo() {
        JLabel lbl = new JLabel("Pequeno texto Justificativo", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        textoJustificativo = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(textoJustificativo);

        return p;
    }

    private JPanel criarPainelQualidade() {
        JLabel lbl = new JLabel("Qualidade da apresentação:", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        qualidade = new JTextField(CAMPO_LARGURA);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(qualidade);

        return p;
    }

    /**
     * Cria o painel de botões
     *
     * @return painel
     */
    private JPanel criarPainelBotoes() {
        okBtn = criarBotaoOK();
        getRootPane().setDefaultButton(okBtn);

        cancelarBtn = criarBotaoCancelar();
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(okBtn);
        p.add(cancelarBtn);

        return p;
    }

    /**
     * Cria botão OK
     *
     * @return botão
     */
    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                String confiancaTxt = confianca.getText();
                String adequacaoTxt = adequacao.getText();
                String originalidadeTxt = originalidade.getText();
                String qualidadeTxt = qualidade.getText();
                String recomendacaoTxt = recomendacao.getText();
                String justificacaoTxt = textoJustificativo.getText();
                boolean flag = true;
                do {
                    if (confiancaTxt.equals("") || adequacaoTxt.equals("")
                            || originalidadeTxt.equals("") || qualidadeTxt.equals("")
                            || recomendacaoTxt.equals("") || justificacaoTxt.equals("")) {
                        flag = false;
                        mensagem("Faltam dados", JOptionPane.ERROR_MESSAGE);
                    }
                    if (!confiancaTxt.matches("0|1|2|3|4|5")
                            || !adequacaoTxt.matches("0|1|2|3|4|5")
                            || !originalidadeTxt.matches("0|1|2|3|4|5")
                            || !qualidadeTxt.matches("0|1|2|3|4|5")
                            || !recomendacaoTxt.matches("Aceite|Rejeitado")) {
                        flag = false;
                        mensagem("Valores introduzidos inválidos", JOptionPane.ERROR_MESSAGE);
                    }
                    Revisao r = m_reverArtigoController.novaRevisao();
                    m_reverArtigoController.setDados(Integer.parseInt(confiancaTxt),
                            Integer.parseInt(adequacaoTxt),
                            Integer.parseInt(originalidadeTxt),
                            Integer.parseInt(qualidadeTxt),
                            recomendacaoTxt, justificacaoTxt, revisor);
                    m_reverArtigoController.registaRevisao(r);
                    m_reverArtigoController.getSubmissao(artigo).setRevista();

                    if (artigo.getListaRevisoes() != null) {
                        flag = true;
                    } else {
                        mensagem("Revisão nao registada", JOptionPane.ERROR_MESSAGE);
                        flag = false;
                    }
                } while (flag == false);
                if (confirmar() == 0) {
                    mensagem("Revisão guardada com sucesso", JOptionPane.INFORMATION_MESSAGE);
                    dispose();
                } else {
                    mensagem("Revisão cancelada", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
        return btn;
    }

    /**
     * Cria o botão Cancelar
     *
     * @return JButton
     */
    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                mensagem("Cancelado!", JOptionPane.INFORMATION_MESSAGE);
                dispose();
            }
        });
        return btn;
    }

}
