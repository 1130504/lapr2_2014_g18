/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.GUI;

import eventoscientificos.controller.ValidarEventoImportadoController;
import eventoscientificos.eventostate.EventoCriadoCSVState;
import eventoscientificos.eventostate.EventoRegistadoState;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class ValidarEventoImportadoUI extends JDialog {

    /**
     * A instância da Empresa.
     */
    private Empresa m_empresa;
    /**
     * A instância de DefinirValorRegistoController.
     */
    private ValidarEventoImportadoController m_controllerVEI;
    private JButton aceitarBtn, rejeitarBtn;
    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;
    private JLabel eventoLabel;
    int indexEvento = 0;
    int indexEventoAux = 0;

    public ValidarEventoImportadoUI(Frame pai, Empresa m_empresa) {
        super(pai, "Validar Eventos", true);

        this.m_empresa = m_empresa;
        this.m_controllerVEI = new ValidarEventoImportadoController(m_empresa);

        m_controllerVEI.registoEventosTemp();
        if (m_controllerVEI.getListaTemp().isEmpty()) {
            mensagem("Lista de eventos no estado criado vazia", JOptionPane.ERROR_MESSAGE);
        } else {
            try {
                JPanel p1 = criarPainelEvento();
                JPanel p2 = criarPainelBotoes();
                this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                
                while (!m_controllerVEI.getListaTemp().isEmpty()) {

                    Evento e = m_controllerVEI.getListaTemp().get(indexEventoAux);
                    m_controllerVEI.setM_evento(e);
                    indexEventoAux++;
                    
                    this.eventoLabel.setText("");
                    
                    this.eventoLabel.setText("Título: " + e.getM_strTitulo() + "\n"
                            + "Descrição: " + e.getM_strDescricao() + "\n"
                    );

                    add(p1, BorderLayout.NORTH);
                    add(p2, BorderLayout.SOUTH);
                    setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
                    pack();
                    setResizable(false);
                    
                    
                    setVisible(true);
                }
            } catch (NullPointerException | IndexOutOfBoundsException ex) {
                mensagem("Não existem mais eventos para confirmar", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Cria o painel de botões
     *
     * @return painel
     */
    private JPanel criarPainelBotoes() {
        aceitarBtn = criarBotaoAceitar();
        getRootPane().setDefaultButton(aceitarBtn);

        rejeitarBtn = criarBotaoRejeitar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(aceitarBtn);
        p.add(rejeitarBtn);

        return p;
    }

    private JButton criarBotaoAceitar() {
        JButton btn = new JButton("Aceitar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                m_controllerVEI.setEstadoRegistado();
                if (m_controllerVEI.getM_evento().getEstado() instanceof EventoRegistadoState) {
                    mensagem("Evento registado com sucesso", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    mensagem("Evento não foi registado com sucesso", JOptionPane.WARNING_MESSAGE);
                }
                dispose();

            }
        });
        return btn;
    }

    private JButton criarBotaoRejeitar() {
        JButton btn = new JButton("Rejeitar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                m_controllerVEI.getListaTemp().remove(indexEvento);
                if (m_controllerVEI.getM_evento().getEstado() instanceof EventoCriadoCSVState) {
                    mensagem("Evento rejeitado com sucesso", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    mensagem("Evento não foi rejeitado com sucesso", JOptionPane.WARNING_MESSAGE);
                }
                dispose();
            }
        });
        return btn;
    }

    /**
     * Apresenta uma janela com uma mensagem recebida por parâmetro.
     *
     * @param texto da mensagem a apresentar
     */
    private void mensagem(String texto, int icon) {
        JOptionPane.showMessageDialog(ValidarEventoImportadoUI.this,
                texto,
                "Validar Evento",
                icon);
    }

    /**
     * Apresenta uma janela de confirmação e devolve um inteiro conforme a
     * decisão
     *
     * @return int resposta
     */
    private int confirmar() {
        String[] opcoes = {"Confirmar", "Cancelar"};
        int response = JOptionPane.showOptionDialog(this, "Confirma?", "Validar Evento", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
                null, opcoes, opcoes[0]);
        return response;
    }

    private JPanel criarPainelEvento() {
        this.eventoLabel = new JLabel("Evento:", JLabel.LEFT);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 0;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(eventoLabel);
        return p;
    }
}
