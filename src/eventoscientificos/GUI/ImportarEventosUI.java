/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.GUI;

import eventoscientificos.model.*;
import eventoscientificos.controller.*;
import java.util.List;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.io.File;
import java.io.IOException;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.xml.sax.SAXException;

/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */
public class ImportarEventosUI extends JDialog {

    /**
     * Empresa.
     */
    private Empresa m_empresa;

    /**
     * Controller Importar Eventos Científicos.
     */
    private ImportarEventosController m_controllerIEC;
    JTextArea _resultArea = new JTextArea(6, 20);

    /**
     * Constrói uma instância de ImportarEventosUI.
     *
     * @param pai
     * @param empresa
     */
    public ImportarEventosUI(Frame pai, Empresa empresa) {
        super(pai, "Criar Evento Automaticamente", true);
        this.m_empresa = empresa;
        this.m_controllerIEC = new ImportarEventosController(m_empresa);

        File f = criarFileChooser();
        if (f != null) {
            m_controllerIEC.setPathFile(f.getAbsolutePath());
            m_controllerIEC.setExtensao(f.getAbsolutePath());

            try {
                m_controllerIEC.ListaEventos();
                m_controllerIEC.guardarEventos();
            } catch (IOException | NullPointerException ex) {
                mensagem("Ficheiro inválido", JOptionPane.ERROR_MESSAGE);
            } catch (ParserConfigurationException | XPathExpressionException | SAXException e) {
                mensagem("Erro ao ler ficheiro", JOptionPane.ERROR_MESSAGE);
            }
        }
        String eventos = "";
        for (Evento e : m_controllerIEC.getRegistoEventos()) {
            eventos += e.toString() + "\n";
        }
        if (eventos != null) {
            mensagem("Eventos lidos com sucesso!", JOptionPane.INFORMATION_MESSAGE);
        }
        _resultArea.setText(eventos);
        JScrollPane scrollingArea = new JScrollPane(_resultArea);

        JPanel content = new JPanel();
        content.setLayout(new BorderLayout());
        content.add(scrollingArea, BorderLayout.CENTER);

        this.setContentPane(content);
        this.setTitle("Eventos Importados");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.pack();

    }

    /**
     * Apresenta uma janela com uma mensagem recebida por parâmetro.
     *
     * @param texto da mensagem a apresentar
     */
    private void mensagem(String texto, int icon) {
        JOptionPane.showMessageDialog(ImportarEventosUI.this,
                texto,
                "Importar Eventos",
                icon);
    }

    public File criarFileChooser() {
        JFileChooser filechooser = new JFileChooser();
        if (filechooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            File f = filechooser.getSelectedFile();
            System.out.println("This file was chosen for opening: " + f);
            return f;
        } else {
            System.out.println("No file was chosen or an error occured");
            return null;
        }
    }
}
