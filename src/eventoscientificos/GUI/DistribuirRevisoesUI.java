/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.GUI;

import eventoscientificos.controller.DistribuirRevisoesController;
import eventoscientificos.model.*;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import javax.swing.border.EmptyBorder;
import javax.swing.colorchooser.AbstractColorChooserPanel;

/**
 *
 * @author Bruno Freitas <1130504@isep.ipp.pt>
 */
public class DistribuirRevisoesUI extends JDialog {

    /**
     * A instância da Empresa.
     */
    private Empresa m_empresa;
    /**
     * A instância de DistribuirRevisoesController.
     */
    private DistribuirRevisoesController m_controllerDRC;
    /**
     * Artigo a considerar
     */
    private Artigo m_artigo;
    /**
     * Lista de revisores
     */
    private JList listaRevisores;
    /**
     * Lista de revisores selecionados
     */
    private ArrayList<Revisor> listaRevisoresSelecionados;
    /**
     * Botões Ok e Cancelar
     */
    private JButton btnOk, btnCancelar;

    private static final int DIALOGO_DESVIO_X = 100, DIALOGO_DESVIO_Y = 100;

    /**
     * Contrutor que recebe como parâmetro o frame pai e a empresa
     *
     * @param pai
     * @param empresa
     */
    public DistribuirRevisoesUI(Frame pai, Empresa empresa) {
        super(pai, "Distribuir Revisões", true);
        this.m_empresa = empresa;
        this.m_controllerDRC = new DistribuirRevisoesController(this.m_empresa);

        String OrgId = this.inserirID();

        List<Evento> le = m_controllerDRC.getEventosOrganizador(OrgId);
        if (OrgId != null) {
            if (!le.isEmpty()) {
                Evento e = this.mostraEventos(le);
                this.m_controllerDRC.setEvento(e);
                m_artigo = this.mostraArtigos(e.getListaSubmissoes());
                ArrayList<Revisor> lr = m_controllerDRC.getListaRevisores();

                JPanel p1 = criarPainelLista(lr);
                JPanel p2 = criarPainelBotoes();
                add(p1, BorderLayout.NORTH);
                add(p2, BorderLayout.SOUTH);

                this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                setLocation(pai.getX() + DIALOGO_DESVIO_X, pai.getY() + DIALOGO_DESVIO_Y);
                pack();
                setResizable(false);
                setVisible(true);

            } else {
                this.mensagem("Organizador inválido ou sem Eventos.", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Cria o painel de botões
     *
     * @return painel
     */
    private JPanel criarPainelBotoes() {
        btnOk = criarBotaoOK();
        getRootPane().setDefaultButton(btnOk);

        btnCancelar = criarBotaoCancelar();
        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnOk);
        p.add(btnCancelar);

        return p;
    }

    /**
     * Cria botão OK
     *
     * @return botão
     */
    private JButton criarBotaoOK() {
        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                listaRevisoresSelecionados = (ArrayList)listaRevisores.getSelectedValuesList();
                Distribuicao d = new Distribuicao(m_artigo,listaRevisoresSelecionados);
                mensagem(m_controllerDRC.mostraDistribuicao(d), JOptionPane.INFORMATION_MESSAGE);
                if (confirmar() == 0) {
                    
                    m_controllerDRC.setDistribuicao(d);
                    dispose();
                }else{
                    mensagem("Distribuição cancelada", JOptionPane.INFORMATION_MESSAGE);
                }
                if (m_controllerDRC.getDistribuicoes() != null) {
                    mensagem("Distribuição guardada com sucesso", JOptionPane.INFORMATION_MESSAGE);

                } else {
                    mensagem("Distribuição nao guardada!", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        return btn;
    }

    /**
     * Cria o botão Cancelar
     *
     * @return JButton
     */
    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                mensagem("Cancelado!", JOptionPane.INFORMATION_MESSAGE);
                dispose();
            }
        });
        return btn;
    }

    /**
     * Apresenta uma janela para inserir o id.
     *
     * @return String id
     */
    private String inserirID() {
        return (JOptionPane.showInputDialog(DistribuirRevisoesUI.this,
                "Insira o ID do Organizador:", "Distribuir Revisões",
                JOptionPane.DEFAULT_OPTION));
    }

    /**
     * Apresenta uma janela com uma mensagem recebida por parâmetro.
     *
     * @param texto da mensagem a apresentar
     */
    private void mensagem(String texto, int icon) {
        JOptionPane.showMessageDialog(DistribuirRevisoesUI.this,
                texto,
                "Distribuir Revisões",
                icon);
    }

    /**
     * Apresenta uma janela de confirmação e devolve um inteiro conforme a
     * decisão
     *
     * @return int resposta
     */
    private int confirmar() {
        String[] opcoes = {"Confirmar", "Cancelar"};
        int response = JOptionPane.showOptionDialog(this, "Confirma?", "Distribuir Revisões", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
                null, opcoes, opcoes[0]);
        return response;
    }

    /**
     * Método para apresentar numa janela a lista de eventos recebida por
     * parâmetro.
     *
     * @param listaEventos a lista de eventos a apresentar
     * @return o Evento escolhido
     */
    private Evento mostraEventos(List<Evento> listaEventos) {

        String[] opcoes = new String[listaEventos.size()];
        int aux = 1;
        for (int i = 0; i < opcoes.length; i++) {
            opcoes[i] = aux + i + " - " + listaEventos.get(i).getM_strTitulo();
        }

        String evento = (String) JOptionPane.showInputDialog(DistribuirRevisoesUI.this,
                "Escolha um evento:", "Distribuir Revisões",
                JOptionPane.PLAIN_MESSAGE,
                null,
                opcoes,
                opcoes[0]);
        if (evento == null) {
            return null;
        }
        String[] parts = evento.split(" - ");
        return (listaEventos.get(Integer.parseInt(parts[0]) - 1));
    }

    /**
     * Método para apresentar numa janela a lista de artigos recebida por
     * parâmetro.
     *
     * @param listaSubmissoes a lista de submissoes a apresentar
     * @return o Artigo escolhido
     */
    private Artigo mostraArtigos(ListaSubmissoes listaSubmissoes) {

        String[] opcoes = new String[listaSubmissoes.getListaSubmissao().size()];
        int aux = 1;
        for (int i = 0; i < opcoes.length; i++) {
            opcoes[i] = aux + i + " - " + listaSubmissoes.getListaSubmissao().get(i).getArtigo().getTitulo();
        }

        String artigo = (String) JOptionPane.showInputDialog(DistribuirRevisoesUI.this,
                "Escolha um artigo:", "Distribuir Revisões",
                JOptionPane.PLAIN_MESSAGE,
                null,
                opcoes,
                opcoes[0]);
        if (artigo == null) {
            return null;
        }
        String[] parts = artigo.split(" - ");
        return (listaSubmissoes.getListaSubmissao().get(Integer.parseInt(parts[0]) - 1).getArtigo());
    }

    private JPanel criarPainelLista(ArrayList<Revisor> lr) {
        Revisor[] array = lr.toArray(new Revisor[lr.size()]);
        listaRevisores = new JList(array);
        listaRevisores.setVisibleRowCount(5);
        final int LISTA_LARGURA = 300, LISTA_ALTURA = 300;
        listaRevisores.setPreferredSize(new Dimension(LISTA_LARGURA, LISTA_ALTURA));
        JScrollPane scrRevisores = new JScrollPane(listaRevisores);

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10, MARGEM_ESQUERDA = 10,
                MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA, MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(scrRevisores);
        return p;
    }

}
