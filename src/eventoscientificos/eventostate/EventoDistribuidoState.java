/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.eventostate;

import eventoscientificos.model.Evento;
import java.io.Serializable;

/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */
public class EventoDistribuidoState implements EventoState, Serializable {

    /**
     * Evento.
     */
    private Evento m_evento;

    /**
     * Constrói uma instância EventoDistribuidoState.
     *
     * @param e
     */
    public EventoDistribuidoState(Evento e) {
        this.m_evento = e;

    }

    @Override
    public boolean setcriado() {
        return false;
    }

    @Override
    public boolean setRegistado() {
        return false;
    }

    @Override
    public boolean setTopicosCriados() {
        return false;
    }

    @Override
    public boolean setCPDefinida() {
        return false;
    }

    @Override
    public boolean setDistribuido() {
        return false;
    }

    @Override
    public boolean setRevisto() {
        if (valida()) {
            m_evento.setEstado(new EventoRevistoState(m_evento));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean setDecidido() {
        return false;
    }

    @Override
    public boolean setNotificado() {
        return false;
    }

    @Override
    public boolean setCameraReady() {
        return false;
    }

    @Override
    public boolean valida() {
        if (this.m_evento.getEstado() instanceof EventoDistribuidoState
                && !this.m_evento.getM_listaRevisoes().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean setCriadoPorCSV() {
        return false;
    }

    @Override
    public boolean setLidoPorCSV() {
        return false;
    }

    @Override
    public boolean setValoresRegistoDefinido() {
return false;
    }

}
