/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.eventostate;

import eventoscientificos.model.Evento;

/**
 *
 * @author Pedro Miguel Gomes < aka Dante w-email: 1130383@isep.ipp.pt>
 */
public interface EventoState {
    /**
     * EventoCriado pelo ficheiro CSV.
     * @return 
     */
    public boolean setCriadoPorCSV();
    /**
     * EventoLido pelo ficheiro CSV.
     * @return 
     */
    public boolean setLidoPorCSV();
            
    /**
     * EventoCriado
     * @return 
     */
     public boolean setcriado();
    /**
     * Altera o estado do Evento para registado.
     * @return 
     */
    public boolean setRegistado();
    /**
     * Altera o estado do Evento para Evento com valores Registo Definidos.
     * @return 
     */
    public boolean setValoresRegistoDefinido();
    /**
     * Altera o estado do Evento para Evento tópicos criados.
     * @return 
     */
    public boolean setTopicosCriados();
    /**
     * Altera o estado do Evento para Evento CP Definida.
     * @return 
     */
    public boolean setCPDefinida();
    /**
     * Altera o estado do Evento para Evento com Artigos por rever Distribuídos.
     * @return 
     */
    public boolean setDistribuido();
    /**
     * Altera o estado do Evento para Evento com Artigos Revistos. 
     * @return 
     */
    public boolean setRevisto();
    /**
     * Altera o estado do Evento para Evento com Artigos Decididos.
     * @return 
     */
    public boolean setDecidido();
    /**
     * Altera o estado do Evento para Evento com Autores Notificados.
     * @return 
     */
    public boolean setNotificado();
    /**
     * Altera o estado do Evento Camera Ready.
     * @return 
     */
    public boolean setCameraReady();
    
    /**
     * Valida o Estado do Evento.
     * *pode ser usado para impor pré-condição entre estados.
     * @return 
     */
    public boolean valida();
    
    
}
