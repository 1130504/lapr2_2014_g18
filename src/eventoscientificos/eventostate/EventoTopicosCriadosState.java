/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.eventostate;

import eventoscientificos.eventostate.EventoState;
import eventoscientificos.eventostate.EventoCPDefininaState;
import eventoscientificos.model.Evento;
import java.io.Serializable;

/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */
public class EventoTopicosCriadosState implements EventoState, Serializable {

    /**
     * Evento.
     */
    private Evento m_evento;

    public EventoTopicosCriadosState(Evento e) {
        this.m_evento = e;
    }

    @Override
    public boolean setcriado() {
        return false;
    }

    @Override
    public boolean setRegistado() {
        return false;
    }

    @Override
    public boolean setTopicosCriados() {
        return false;
    }

    @Override
    public boolean setCPDefinida() {
        if (valida()) {
            m_evento.setEstado(new EventoCPDefininaState(m_evento));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean setDistribuido() {
        return false;
    }

    @Override
    public boolean setRevisto() {
        return false;
    }

    @Override
    public boolean setDecidido() {
        return false;
    }

    @Override
    public boolean setNotificado() {
        return false;
    }

    @Override
    public boolean setCameraReady() {
        return false;
    }

    @Override
    public boolean valida() {
        return m_evento.getEstado() instanceof EventoTopicosCriadosState
                && !this.m_evento.getCP().getM_listaRevisores().isEmpty();

    }

    @Override
    public boolean setCriadoPorCSV() {
        return false;
    }

    @Override
    public boolean setLidoPorCSV() {
        return false;
    }

    @Override
    public boolean setValoresRegistoDefinido() {
return false;
    }

}
