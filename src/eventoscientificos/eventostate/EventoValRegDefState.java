/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.eventostate;

import eventoscientificos.model.*;
import java.io.Serializable;

/**
 *
 * @author PedroGomes <1130383@isep.ipp.pt>
 */
public class EventoValRegDefState implements EventoState, Serializable {

    /**
     * Evento.
     */
    private Evento evento;

    /**
     * Constrói instância EventoValoresRegistoDefinidosState.
     *
     * @param e
     */
    public EventoValRegDefState(Evento e) {
        this.evento = e;
    }

    @Override
    public boolean setCriadoPorCSV() {
        return false;
    }

    @Override
    public boolean setLidoPorCSV() {
        return false;
    }

    @Override
    public boolean setcriado() {
        return false;
    }

    @Override
    public boolean setRegistado() {
        return false;
    }

    @Override
    public boolean setTopicosCriados() {
        if (valida()) {
            this.evento.setEstado(new EventoTopicosCriadosState(evento));

            return true;
        } else {
            return false;

        }
    }

    @Override
    public boolean setCPDefinida() {
        return false;
    }

    @Override
    public boolean setDistribuido() {
        return false;
    }

    @Override
    public boolean setRevisto() {
        return false;
    }

    @Override
    public boolean setDecidido() {
        return false;
    }

    @Override
    public boolean setNotificado() {
        return false;
    }

    @Override
    public boolean setCameraReady() {
        return false;
    }

    @Override
    public boolean valida() {
        if (this.evento.getEstado() instanceof EventoValRegDefState
                && !this.evento.getTopicos().getTopicos().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean setValoresRegistoDefinido() {
        return false;
    }

}
