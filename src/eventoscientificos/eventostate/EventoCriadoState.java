/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.eventostate;

import eventoscientificos.model.Evento;
import java.io.Serializable;

/**
 *
 * @author Pedro Miguel Gomes < aka Dante w-email: 1130383@isep.ipp.pt>
 */
public class EventoCriadoState implements EventoState, Serializable {

    /**
     * Evento.
     */
    private Evento m_evento;

    /**
     * Constrói uma instância EventoCriadoState que contém o Evento.
     *
     * @param e
     */
    public EventoCriadoState(Evento e) {
        this.m_evento = e;
    }

    @Override
    public boolean setcriado() {
        return false;
    }

    /**
     * Cria uma instância de EventoRegistadoState que passa como parâmetro o evento.
     *
     * @return
     */
    @Override
    public boolean setRegistado() {
        if (valida()) {
            this.m_evento.setEstado(new EventoRegistadoState(m_evento));

            return true;
        } else {
            return false;

        }
    }

    @Override
    public boolean setTopicosCriados() {
        return false;
    }

    @Override
    public boolean setCPDefinida() {
        return false;
    }

    @Override
    public boolean setDistribuido() {
        return false;
    }

    @Override
    public boolean setRevisto() {
        return false;
    }

    @Override
    public boolean setDecidido() {
        return false;
    }

    @Override
    public boolean setNotificado() {
        return false;
    }

    @Override
    public boolean setCameraReady() {
        return false;
    }

    @Override
    public boolean valida() {

        if (this.m_evento.getM_strTitulo() != null
                && this.m_evento.getM_strDescricao() != null
                && this.m_evento.getM_local() != null
                && this.m_evento.getM_strDataInicio() != null
                && this.m_evento.getM_strDataFim() != null
                && this.m_evento.getEstado() instanceof EventoCriadoState) {
         return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean setCriadoPorCSV() {
        return false;
    }

    @Override
    public boolean setLidoPorCSV() {
        return false;
    }

    @Override
    public boolean setValoresRegistoDefinido() {
        return false;
    }

}
